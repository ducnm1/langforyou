<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'cat_title', 'cat_parent_id'
    ];

    /**
     * Get the user that owns the article.
     */
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function catParent()
    {
        return $this->belongsTo('App\Cat','cat_parent_id','id');
    }

    public function catChildrent(){
        return $this->hasMany('App\Cat','cat_parent_id','id');   
    }

    public function posts()
    {
        return $this->hasMany('App\Post','cat_id','id');
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
