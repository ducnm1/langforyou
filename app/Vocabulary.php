<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vocabulary extends Model
{
    protected $fillable = [
        'user_id', 'vocabulary_title', 'group_vocabulary_id', 'vocabulary_content', 'vocabulary_media'
    ];
    public function user()
    {
        return $this->belongsTo('App\GroupVocabulary','group_vocabulary_id','id');
    }
}
