<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupVocabulary extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'group_vocabulary_title', 'group_vocabulary_thumbnail', 'group_quiz_id'
    ];

    /**
     * Get the vocabularies for the group vocabulary.
     */
    public function vocabularies()
    {
        return $this->hasMany('App\Vocabulary','group_vocabulary_id','id');
    }

    public function groupQuiz()
    {
        return $this->hasOne('App\GroupQuiz','group_quiz_id','id');   
    }

    

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
