<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Auth;
use App\Post;
use App\User;
use App\Vocabulary;

use Route;
class ShareDataComposer
{

    protected $currentNameRoute;
    
    public function __construct(Request $request)
    {
        if (Route::current() != null) {
            $this->currentNameRoute = Route::current()->getName();
        }
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'currentNameRoute' => $this->currentNameRoute
        ]);
    }
}