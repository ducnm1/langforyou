<?php

namespace App\Http\Middleware;

use Closure;
use Menu;

class FrontendTopMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('FrontendTopMenu', function($menu){

          $menu->add('Trang chủ');
          $menu->add('Từ vựng', array('route'  => array('post.listOfType', 'tu-vung')));
          $menu->add('Ngữ pháp', array('route'  => array('post.listOfType', 'ngu-phap')));
          $menu->add('Giải trí', array('route'  => array('post.listOfType', 'giai-tri')));

          $menu->add('Trắc nghiệm', array('route'  => array('group_quiz.list')));
          

        });
        return $next($request);
    }
}
