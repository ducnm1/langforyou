<?php


Route::group(['middleware' => ['web']], function () {
	Route::get('auth/facebook', [
		'as' => 'facebook',
		'uses' => 'UsersController@redirectToProvider'
	]);
	Route::get('auth/facebook/callback', [
		'as'=>'facebookCallback',
		'uses'=>'UsersController@handleProviderCallback'
	]);
	Route::get('ajaxGetPost',[
		'as'=>'ajaxGetPost',
		'uses'=>'PostsController@ajaxGetPost'
	]);
	Route::post('ajaxResultQuiz', [
		'as' => 'ajaxResultQuiz',
		'uses' => 'QuizzesController@plusScore'
	]);

	Route::post('ajaxGetQuizzes', [
		'as' => 'ajaxGetQuizzes',
		'uses' => 'GroupQuizzesController@ajaxGetQuizzes'
	]);

	Route::get('searchSuggest', [
		'as'=>'searchSuggest',
		'uses'=>'AjaxController@searchSuggest'
	]);

	Route::get('historyPost', [
		'as'=>'historyPost',
		'uses'=>'PostsController@historyPost'
	]);

	Route::get('orderUser', [
		'as'=>'orderUser',
		'uses'=>'UsersController@orderUser'
	]);
	Route::get('getAllUsers', [
		'as'=>'getAllUsers',
		'uses'=>'UsersController@getAllUsers'
	]);
	Route::get('getCurrentUser', [
		'as'=>'getCurrentUser',
		'uses'=>'UsersController@getCurrentUser'
	]);

	Route::get('getAllCats', [
		'as'=>'getAllCats',
		'uses'=>'CatsController@getAllCats'
	]);
	// Route::get('convert', [
	// 	'as'=>'convert',
	// 	'uses'=>'PostsController@convert'
	// ]);
	

	Route::get('listYoutubeAjax', [
		'as'=>'listYoutubeAjax',
		'uses'=>'YoutubesController@listYoutubeAjax'
	]);

	Route::get('getCatsNumberPosts', [
		'as'=>'getCatsNumberPosts',
		'uses'=>'CatsController@getCatsNumberPosts'
	]);



	Route::group(['middleware' => 'FrontendTopMenu'], function () {
    	Route::get('/', [
    		'as' => 'home',
    		'uses' => 'PostsController@index'
    	]);
    	/**
		 * Post
		 */
		// Route::get('load_more_post', [
		// 	'as'=> 'load.more.post',
		// 	'uses'=> 'PostsController@ajaxIndex'
		// ]);
		Route::get('tu-vung',[
			'as'  => 'post.listOfType',
			'uses' => 'PostsController@listOfType'
		]);
		Route::get('ngu-phap',[
			'as'  => 'post.listOfType',
			'uses' => 'PostsController@listOfType'
		]);
		Route::get('giai-tri',[
			'as'  => 'post.listOfType',
			'uses' => 'PostsController@listOfType'
		]);
		Route::get('standard',[
			'as'  => 'post.listOfType',
			'uses' => 'PostsController@listOfType'
		]);

		Route::get('post/user/{id}',[
			'as'  => 'post.listOfUser',
			'uses' => 'PostsController@listOfUser'
		])->where('id', '[0-9]+');

		Route::get('feature-post',[
			'as'  => 'post.feature',
			'uses' => 'PostsController@featurePosts'
		]);

		/**
		 * Quiz
		 */
		
		Route::get('trac-nghiem',[
			'as'  => 'group_quiz.list',
			'uses' => 'GroupQuizzesController@list_group_quiz'
		]);
		Route::get('trac-nghiem/{name}-{id}',[
			'as'  => 'group_quiz.show',
			'uses' => 'GroupQuizzesController@show'
		]);

		/**
		 * Vocabulary
		 */
		
		Route::get('tu-vung/{name}-{id}',[
			'as'  => 'vocabulary.show',
			'uses' => 'VocabulariesController@show'
		]);


		Route::get('search',[
			'as'=>'search',
			'uses' => 'SearchesController@search'
		]);

		/**
		 * Cat
		 */

		Route::get('chuyen-muc/{name}-{id}',[			
			'as'=>'category.show',
			'uses'=>'PostsController@listOfCat'
		]);
		/**
		 * Page
		 */
		Route::get('page/{name}-{id}',[			
			'as'=>'page.show',
			'uses'=>'PagesController@show'
		]);
		
	});
	Route::get('postsAjax',[
		'as'=>'listPostAjax',
		'uses' => 'PostsController@listPostAjax'
	]);
	Route::group(['middleware' => ['auth', 'DashboardSidebarMenu', 'CheckPermission'], 'prefix' => 'dashboard'], function () {
		/**
		 * Posts
		 */
		Route::get('posts',[
			'as'=>'listPost',
			'uses' => 'PostsController@listPost'
		]);

		Route::get('post/create',[			
			'as'=>'post.create',
			'uses'=>'PostsController@create'
		]);

		Route::post('post',[			
			'as'=>'post.store',
			'uses'=>'PostsController@store'
		]);


		Route::get('post/{id}/edit',[			
			'as'  => 'post.edit',
			'uses' => 'PostsController@edit'
		])->where('id', '[0-9]+');

		Route::put('post/{id}',[			
			'as'=>'post.update',
			'uses'=>'PostsController@update'
		])->where('id', '[0-9]+');

		Route::get('post/{id}/destroy',[			
			'as'  => 'post.destroy',
			'uses' => 'PostsController@destroy'
		]);

		Route::delete('posts/destroy_list_items',[		
			'as'  => 'posts.destroy_list_items',
			'uses' => 'PostsController@destroy_list_items'
		]);		

		/**
		 * Pages
		 */
		Route::get('pages',[
			'as'=>'pages.index',
			'uses' => 'PagesController@index'
		]);

		Route::get('page/create',[			
			'as'=>'page.create',
			'uses'=>'PagesController@create'
		]);

		Route::post('page',[			
			'as'=>'page.store',
			'uses'=>'PagesController@store'
		]);


		Route::get('page/{id}/edit',[			
			'as'  => 'page.edit',
			'uses' => 'PagesController@edit'
		])->where('id', '[0-9]+');

		Route::put('page/{id}',[			
			'as'=>'page.update',
			'uses'=>'PagesController@update'
		])->where('id', '[0-9]+');

		Route::get('page/{id}/destroy',[			
			'as'  => 'page.destroy',
			'uses' => 'PagesController@destroy'
		]);

		/**
		 * Cat
		 */
		Route::get('cats',[		
			'as'=>'cats.index',
			'uses'=>'CatsController@index'
		]);
		Route::get('cat/create',[				
			'as'=>'cat.create',
			'uses'=>'CatsController@create'
		]);

		Route::post('cat',[			
			'as'=>'cat.store',
			'uses'=>'CatsController@store'
		]);

		Route::get('cat/{id}/edit',[			
			'as'=>'cat.edit',
			'uses'=>'CatsController@edit'
		])->where('id', '[0-9]+');

		Route::put('cat/{id}',[			
			'as'=>'cat.update',
			'uses'=>'CatsController@update'
		])->where('id', '[0-9]+');

		Route::get('cat/{id}/destroy',[			
			'as'=>'cat.destroy',
			'uses'=>'CatsController@destroy'
		])->where('id', '[0-9]+');

		/**
		 * Quizzes
		 */
		
		Route::get('quiz/create',[		
			'as'=>'quiz.create',
			'uses'=>'QuizzesController@create'
		]);

		Route::post('quiz/store',[		
			'as'=>'quiz.store',
			'uses'=>'QuizzesController@store'
		]);

		Route::get('quiz/{id}/edit',[		
			'as'=>'quiz.edit',
			'uses'=>'QuizzesController@edit'
		])->where('id', '[0-9]+');

		Route::put('quiz/{id}',[		
			'as'=>'quiz.update',
			'uses'=>'QuizzesController@update'
		])->where('id', '[0-9]+');

		Route::get('quiz/{id}/destroy',[		
			'as'=>'quiz.destroy',
			'uses'=>'QuizzesController@destroy'
		])->where('id', '[0-9]+');

		/**
		 * Group quiz
		 */
		Route::get('group_quizzes',[		
			'as'=>'group_quizzes.index',
			'uses'=>'GroupQuizzesController@index'
		]);
		Route::get('group_quiz/{id}',[
			'as'=>'quizzes.index',
			'uses'=>'QuizzesController@index'
		])->where('id', '[0-9]+');
		Route::get('group_quiz/create',[		
			'as'=>'group_quiz.create',
			'uses'=>'GroupQuizzesController@create'
		]);

		Route::post('group_quiz',[		
			'as'=>'group_quiz.store',
			'uses'=>'GroupQuizzesController@store'
		]);

		Route::get('group_quiz/{id}/edit',[		
			'as'=>'group_quiz.edit',
			'uses'=>'GroupQuizzesController@edit'
		])->where('id', '[0-9]+');

		Route::put('group_quiz/{id}',[			
			'as'=>'group_quiz.update',
			'uses'=>'GroupQuizzesController@update'
		])->where('id', '[0-9]+');

		Route::get('group_quiz/{id}/destroy',[			
			'as'=>'group_quiz.destroy',
			'uses'=>'GroupQuizzesController@destroy'
		])->where('id', '[0-9]+');

		/**
		 * 
		 * Youtube
		 * 
		 */
		
		Route::get('youtubes',[
			'as'=>'youtubes.index',
			'uses' => 'YoutubesController@index'
		]);

		Route::get('youtube/create',[			
			'as'=>'youtube.create',
			'uses'=>'YoutubesController@create'
		]);
		Route::post('youtube',[			
			'as'=>'youtube.store',
			'uses'=>'YoutubesController@store'
		]);
		Route::get('youtube/{id}/edit',[			
			'as'=>'youtube.edit',
			'uses'=>'YoutubesController@edit'
		])->where('id', '[0-9]+');
		Route::put('youtube/{id}',[			
			'as'=>'youtube.update',
			'uses'=>'YoutubesController@update'
		])->where('id', '[0-9]+');
		Route::get('youtube/{id}/destroy',[			
			'as'=>'youtube.destroy',
			'uses'=>'YoutubesController@destroy'
		])->where('id', '[0-9]+');

		/**
		 * 
		 * Vocabulary
		 * 
		 */
		
		Route::get('vocabularys',[
			'as'=>'vocabularies.index',
			'uses' => 'VocabulariesController@index'
		]);

		Route::get('vocabulary/create',[			
			'as'=>'vocabulary.create',
			'uses'=>'VocabulariesController@create'
		]);
		Route::post('vocabulary',[			
			'as'=>'vocabulary.store',
			'uses'=>'VocabulariesController@store'
		]);
		Route::get('vocabulary/{id}/edit',[			
			'as'=>'vocabulary.edit',
			'uses'=>'VocabulariesController@edit'
		])->where('id', '[0-9]+');
		Route::put('vocabulary/{id}',[			
			'as'=>'vocabulary.update',
			'uses'=>'VocabulariesController@update'
		])->where('id', '[0-9]+');
		Route::get('vocabulary/{id}/destroy',[			
			'as'=>'vocabulary.destroy',
			'uses'=>'VocabulariesController@destroy'
		])->where('id', '[0-9]+');

		
		
		


		/**
		 * 
		 * Search
		 * 
		 */
		

		
		// Route::get('search',[
		// 	'as'=>'dashboard.search.post',
		// 	'uses' => 'SearchController@getSearchPostdashboard'
		// ]);


	});

	Route::group(['middleware' => ['auth', 'DashboardSidebarMenu'], 'prefix' => 'dashboard'], function () {
		/**
		 * 
		 * User
		 * 
		 */

		Route::get('users',[			
			'as'=>'users.index',
			'uses'=>'UsersController@index'
		]);

		Route::get('user/updateLearnLang',[			
			'as'=>'users.learn.lang',
			'uses'=>'UsersController@updateLearLang'
		]);

		Route::get('user/verify',[			
			'as'=>'users.sendemail',
			'uses'=>'UsersController@sendEmailVerify'
		]);
		Route::post('user/verify',[			
			'as'=>'users.verifyAccount',
			'uses'=>'UsersController@vefifyUser'
		]);

		Route::get('user/{id}/create',[
			
			'as'=>'user.create',
			'uses'=>'UsersController@create'
		])->where('id', '[0-9]+');

		Route::post('user',[
				
			'as'=>'user.store',
			'uses'=>'UsersController@store'
		]);

		Route::get('user/{id}/edit',[			
			'as'  => 'user.edit',
			'uses' => 'UsersController@edit'
		])->where('id', '[0-9]+');

		Route::put('user/{id}',[			
			'as'=>'user.update',
			'uses'=>'UsersController@update'
		])->where('id', '[0-9]+');

		Route::get('user/{id}/destroy',[
			
			'as'  => 'user.destroy',
			'uses' => 'UsersController@destroy'
		])->where('id', '[0-9]+');

		Route::get('user',[			
			'as'=>'user.profile',
			'uses'=>'UsersController@show'
		])->where('id', '[0-9]+');

	});

	
	/*
	*
	*Authen
	* 
	 */

	Route::auth();
    Route::get('dashboard', 'HomeController@index');

    Route::group(['middleware' => 'FrontendTopMenu'], function () {
  //   	Route::get('loai/{postType}',[
		// 	'as'  => 'post.listOfType',
		// 	'uses' => 'PostsController@listOfType'
		// ])->where('name', '[A-Za-z]+');
		Route::get('{name}-{id}',[
			'as'  => 'post.show',
			'uses' => 'PostsController@show'
		]);
		
	});
});