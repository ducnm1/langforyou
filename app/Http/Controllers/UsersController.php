<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Auth;
use Mail;
use Socialite;
class UsersController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();

        $authUser = $this->findOrCreateUser($user);
 
        Auth::login($authUser, true);
 
        return redirect()->route('home');
    }

    private function findOrCreateUser($facebookUser)
    {
        $authUser = User::where('email', $facebookUser->getEmail())->first();
 
        if ($authUser){
            return $authUser;
        }
 
        return User::create([
            'name' => $facebookUser->getName(),
            'email' => $facebookUser->getEmail()
        ]);
    }
    /**
     * Send an e-mail reminder to the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function sendEmailVerify(Request $request)
    {
        return view('emails.send');
    }

    public function vefifyUser(Request $request){
        $user = User::find($request->user_id);

        Mail::send('emails.verifyAccount', ['user' => $user], function ($m) use ($user) {
            $m->from('postmaster@sandboxec064e9aa11b4f83b69cedc9d9165b99.mailgun.org', 'Your Application');

            $m->to("duc91bk@gmail.com", "nguyen minh duc")->subject('Your Reminder!');
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users=User::orderBy('id')->paginate(20);
        return view('backends.pages.listUser', compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($id)
    {
        if (Auth::user()->level < 3) {
            return view('users.create');
        } else {
            dd("Bạn không có quyền thực hiện thao tác này.");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->level < 3) {
            $user = new User;

            $user->name=$request->name;

            $user->description=$request->description;

            $user->level=$request->level;

            $user->email=$request->email;

            $user->password=bcrypt($request->password);

            $user->save();

            return redirect()->route('users.index');
        } else {
            dd("Bạn không có quyền thực hiện thao tác này.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show()
    {
        $user=User::find(Auth::user()->id);
        return view('backends.pages.userProfile', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::user()->user_level > 5 ) {
            $user = User::find($id);           
            return view('backends.pages.editUser', compact('user'));
        } else {
            dd("Bạn không có quyền thực hiện thao tác này.");
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->user_level > 5) {
            $user=User::find($id);
            $user->user_level=$request->user_level;
            $user->save();

            return redirect()->route('users.index');
        } else {
            dd("Bạn không có quyền thực hiện thao tác này.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->user_level > 5) {
            $user = User::find($id);
            $user->destroy($id);
            return redirect()->route('users.index');
        } else {
            dd("Bạn không có quyền thực hiện thao tác này.");
        }
    }

    public function updateLearLang(Request $request){
        if ($request->ajax()) {
            $user = User::find(Auth::user()->id);
            $user->user_learn_lang = $request->user_learn_lang;
            $user->save();
            return 'Cập nhật thành công.';
        }
    }

    public function orderUser(){
        $users = User::orderBy('user_score','desc')->where('users.user_level', '<', 5)->take(5)->get()->toArray();   

        for ($i=0; $i < count($users); $i++) { 
            $users[$i]['email'] = md5($users[$i]['email']);
        }
        return $users;
    }

    public function getAllUsers(){
        $users = User::orderBy('id','ASC')
                ->where('users.user_level','>', 3)
                ->select('users.id', 'users.name')
                ->get()->toJson();
        return $users;
    }

    public function getCurrentUser(){
        $user = User::where('users.id','=', Auth::user()->id)
                ->select('users.id', 'users.name')
                ->get()->toJson();;
        return $user;
    }
}
