<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Vocabulary;
use Auth;
use Image;

class VocabulariesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vocabularies = Vocabulary
            ::orderBy('created_at','desc')
            ->join('users', 'vocabularies.user_id', '=', 'users.id')
            ->select('vocabularies.*','users.name')
            ->paginate(20);
        return view('backends.pages.listVocabulary', compact('vocabularies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backends.pages.createVocabulary');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'vocabulary_title' => 'required|unique:vocabularies|min:2|max:50',
            'vocabulary_content' => 'required',
            'vocabulary_media' => 'required',
            'vocabulary_ipa' => 'required',
            'vocabulary_thumbnail' => 'required'

        ]);
        $vocabulary = new Vocabulary;
        $vocabulary->user_id = Auth::user()->id;
        $vocabulary->vocabulary_title = trim($request->vocabulary_title);
        $vocabulary->vocabulary_ipa = trim($request->vocabulary_ipa);
        if ($request->hasFile('vocabulary_thumbnail')) {
            $vocabulary->vocabulary_thumbnail = $this->getThumbnail($request);
        }
        $vocabulary->vocabulary_content = $request->vocabulary_content;
        if ($request->hasFile('vocabulary_media')) {
            $vocabulary->vocabulary_media = $this->getAudio($request);
        }
        
        $vocabulary->post_id = trim(str_replace(" ", "", $request->post_id),',');        
        $vocabulary->save();

        return redirect()->route('vocabulary.edit', $vocabulary->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($name, $id)
    {
        $x = $name."-".$id;
        $x = explode("-", $x);        
        $id = $x[count($x)-1];
        unset($x[count($x)-1]);
        $name = implode("-", $x);

        $vocabulary = Vocabulary::find($id);
        if ($vocabulary == null) {
            return abort(404);
        }

        $originalTitle = $this->seoname($vocabulary->vocabulary_title);

        if ($name != $originalTitle) {
            return redirect()->route('vocabulary.show', ['name'=>$originalTitle, 'id'=>$id]);
        }
        return view('frontends.pages.detailVocabulary', compact('vocabulary'));
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id , Request $request)
    {
        $vocabulary = Vocabulary::find($id);
        if ($vocabulary->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {            
            return view('backends.pages.editVocabulary', compact('vocabulary'));
        } else {
            return abort(401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'vocabulary_title' => 'required|min:2|max:50',
            'vocabulary_content' => 'required'
        ]);

        $vocabulary = Vocabulary::find($id);
        if ($vocabulary->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {
            $vocabulary->vocabulary_title = trim($request->vocabulary_title);
            $vocabulary->vocabulary_ipa = trim($request->vocabulary_ipa);
            if ($request->hasFile('vocabulary_thumbnail')) {
                $vocabulary->vocabulary_thumbnail = $this->getThumbnail($request);
            }
            $vocabulary->vocabulary_content = $request->vocabulary_content;
            if ($request->hasFile('vocabulary_media')) {
                $vocabulary->vocabulary_media = $this->getAudio($request);
            }
            $vocabulary->post_id = trim(str_replace(" ", "", $request->post_id),',');
            $vocabulary->save();

            return redirect()->route('vocabulary.edit', $vocabulary->id);
        } else {
            return abort(401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get audio
     */
    public function getAudio($request)
    {
        $target_dir = "uploads/audios/";
        $file = $request->file('vocabulary_media');
        $file_name = time()."-".$file->getClientOriginalName();
        $file_extension =  $file->guessExtension();
        
        if(in_array($file_extension, array('jpg','png','jpeg', 'gif','mpga'))) {           
            $file->move($target_dir, $file_name);        
            $target_file = "/" . $target_dir.$file_name;        
            return $target_file;
        }
    }    

    public function getThumbnail($request){
        $target_dir = "uploads/vocabularies/";
        $file = $request->file('vocabulary_thumbnail');
        $file_name = time()."-".$file->getClientOriginalName();
        $file_extension =  $file->guessExtension();
        
        if(in_array($file_extension, array('jpg','png','jpeg', 'gif','mpga'))) {       
            $file->move($target_dir, $file_name);        
            $target_file =  $target_dir.$file_name;
            $image = Image::make($target_file)->resize(320, 180)->save();
            
            return "/" .$target_file;
        }
    }

    public function seoname($str){
        if(!$str) return false;
        $unicode = array(
            'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
            'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
            'd'=>array('đ'),
            'D'=>array('Đ'),
            'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
            'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
            'i'=>array('í','ì','ỉ','ĩ','ị'),
            'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
            'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),
            'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
            'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
            'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
            'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
            'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
            '-'=>array(' ','&quot;','.','-–-')
        );
        foreach($unicode as $nonUnicode=>$uni){
            foreach($uni as $value)
            $str = @str_replace($value,$nonUnicode,$str);
            $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
            $str = preg_replace("/-+-/","-",$str);
            $str = preg_replace("/^\-+|\-+$/","",$str);
        }
        return strtolower($str);
    }
}
