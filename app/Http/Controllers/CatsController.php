<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cat;
use App\Post;
use Auth;

use Carbon\Carbon;

class CatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return cats.index
     */
    public function index()
    {
        $cats=Cat
            ::orderBy('id' , 'desc')
            -> join('users', 'cats.user_id', '=', 'users.id')
            -> select('cats.*', 'users.name', 'users.user_level')
            -> paginate(20);

        return view('backends.pages.listCat', compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return cats.create
     */
    public function create()
    {
        $cats = Cat::all();
        $arCats = array("-1"=>"None");
        foreach ($cats as $cat) {
            $arCats[$cat->id] = $cat->cat_title;
        }
        return view('backends.pages.createCat', compact('arCats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CatFormRequest  $request
     * @return cats.index
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cat_title' => 'required|unique:cats|min:3|max:255',
        ]);

        $cat = new Cat;

        $cat->cat_title  = trim($request->cat_title);
        $cat->user_id   = Auth::user()->id;
        if ($request->cat_parent_id != -1) {
            $cat->cat_parent_id = $request->cat_parent_id;
        }
        
        $cat->save();

        return redirect()->route('cat.edit', $cat->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $cat = Cat::find($id);
        $posts = $cat->posts->toArray();
        $posts = array_reverse($posts);        

        return view('frontends.pages.listPostOfCat', compact('cat','posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return cats.edit
     */
    public function edit($id)
    {
        $cat = Cat::find($id);
        $user = $cat->user;
        if ($cat->user_id == Auth::user()->id || (Auth::user()->user_level > 3 && Auth::user()->user_level > $user->user_level)) {
            $cats = Cat::all();
            $arCats = array("-1"=>"None");
            foreach ($cats as $catSingle) {
                if ($catSingle->id != $id) {
                    $arCats[$catSingle->id] = $catSingle->cat_title;
                }
            }
            return view('backends.pages.editCat', compact('cat', 'arCats'));
        } else {
            return abort(401);
        }        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CatFormRequest  $request
     * @param  int  $id
     * @return cats.index
     */
    public function update(Request $request, $id)
    {
        $cat = Cat::find($id);
        $user = $cat->user;
        if ($cat->user_id == Auth::user()->id || (Auth::user()->user_level > 3 && Auth::user()->user_level > $user->user_level)) {         
            $cat->cat_title  = trim($request->cat_title);
            if ($request->cat_parent_id != -1) {
                $cat->cat_parent_id = $request->cat_parent_id;
            } else {
                $cat->cat_parent_id = -1;
            }
            $cat->save();
            return redirect()->route('cats.index');
        } else {
            return abort(401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return cats.index
     */
    public function destroy($id)
    {
        $cat = Cat::find($id);
        $user = $cat->user;
        if ($cat->user_id == Auth::user()->id || (Auth::user()->user_level > 3 && Auth::user()->user_level > $user->user_level)) {
            $cat->destroy($id);
            return redirect()->route('cats.index');
        } else {
            return abort(401);
        }
    }


    public function getAllCats(){
        $cats = Cat::orderBy('id','ASC')
                ->select('cats.id', 'cats.cat_title')
                ->get()->toJson();
        return $cats;
    }

    public function getCatsNumberPosts(){
        $currentTime = Carbon::now()->timestamp;

        $cats = Cat::orderBy('id','ASC')
                ->select('cats.id', 'cats.cat_title')
                ->get()->toArray();
        $ar = array();
        //var_dump($cats);
        for ($i=0; $i < count($cats) ; $i++) { 
            $cat = Cat::find($cats[$i]['id']);
            // $posts = $cat->posts->count();
            $posts = Post::
                    where('posts.cat_id', '=', $cats[$i]['id'])
                    ->where('posts.post_status','=', 'public')
                    ->where('posts.post_timer','<=', $currentTime)
                    ->select('posts.cat_id')
                    ->get()
                    ->count();

            if ($posts > 0 ) {
                array_push($ar,['id'=>$cats[$i]['id'], 'cat_title'=>$cats[$i]['cat_title'], 'posts'=>$posts]);
            }            
        }

        return json_encode($ar);
    }
}
