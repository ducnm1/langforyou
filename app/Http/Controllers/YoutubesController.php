<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Youtube;
use App\User;
use Auth;
use Excel;
class YoutubesController extends Controller
{
    public function index(Request $request)
    {   
    	return view('backends.pages.listYoutube');
    }
    public function listYoutubeAjax(Request $request)
    {
        $user = $request->user;
        if ($user == 0) {
            $youtubes=Youtube
            ::orderBy('id' , 'desc')
            -> join('users', 'youtubes.user_id', '=', 'users.id')            
            -> select('youtubes.id', 'youtubes.youtube_title', 'users.name')
            -> paginate(24)->toJson();
        } else {
            $youtubes=Youtube
            ::orderBy('id' , 'desc')
            -> join('users', 'youtubes.user_id', '=', 'users.id')  
            -> where('youtubes.user_id', '=', $user)          
            -> select('youtubes.id', 'youtubes.youtube_title', 'users.name')
            -> paginate(24)->toJson();
        }
        return $youtubes;
    }
    public function create(Request $request)
    {
    	return view('backends.pages.createYoutube');
    }
    public function store(Request $request)
    {
    	
    	$this->validate($request, [
            'youtube_title' => 'required|unique:youtubes|min:3|max:255',
            'youtube_url' => 'required'
        ]);
        $youtube = new Youtube;        

    	$youtube->user_id =  Auth::user()->id;
        $youtube->youtube_title = $request->youtube_title;
        $youtubeId = $this->get_youtube_id($request);
        $youtube->youtube_code_id = $youtubeId;
        $youtube->youtube_thumbnail = "https://i.ytimg.com/vi/". $youtubeId ."/mqdefault.jpg";
        if ($request->youtube_sub_en != null) {
            $youtube->youtube_sub_en = $this->getFile($request, 'youtube_sub_en');
            
        }
        if ($request->youtube_sub_vn != null) {
            $youtube->youtube_sub_vn = $this->getFile($request, 'youtube_sub_vn');

        }
        $youtube->save();

        return redirect()->route('youtube.edit', $youtube->id);
    }
    public function edit(Request $request, $id)
    {
        $youtube = Youtube::find($id);
  
        $arUsers = array();
            
        $users = User::orderBy('id','ASC')
                ->where('users.user_level','>', 3)
                ->select('users.id', 'users.name')
                ->get();
  

        if ($youtube->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {    	
    	   return view('backends.pages.editYoutube', compact('youtube', 'users'));
        } else {
            return abort(401);
        }
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'youtube_title' => 'required|min:3|max:255'
        ]);

    	$youtube = Youtube::find($id);

        if ($youtube->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {
        	$youtube->youtube_title = $request->youtube_title;
        	$youtubeId = $this->get_youtube_id($request);
            $youtube->youtube_code_id = $youtubeId;
            $youtube->youtube_thumbnail = "https://i.ytimg.com/vi/". $youtubeId ."/mqdefault.jpg";  
            if ($request->youtube_sub_en != null) {
                $youtube->youtube_sub_en = $this->getFile($request, 'youtube_sub_en');
            }
            if ($request->youtube_sub_vn != null) {
                $youtube->youtube_sub_vn = $this->getFile($request, 'youtube_sub_vn');
            }

            if ($request->has('change_author')) {
                $youtube->user_id = $request->change_author;
            }

            $youtube->save();
            return redirect()->route('youtubes.index');
        } else {
            return abort(401);
        } 
    }
    public function destroy(Request $request, $id)
    {
    	$youtube = Youtube::find($id);
        if ($youtube->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {
        	$youtube->destroy($id);
        	return redirect()->route('youtubes.index');
        } else {
            return abort(401);
        }
    }
    public function get_youtube_id($request){
        $url = $request->youtube_url;                 
        $queryString = parse_url($url, PHP_URL_QUERY);
        parse_str($queryString, $params);

        return $params['v'];
    }

    public function getFile($request, $sub){       
        $file = $request->file($sub);
        $file_name = $file->getClientOriginalName();
        $file_extension =  $file->guessExtension();
        
        if(in_array($file_extension, array('xls','xlsx'))) {
            $results = Excel::load($file, function($reader) {

            })->get()->toArray()[0];

            $subTitle = '';
            $index = 0;
            foreach ($results as $result) {
                $obj = $result[0];
                if (str_contains($obj, '-->')) {

                    $arTimeStart = explode(" ", $obj);
                    $arTimeStart = explode(":", $arTimeStart[0]);
                    $hourStart = $arTimeStart[0];
                    $minuteStart = $arTimeStart[1];
                    $secondsStart = $arTimeStart[2];
                    $timestampStart = $hourStart * 3600 + $minuteStart * 60 + $secondsStart;

                    $arTimeEnd = explode(" ", $obj);
                    $arTimeEnd = explode(":", $arTimeEnd[2]);
                    $hourEnd = $arTimeEnd[0];
                    $minuteEnd = $arTimeEnd[1];
                    $secondsEnd = $arTimeEnd[2];
                    $timestampEnd = $hourEnd * 3600 + $minuteEnd * 60 + $secondsEnd;

                    $index == 0 ? $subTitle .= '<p class="hide" data-time-start="' : $subTitle .= '</p><p class="hide" data-time-start="';
                    $subTitle .= $timestampStart;
                    $subTitle .= '" data-time-end="';
                    $subTitle .= $timestampEnd;
                    $subTitle .= '">';
                } elseif(is_string($obj)) {
                    $subTitle .= $obj . " ";
                }
                $index++;
            }
            $subTitle .= '</p>';
            return $subTitle;           
        }
        return "";
    }

}
