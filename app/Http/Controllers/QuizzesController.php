<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Quiz;
use App\Answer;
use App\GroupQuiz;
use App\User;
use Auth;
use Image;
use Carbon\Carbon;
use Route;

class QuizzesController extends Controller
{
    public function index($id)
    {
        $quizzes = Quiz::orderBy('created_at','desc')
            ->where('quizzes.group_quiz_id', '=', $id)
            ->select('quizzes.id','quizzes.quiz_title')
            ->paginate(15);

        $ar_quizzes = Quiz::orderBy('created_at','desc')
            ->where('quizzes.group_quiz_id', '=', $id)
            ->select('quizzes.id','quizzes.quiz_title')
            ->get()->toArray();

        $ar_quiz_id = array();
        for ($i=0; $i < count($ar_quizzes); $i++) { 
            array_push($ar_quiz_id, $ar_quizzes[$i]["id"]);
        }

        $answers = Answer::whereIn('quiz_id', $ar_quiz_id)
            ->select('answers.answer_title', 'answers.quiz_id','answers.answer_is_correct')
            ->get();

        $group_quiz_title = GroupQuiz::where('group_quizzes.id', '=', $id)
            ->select('group_quizzes.id', 'group_quizzes.group_quiz_title')
            ->get()[0]->group_quiz_title;
   
       
        return view('backends.pages.listQuiz', compact('quizzes', 'group_quiz_title', 'answers'));
    }

    public function create()
    {
        $group_quizzes = GroupQuiz
            ::orderBy('created_at','desc')
            ->where('user_id', '=', Auth::user()->id)
            ->select('group_quizzes.*')
            ->get();
        $arGroupQuizzes = array();
        foreach ($group_quizzes as $group_quiz) {
            $arGroupQuizzes[$group_quiz->id] = $group_quiz->group_quiz_title;
        }

        return view('backends.pages.createQuiz', compact('arGroupQuizzes'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'quiz_title' => 'required|min:3|max:255',
            'group_quiz_id' => 'required',
        ]);
        $group_quiz_id = $request->group_quiz_id;
        $quiz_type = $request->quiz_type;

        /**
         * table quizzes
         * @var quiz
         */

        $quiz = new Quiz;

        $quiz->quiz_title = trim($request->quiz_title);
        $quiz->quiz_description = $request->quiz_description;
        if ($request->hasFile("quiz_media")) {
            $quiz->quiz_media = $this->getThumbnail($request, "quiz_media");
        }  
        if ($request->hasFile("quiz_thumbnail")) {
            $quiz->quiz_thumbnail = $this->getThumbnail($request, "quiz_thumbnail");
        }       
        $quiz->group_quiz_id = $group_quiz_id;        
        $quiz->quiz_type = $quiz_type;

        $quiz->user_id = Auth::user()->id;

        $quiz->save();
        
        /**
         * table answers
         * @var 
         */
        
        $is_correct  = $request->answer_is_correct;        

        $LastInsertId = $quiz->id;
        $last_quiz = Quiz::find($LastInsertId);

        if ($quiz_type == "select") {
            for ($i=0; $i < 4; $i++) {
                $nameAnswer = "answers_".$i;
                $answer_title = trim($request->$nameAnswer);
                $thumbnail = 'answer_thumbnail'.$i;
                if ($request->hasFile($thumbnail) && $request->file($thumbnail)->isValid()) {
                    $answerThumbnail = $this->getThumbnail($request, $thumbnail);
                } else {
                    $answerThumbnail = "";
                }
                $is_correct == $i ? $answer_is_correct = 1 : $answer_is_correct = 0;
                
                $last_quiz->answers()->saveMany([
                    new Answer([
                        'answer_title'   => $answer_title,
                        'answer_thumbnail'   => $answerThumbnail,
                        'answer_is_correct' => $answer_is_correct
                        ])
                ]);
            }
        } else if ($quiz_type == "input") {
            $num_answer = $request->num_answer;
            for ($i=0; $i < $num_answer ; $i++) {
                $nameAnswer = "answer_text_".$i;
                $answerTitle = trim($request->$nameAnswer);
                
                if (!empty($answerTitle)) {
                    $last_quiz->answers()->saveMany([
                        new Answer([
                            'answer_title'   => $answerTitle,
                            'answer_is_correct' => 1
                            ])                        
                    ]);
                }
            }
        }

        return redirect()->route('quiz.edit', $quiz->id);

    }

    public function edit($id)
    {
        $quiz = Quiz::find($id);

        if ($quiz->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {
            $answers = $quiz->answers;
            $group_quizzes = GroupQuiz
                ::orderBy('created_at','desc')
                ->where('user_id', '=', Auth::user()->id)
                ->select('group_quizzes.*')
                ->get();

            return view('backends.pages.editQuiz', compact('quiz', 'group_quizzes', 'answers'));
        } else {
            return abort(401);
        }     
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'quiz_title' => 'required|min:3|max:255',
            'group_quiz_id' => 'required',
        ]);

        $group_quiz_id = $request->group_quiz_id;
        $quiz_type = $request->quiz_type;

        /**
         * table quizzes
         * @var quiz
         */

        $quiz = Quiz::find($id);
        if ($quiz->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {
            $quiz->quiz_title = trim($request->quiz_title); 
            $quiz->quiz_description = $request->quiz_description;
            if ($request->hasFile("quiz_media")) {
                $quiz->quiz_media = $this->getThumbnail($request, "quiz_media");
            }            
            if ($request->hasFile("quiz_thumbnail")) {
                $quiz->quiz_thumbnail = $this->getThumbnail($request, "quiz_thumbnail");
            }  
            $quiz->group_quiz_id = $group_quiz_id;        
            $quiz->save();
            
            /**
             * table answers
             * @var 
             */

            $is_correct  = $request->answer_is_correct;
            $LastInsertId = $quiz->id;
            $last_quiz = Quiz::find($LastInsertId);


            if ($quiz_type == "select") {
                for ($i=0; $i < 4; $i++) {
                    $nameAnswer = "answers_".$i;
                    $answer_title = $request->$nameAnswer;
                    $thumbnail = 'answer_thumbnail'.$i;
                    $request->hasFile($thumbnail) ? $answerThumbnail = $this->getThumbnail($request, $thumbnail) : $answerThumbnail = "";
                    $is_correct == $i ? $answer_is_correct = 1 : $answer_is_correct = 0;                
                    $name_id_answer = "id_answers_".$i;  
                    
                    $answer = Answer::find($request->$name_id_answer);

                    $answer->answer_title = trim($answer_title);
                    $answer->answer_is_correct = $answer_is_correct;
                    $answer->answer_thumbnail = $answerThumbnail;
                    
                    $last_quiz->answers()->saveMany([
                        $answer,             
                    ]);
                }
            } else if ($quiz_type == "input") {
                $answers = $last_quiz->answers;
                $arIdAnswers = array();

                foreach ($answers as $answer) {
                    array_push($arIdAnswers, $answer->id);
                }
                for ($i=0; $i < count($arIdAnswers) ; $i++) { 
                    $answer = Answer::find($arIdAnswers[$i]);
                    $answer->destroy($answer->id);
                }

                $num_answer= $request->num_answer;

                for ($i=0; $i < $num_answer ; $i++) {
                    $nameAnswer = "answer_text_".$i;
                    $answerTitle = trim($request->$nameAnswer);
                    if (!empty($answerTitle)) {
                        $last_quiz->answers()->saveMany([
                            new Answer([
                                'answer_title'   => $answerTitle,
                                'answer_is_correct' => 1
                                ])                        
                        ]);
                    }
                }
            }        

            return redirect()->route('quizzes.index', $group_quiz_id);
        } else {
            return abort(401);
        } 
    }

    public function destroy($id)
    {
        $quiz = Quiz::find($id);

        if ($quiz->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {
            $group_quiz = $quiz->group_quizzes;
            $answers = $quiz->answers;
            $quiz->destroy($id);
            foreach ($answers as $answer) {
                $answer->destroy($answer->id);
            }
            return redirect()->route('quizzes.index', $group_quiz->id);
        } else {
            return abort(401);
        }    
    }

    public function getThumbnail($request,$thumbnail){
        if ($thumbnail != "quiz_media") {
            $target_dir = "uploads/images/";            
        } else {
            $target_dir = "uploads/audios/";
        }
        
        $file = $request->file($thumbnail);
        $file_name = time()."-".$file->getClientOriginalName();
        $file_extension =  $file->guessExtension();
        
        if(in_array($file_extension, array('jpg','png','jpeg', 'gif','mpga'))) {      
            $file->move($target_dir, $file_name);        
            $target_file = $target_dir.$file_name;
            if (str_contains($thumbnail, 'answer_thumbnail')) {
                $image = Image::make($target_file)->resize(200, 200)->save();
            }
            
            return "/" . $target_file;
        }
    }

    public function plusScore(Request $request) 
    {
        $groupQuizId = $request->groupQuizId;
        $currentTime = Carbon::now();
        $newTimestamp = $currentTime->minute * $currentTime->hour;
        $verifyTime = false;

        if (Auth::check()) {
            if ($groupQuizId != 0) {
                $latestSingleQuiz = Auth::user()->id . "LatestSingleQuiz" . $groupQuizId;
                if (!$request->session()->has($latestSingleQuiz)) {
                    $request->session()->put($latestSingleQuiz, $newTimestamp);
                    $verifyTime = true;
                } else {
                    $oldTimestamp = $request->session()->get($latestSingleQuiz);
                    ($newTimestamp-$oldTimestamp) > 2 ? $verifyTime = true : $verifyTime = false;
                }
            } else {
                $latestAllQuiz = Auth::user()->id . "LatestAllQuiz";
                if (!$request->session()->has($latestAllQuiz)) {
                    $request->session()->put($latestAllQuiz, $newTimestamp);
                    $verifyTime = true;
                } else {
                    $oldTimestamp = $request->session()->get($latestAllQuiz);
                    ($newTimestamp-$oldTimestamp) > 2 ? $verifyTime = true : $verifyTime = false;
                }
            }    
        } else {
            $verifyTime = true;
        }
        

        if ($verifyTime == true) {        
            $jsonResult = $request->jsonResult;
            
            for ($i=0; $i < count($jsonResult) ; $i++) 
            { 
                $quiz = Quiz::find($jsonResult[$i]['quiz_id']);
                $answer = Answer::find($jsonResult[$i]['answer_id']);        

                if ($quiz->quiz_type == 'select') {
                    if ($jsonResult[$i]['quiz_id'] == $answer->quiz_id &&
                        $answer->answer_is_correct == 1 ) {
                        continue;
                    } else {                        
                        return ["status"=>2, "notify"=>"Hệ thống nhận thấy có sai xót trong bài kiểm tra của bạn. Bạn vui lòng làm lại bài kiểm tra để được tích lũy điểm."];
                    }
                } else {
                    $answerFromUser = $this->stringFormat($jsonResult[$i]['answer_from_user']);

                    $answerTitle = $this->stringFormat($answer->answer_title);

                    if ($jsonResult[$i]['quiz_id'] == $answer->quiz_id &&
                        $answerFromUser == $answerTitle
                        ) 
                    {
                        continue;
                    } else {
                        return ["status"=>2, "notify"=>"Hệ thống nhận thấy có sai xót trong bài kiểm tra của bạn. Bạn vui lòng làm lại bài kiểm tra để được tích lũy điểm."];
                    }
                }
                
            }

            if (Auth::check()) {

                $user = User::find(Auth::user()->id);
                
                $currentTime = $currentTime->month . $currentTime->day;

                // set session history group quiz
                $oldUserHistory = explode(',', $user->user_history);
                if ($groupQuizId != 0) {
                    $idHistory = "group_quiz_" . $groupQuizId;
                    if (!in_array($idHistory, $oldUserHistory)) {
                        array_push($oldUserHistory, $idHistory);
                    } else {
                        for ($j=0; $j < count($oldUserHistory); $j++) { 
                            if ($oldUserHistory[$j] == $idHistory) {
                                unset($oldUserHistory[$j]);
                                array_push($oldUserHistory, $idHistory);
                                break;
                            }
                        }
                    }
                } else {               
                    $hasAllGroupQuiz = false;
                    for ($i=0; $i < count($oldUserHistory); $i++) { 
                        if (starts_with($oldUserHistory[$i], 'all_group_quiz_')) {
                            $numberComplete = trim($oldUserHistory[$i], 'all_group_quiz_') + 1;
                            $oldUserHistory[$i] = 'all_group_quiz_' . $numberComplete;
                            $hasAllGroupQuiz = true;
                            break;
                        }
                    }
                    if ($hasAllGroupQuiz == false) {
                        array_push($oldUserHistory, 'all_group_quiz_1');
                    }
                }
                
                $oldUserHistory = implode(",", $oldUserHistory);
                $user->user_history = $oldUserHistory;
                $user->save();


                // set session so lan duoc cong diem trong ngay
                $verifyNUmber = false;
                $sessionQuiz = "sessionQuiz:" . Auth::user()->id . "," . $groupQuizId;
                
                if (!$request->session()->has($sessionQuiz)) 
                {
                    $request->session()->put($sessionQuiz, $currentTime . "," . 1);
                    $verifyNUmber = true;
                }
                else 
                {
                    $oldSession = explode(",", $request->session()->get($sessionQuiz));

                    $oldIime = $oldSession[0];
                    $oldNumber = $oldSession[1];

                    if ($currentTime == $oldIime && $oldNumber < 3) {
                        $oldNumber += 1;
                        $request->session()->put($sessionQuiz, $currentTime . "," . $oldNumber);
                        $verifyNUmber = true;
                    } 
                    elseif($currentTime == $oldIime && $oldNumber >= 3) 
                    {
                        return ["status"=>1, "notify"=>"Chúc mừng bạn đã hoàn thành bài trắc nghiệm. Bạn đã vượt quá số lần được cộng điểm cho phép với bài trắc nghiệm này trong ngày."];
                    } 
                    elseif($currentTime != $oldIime)
                    {
                        $request->session()->put($sessionQuiz, $currentTime . "," . 1);
                        $verifyNUmber = true;
                    }

                }

                if ($verifyNUmber == true) {
                    $user->user_score += 10;
                    $userHistory = explode(",", $user->user_history);
                    $numberGroupQuiz = 0;
                    $numberAllGroupQuiz = 0;
                    for ($i=0; $i < count($userHistory); $i++) { 
                        if (starts_with($userHistory[$i], 'group_quiz_')) {
                            $numberGroupQuiz += 1;
                        }

                        if (starts_with($userHistory[$i], 'all_group_quiz_')) {
                            $numberAllGroupQuiz += 1;
                        }
                    }


                    if ($user->user_score >= 2000 && $numberGroupQuiz >= 160 && $numberAllGroupQuiz >= 40) {
                        $user->user_level = 5; 
                    }
                    elseif ($user->user_score >= 1000 && $numberGroupQuiz >= 80 && $numberAllGroupQuiz >= 20) {
                        $user->user_level = 4;   
                    }
                    elseif ($user->user_score >= 500 && $numberGroupQuiz >= 40 && $numberAllGroupQuiz >= 10) {
                        $user->user_level = 3;  
                    }
                    elseif ($user->user_score >= 250 && $numberGroupQuiz >= 20 && $numberAllGroupQuiz >= 5) {
                        $user->user_level = 2;
                    }


                    $user->save();

                    return ["status"=>1, "notify"=>"Chúc mừng bạn đã hoàn thành bài trắc nghiệm. Bạn được cộng thêm 10 điểm."];
                }  
            } else {
                return ["status"=>1, "notify"=>"Chúc mừng bạn đã hoàn thành bài trắc nghiệm. Bạn hãy <a href='/login'>đăng</a> nhập để lưu lại quá trình học tập của mình và sử dụng các chức năng khác của website"];
            }
        } else {
            return ["status"=>2, "notify"=>"Bạn làm quá nhanh. Vui lòng làm chậm lại."];
        }
    }
    public function stringFormat($str){

        if(!$str) return false;
        $unicode = array(
            'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
            'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
            'd'=>array('đ'),
            'D'=>array('Đ'),
            'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
            'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
            'i'=>array('í','ì','ỉ','ĩ','ị'),
            'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
            'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),
            'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
            'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
            'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
            'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
            'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
            '-'=>array(' ','&quot;','.','-–-'),
            'toi'=>array('tao','minh','tui'),
            'ban'=>array('may')
        );
        foreach($unicode as $nonUnicode=>$uni){
            foreach($uni as $value)
            $str = @str_replace($value,$nonUnicode,$str);
            $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
            $str = preg_replace("/-+-/","-",$str);
            $str = preg_replace("/^\-+|\-+$/","",$str);
        }
        return strtolower($str);
    }  
}