<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Page;
use Auth;
use App\User;
use Image;
use Carbon\Carbon;


class PagesController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $pages = Page
            ::orderBy('id','desc')
            ->join('users', 'pages.user_id', '=', 'users.id')
            ->select('pages.*','users.name','users.user_level')
            ->paginate(20);

        for ($i=0; $i < count($pages) ; $i++) { 
            $pages[$i]['page_title_unicode'] = $this->seoname($pages[$i]['page_title']);
        }
        return view('backends.pages.listPage', compact('pages'));       
    }

    public function create()
    {
        return view('backends.pages.createPage');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'page_title' => 'required|unique:pages|min:3|max:255',
        ]);

        $page = new Page;

        $page->page_title = trim($request->page_title);

        if ($request -> has('page_content')) {
            $page->page_content = $request->page_content;
        }

        if ($request->hasFile('page_thumbnail') && $request->file('page_thumbnail')->isValid()) {
            $page->page_thumbnail = $this->getThumbnail($request);
        }
       
        $page->user_id = Auth::user()->id;     

        $page->page_lang = $request->page_lang;

        $page->save();
        $request->session()->flash('status_action_post', 'Bài viết được tạo thành công');

        return redirect()->route('page.edit', $page->id);
    }

    public function show($name, $id)
    {  
        $x = $name."-".$id;
        $x = explode("-", $x);        
        $id = $x[count($x)-1];
        unset($x[count($x)-1]);
        $name = implode("-", $x);

        $page = Page::find($id);
        if ($page == null) {
            return abort(404);
        }

        $originalTitle = $this->seoname($page->page_title);

        if ($name != $originalTitle) {
            return redirect()->route('page.show', ['name'=>$originalTitle, 'id'=>$id]);
        }
        
        return view('frontends.pages.detailPage', compact('page')); 
    }

    public function edit($id, Request $request)
    {
        $page = Page::find($id);
        $user = $page->user;

        if ($page->user_id == Auth::user()->id || Auth::user()->user_level > $user->user_level) {
            return view('backends.pages.editPage', compact('page'));         
        } else {
            return abort(401);
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'page_title' => 'required|min:3|max:255',
        ]);
        $page = Page::find($id);   
        $user = $page->user;
        if ($page->user_id == Auth::user()->id || Auth::user()->user_level > $user->user_level) {            
            
            $page->page_title = trim($request->page_title);

            if ($request -> has('page_content')) {
                $page->page_content = $request->page_content;
            }

            if ($request->hasFile('page_thumbnail')) {
                $page->page_thumbnail = $this->getThumbnail($request);
            }
            $page->page_lang = $request->page_lang;
            $page->save();
            $request->session()->flash('status_action_post', 'Bài viết được cập nhật thành công');
            return redirect()->route('page.edit', $id);
        } else {
            return abort(401);
        }
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        $user = $page->user;
        if ($page->user_id == Auth::user()->id || Auth::user()->user_level > $user->user_level) { 
            $page->destroy($id);
            return redirect()->route('pages.index');
        } else {
            return abort(401);
        }        
    }

    public function getThumbnail($request){
        $target_dir = "uploads/images/";
        $file = $request->file('page_thumbnail');
        $file_name = time()."-".$file->getClientOriginalName();
        $file_extension =  $file->guessExtension();
        
        if(in_array($file_extension, array('jpg','png','jpeg', 'gif','mpga'))) {
            $file->move($target_dir, $file_name);        
            $target_file = $target_dir.$file_name;
            $image = Image::make($target_file)->resize(320, 180)->save();
        
            return "/" . $target_file;
        }
    }

    public function seoname($str){
        if(!$str) return false;
        $unicode = array(
            'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
            'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
            'd'=>array('đ'),
            'D'=>array('Đ'),
            'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
            'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
            'i'=>array('í','ì','ỉ','ĩ','ị'),
            'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
            'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),
            'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
            'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
            'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
            'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
            'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
            '-'=>array(' ','&quot;','.','-–-')
        );
        foreach($unicode as $nonUnicode=>$uni){
            foreach($uni as $value)
            $str = @str_replace($value,$nonUnicode,$str);
            $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
            $str = preg_replace("/-+-/","-",$str);
            $str = preg_replace("/^\-+|\-+$/","",$str);
        }
        return strtolower($str);
    }  
}