<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\GroupQuiz;
use App\Quiz;
use App\Answer;
use Auth;
use Image;

class GroupQuizzesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return 
     */
    public function index()
    {        
        if (Auth::user()->user_level != 7 && Auth::user()->user_level != 6) {
            $group_quizzes = GroupQuiz
                ::orderBy('id', 'desc')
                ->join('users', 'group_quizzes.user_id', '=', 'users.id')
                ->where('group_quizzes.user_id', '=', Auth::user()->id)
                ->select('group_quizzes.*','users.name')
                ->paginate(20);
        }
        else 
        {
            $group_quizzes = GroupQuiz
                ::orderBy('id', 'desc')
                ->join('users', 'group_quizzes.user_id', '=', 'users.id')
                ->select('group_quizzes.*','users.name')
                ->paginate(20);
        }


        $ar_group_quizzes = GroupQuiz
            ::orderBy('id', 'desc')
            ->join('users', 'group_quizzes.user_id', '=', 'users.id')
            ->select('group_quizzes.*')
            ->get()->toArray();



        $ar_group_id = array();
        foreach ($ar_group_quizzes as $key => $value) {
            foreach ($value as $key1 => $value1) {
                if ($key1 == 'id') {
                    array_push($ar_group_id, $value1);    
                }                
            }
        }

        $ar_number_quiz = array();
        foreach ($ar_group_id as $key => $value) {
            $countQuiz = Quiz
            ::join('group_quizzes', 'quizzes.group_quiz_id', '=', 'group_quizzes.id')
            ->where('quizzes.group_quiz_id', '=', $value)
            ->select('quizzes.*')            
            ->get()->count();    
            $ar_number_quiz[$value] = $countQuiz;
        }

        return view('backends.pages.listGroupQuiz', compact('group_quizzes', 'ar_number_quiz'));
    }

    public function list_group_quiz(){
        $group_quizzes = GroupQuiz
            ::orderBy('id')
            ->select('group_quizzes.*')
            ->take(24)
            ->get()->toArray();

        for ($i=0; $i < count($group_quizzes) ; $i++) { 
            $group_quizzes[$i]['group_quiz_title_unicode'] = $this->seoname($group_quizzes[$i]['group_quiz_title']);
        }

        return view('frontends.pages.listGroupQuiz', compact('group_quizzes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {     
        return view('backends.pages.createGroupQuiz');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $group_quiz = new GroupQuiz;

        $group_quiz->user_id = Auth::user()->id;
        $group_quiz->group_quiz_title = trim($request->group_quiz_title);
        $group_quiz->group_quiz_description = trim($request->group_quiz_description);        
        $group_quiz->group_quiz_parent_id = $request->group_quiz_parent_id;

       
        
        if ($request->file('group_quiz_thumbnail') != null) {
            $group_quiz->group_quiz_thumbnail = $this->getThumbnail($request);
        }
        $group_quiz->group_quiz_lang = $request->group_quiz_lang;

        $group_quiz->save();


        return redirect()->route('group_quiz.edit', $group_quiz->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($name, $id, Request $request)
    {
        $x = $name."-".$id;
        $x = explode("-", $x);        
        $id = $x[count($x)-1];
        unset($x[count($x)-1]);
        $name = implode("-", $x);

        $groupQuiz = GroupQuiz::find($id);
        $user = $groupQuiz->user;

        $originalGroupTitle = $this->seoname($groupQuiz->group_quiz_title);

        if ($name != $originalGroupTitle) {
            return redirect()->route('group_quiz.show', ['name'=>$originalGroupTitle, 'id'=>$id]);
        }

        return view('frontends.pages.detailGroupQuiz', compact('groupQuiz', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $group_quiz=GroupQuiz::find($id);

        if ($group_quiz->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {
            return view('backends.pages.editGroupQuiz', compact('group_quiz'));
        } else {
            return abort(401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $group_quiz = GroupQuiz::find($id);
        
        if ($group_quiz->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {
            $group_quiz->group_quiz_title = trim($request->group_quiz_title);
            $group_quiz->group_quiz_description = trim($request->group_quiz_description);        
            $group_quiz->group_quiz_parent_id = $request->group_quiz_parent_id;
            
            if ($request->file('group_quiz_thumbnail') != null) {
                $group_quiz->group_quiz_thumbnail = $this->getThumbnail($request);
            }
            $group_quiz->group_quiz_lang = $request->group_quiz_lang;
            $group_quiz->save();

            return redirect()->route('group_quizzes.index');
        } else {
            return abort(401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $group_quiz=GroupQuiz::find($id);

        if ($group_quiz->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {
            $group_quiz->destroy($id);
            return redirect()->route('group_quizzes.index');
        } else {
            return abort(401);
        }   
    }
        
    public function ajaxGetQuizzes(Request $request) {
        
        $groupQuizId = $request->groupQuizId;
        if ($groupQuizId != 0) {
            $groupQuiz = GroupQuiz::find($groupQuizId);

            if ($groupQuiz != null) {        
                $quizzes = array();
                $childGroupQuiz = $groupQuiz->groupQuizzes;
                if ($childGroupQuiz != null) {
                    $arIdChildGroupQuiz = array($groupQuiz->id);

                    $childGroupQuiz = $groupQuiz->groupQuizzes->toArray();

                    while (count($childGroupQuiz) > 0) {
                        foreach ($childGroupQuiz as $key => $value) {
                            array_push($arIdChildGroupQuiz, $value['id']);                
                            if (GroupQuiz::find($value['id'])->groupQuizzes != null) {
                                $childGroupQuiz = GroupQuiz::find($value['id'])->groupQuizzes->toArray();
                            }
                        }
                    }

                    for ($i=0; $i < count($arIdChildGroupQuiz) ; $i++) { 
                        $quizzes = array_merge($quizzes, GroupQuiz::find($arIdChildGroupQuiz[$i])->quizzes->toArray());
                    }
                } else {
                    $quizzes = array_merge($quizzes, $groupQuiz->quizzes->toArray());
                }
                

                $jsonDataQuiz = '{';
                $jsonDataQuiz .= '"title": "' . $groupQuiz->group_quiz_title . '",';
                $jsonDataQuiz .= '"id":"'. $groupQuiz->id .'",';
                if (Auth::check()) 
                {
                    $jsonDataQuiz .= '"user_level":"'. Auth::user()->user_level .'",';
                }  
                $jsonDataQuiz .= '"quizs":[';
                $countQuiz = count($quizzes);
                for ($i=0; $i < $countQuiz; $i++) { 
                    if ($i == 0) {
                        $jsonDataQuiz .= '{';
                    } else {
                        $jsonDataQuiz .= ',{';
                    }   
                        $jsonDataQuiz .= '"id":"'. $quizzes[$i]["id"] .'",';                 
                        $jsonDataQuiz .= '"title":"'. $quizzes[$i]["quiz_title"] .'",';
                        $jsonDataQuiz .= '"type":"'. $quizzes[$i]["quiz_type"] .'",';
                        $jsonDataQuiz .= '"description":"'. $quizzes[$i]["quiz_description"] .'",';
                        $jsonDataQuiz .= '"media":"'. $quizzes[$i]["quiz_media"] .'",';
                        $jsonDataQuiz .= '"thumbnail":"'. $quizzes[$i]["quiz_thumbnail"] .'",';
                        $jsonDataQuiz .= '"answers":[';
                            $answers= Quiz::find($quizzes[$i]["id"])->answers->toArray();
                            $countAnswer = count($answers);
                            for ($j=0; $j < $countAnswer ; $j++) { 
                                if ($j == 0) {
                                    $jsonDataQuiz .= '{';
                                } else {
                                    $jsonDataQuiz .= ',{';
                                }
                                $jsonDataQuiz .= '"id":"'. $answers[$j]["id"] .'",';
                                $jsonDataQuiz .= '"title":"'. $answers[$j]["answer_title"] .'",';
                                $jsonDataQuiz .= '"thumbnail":"'. $answers[$j]["answer_thumbnail"] .'",';
                                $jsonDataQuiz .= '"is_correct":"'. $answers[$j]["answer_is_correct"] .'"';
                                $jsonDataQuiz .= '}';
                            }
                        $jsonDataQuiz .= ']';
                    $jsonDataQuiz .= '}';
                }

                $jsonDataQuiz .= ']';
                $jsonDataQuiz .= '}';
            }
        } else {                
            $quizzes = array();

            if (Auth::check()) {
                $userHistory = explode(",", Auth::user()->user_history);
                for ($i=0; $i < count($userHistory) ; $i++) { 
                    if (starts_with($userHistory[$i], 'group_quiz_')) {
                        $groupQuiz = GroupQuiz::find(trim($userHistory[$i], 'group_quiz_'));
                        $quiz = $groupQuiz->quizzes->toArray();                
                        $quizzes = array_merge($quiz, $quizzes);
                    }
                }   
            }

            // khi nguoi dung chua hoan thanh bai trac nghiem nao 
            if (count($quizzes) == 0) {
                $groupQuizId = GroupQuiz::select('group_quizzes.id')->get()->toArray();
                for ($i=0; $i < count($groupQuizId) ; $i++) { 
                    $groupQuiz = GroupQuiz::find($groupQuizId[$i]["id"]);
                    $quiz = $groupQuiz->quizzes->toArray();
                    $quizzes = array_merge($quiz, $quizzes);
                }
            }

            $jsonDataQuiz = '{';
            $jsonDataQuiz .= '"title": "Luyện tập tổng quát",';
            $jsonDataQuiz .= '"id":"all",';
            if (Auth::check()) {
                $jsonDataQuiz .= '"user_level":"'. Auth::user()->user_level .'",';
            }                
            $jsonDataQuiz .= '"quizs":[';
            $countQuiz = count($quizzes);
            for ($i=0; $i < $countQuiz; $i++) { 
                if ($i == 0) {
                    $jsonDataQuiz .= '{';
                } else {
                    $jsonDataQuiz .= ',{';
                }   
                    $jsonDataQuiz .= '"id":"'. $quizzes[$i]["id"] .'",';                 
                    $jsonDataQuiz .= '"title":"'. $quizzes[$i]["quiz_title"] .'",';
                    $jsonDataQuiz .= '"type":"'. $quizzes[$i]["quiz_type"] .'",';
                    $jsonDataQuiz .= '"description":"'. $quizzes[$i]["quiz_description"] .'",';
                    $jsonDataQuiz .= '"media":"'. $quizzes[$i]["quiz_media"] .'",';
                    $jsonDataQuiz .= '"thumbnail":"'. $quizzes[$i]["quiz_thumbnail"] .'",';
                    $jsonDataQuiz .= '"answers":[';
                        $answers= Quiz::find($quizzes[$i]["id"])->answers->toArray();
                        $countAnswer = count($answers);
                        for ($j=0; $j < $countAnswer ; $j++) { 
                            if ($j == 0) {
                                $jsonDataQuiz .= '{';
                            } else {
                                $jsonDataQuiz .= ',{';
                            }
                            $jsonDataQuiz .= '"id":"'. $answers[$j]["id"] .'",';
                            $jsonDataQuiz .= '"title":"'. $answers[$j]["answer_title"] .'",';
                            $jsonDataQuiz .= '"thumbnail":"'. $answers[$j]["answer_thumbnail"] .'",';
                            $jsonDataQuiz .= '"is_correct":"'. $answers[$j]["answer_is_correct"] .'"';
                            $jsonDataQuiz .= '}';
                        }
                    $jsonDataQuiz .= ']';
                $jsonDataQuiz .= '}';
            }

            $jsonDataQuiz .= ']';
            $jsonDataQuiz .= '}';
        }
        return response($jsonDataQuiz);   
        
    }
    public function getThumbnail($request){
        $target_dir = "uploads/images/";
        $file = $request->file('group_quiz_thumbnail');
        $file_name = time()."-".$file->getClientOriginalName();
        $file_extension =  $file->guessExtension();
        
        if(in_array($file_extension, array('jpg','png','jpeg', 'gif'))) {
            $file->move($target_dir, $file_name);        
            $target_file = $target_dir.$file_name;
            $image = Image::make($target_file)->resize(320, 180)->save();
        
            return "/" . $target_file;
        }
    }
    public function seoname($str){
        if(!$str) return false;
        $unicode = array(
            'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
            'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
            'd'=>array('đ'),
            'D'=>array('Đ'),
            'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
            'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
            'i'=>array('í','ì','ỉ','ĩ','ị'),
            'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
            'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),
            'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
            'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
            'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
            'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
            'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
            '-'=>array(' ','&quot;','.','-–-')
        );
        foreach($unicode as $nonUnicode=>$uni){
            foreach($uni as $value)
            $str = @str_replace($value,$nonUnicode,$str);
            $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
            $str = preg_replace("/-+-/","-",$str);
            $str = preg_replace("/^\-+|\-+$/","",$str);
        }
        return strtolower($str);
    }  
}
