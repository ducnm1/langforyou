<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Support\JsonableInterface;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Post;
use App\GroupQuiz;
use App\Quiz;
use App\Cat;
use App\Youtube;
use App\Vocabulary;
use Auth;
use App\User;
use Image;
use Carbon\Carbon;
use App\dict;
// use Config;


class PostsController extends Controller
{

    public function __construct()
    {
        // $this->middleware('CheckPermission');

        // $this->middleware('log', ['only' => [
        //     'fooAction',
        //     'barAction',
        // ]]);

        // $this->middleware('subscribed', ['except' => [
        //     'fooAction',
        //     'barAction',
        // ]]);
    }
    public function convert()
    {
        echo '<pre>';
        $dicts = Dict::where('idx','>',3500)->take(500)->get();
        // var_dump($dicts);
        $oldVocabulary = Vocabulary::select('vocabularies.vocabulary_title')->get()->toArray();
        $oldVocabulary2 = array();
        for ($i=0; $i < count($oldVocabulary); $i++) { 
            array_push($oldVocabulary2, $oldVocabulary[$i]['vocabulary_title']);
        }
 

        foreach ($dicts as $dict) {

            if (!in_array($dict->word, $oldVocabulary2)) {
                $str = str_replace (['<C><F><I><N><Q>','</Q></N></I></F></C>'],'',$dict->detail);

                $ipaPositionFirst = strpos($str, '/');
                $breakPositionFirst = strpos($str, '<br />');
                if ($ipaPositionFirst < $breakPositionFirst) {
                    $ipaPositionLast = strpos($str, '/', $ipaPositionFirst + 1);
                    $ipaLength = $ipaPositionLast - $ipaPositionFirst;
                    $ipa = substr($str, $ipaPositionFirst, $ipaLength+1);
                }

                $strDel = substr($str, 0, $breakPositionFirst + 6);           
                $content = str_replace($strDel, "", $str);


                $vocabulary = new Vocabulary;
                $vocabulary->user_id = 2;
                $vocabulary->vocabulary_title = $dict->word;
                if (isset($ipa)) {
                    $vocabulary->vocabulary_ipa = $ipa;
                }
                
                $vocabulary->vocabulary_content = $content;
                     
                $vocabulary->save();
            }
        }
    }
    public function ajaxGetPost(Request $request)
    {
        $postType = $request->postType;
        $catId = $request->catId;
        $userId = $request->userId;

        $currentTime = Carbon::now()->timestamp;
        
        if ($catId == true) 
        {
            $arIdCat = array($catId);
            $childCat = Cat::find($catId)->catChildrent->toArray();
            while ( count( $childCat ) ) {
                for ($i=0; $i < count($childCat); $i++) { 
                    array_push($arIdCat, $childCat[$i]['id']);
                    $childCat = Cat::find($childCat[$i]['id'])->catChildrent->toArray();
                }
            }
            $posts = Post
                ::orderBy('post_timer','desc')
                ->whereIn('posts.cat_id', $arIdCat)
                ->where('posts.post_status', '=','public')
                ->where('posts.post_timer', '<=', $currentTime)
                ->select('posts.*')
                ->paginate(24)->toJson();
            return $posts;
        }
        if ($userId == true) 
        {
            $posts = Post
                ::orderBy('post_timer','desc')
                ->where('posts.user_id', '=', $userId)
                ->where('posts.post_status', '=','public')
                ->where('posts.post_timer', '<=', $currentTime)
                ->select('posts.*')
                ->paginate(24)->toJson();
            return $posts;
        }
        if ($postType == true) 
        {
            $posts = Post
                ::orderBy('post_timer','desc')
                ->where('posts.post_type', '=', $postType)
                ->where('posts.post_status', '=','public')
                ->where('posts.post_timer', '<=', $currentTime)
                ->select('posts.*')
                ->paginate(24)->toJson();
            return $posts;
        } 
        if ($catId == 0 && $userId==0 && $postType==0) {
            $exceptPost = $this->getFeaturePosts()->toArray();
            
            $arIdExceptPost = array();

            for ($i=0; $i < count($exceptPost) ; $i++) { 
                array_push($arIdExceptPost, $exceptPost[$i]['id']);
            }
            $posts = Post
                ::orderBy('post_timer','desc')
                ->whereNotIn('id', $arIdExceptPost)                
                ->where('posts.post_status', '=', 'public')
                ->where('posts.post_timer', '<=', $currentTime)
                ->select('posts.*')
                ->paginate(24)->toJson();
            return $posts;
        }
    }
    public function index()
    {
        return view('frontends.pages.home');
    }

    public function featurePosts(){
        $posts = $this->getFeaturePosts()->toJson();
        return $posts;
    }

    public function getFeaturePosts(){
        // $timeNow = Carbon::now();
        // $currentTime = $timeNow->timestamp;

        // $year = $timeNow->year;
        // $month =  $timeNow->month;
        // $day = $timeNow->day;

        // $oldDay = $day - 15;
        // $oldMonth = $month;
        // $oldYear = $year;
        // 
        
        // $timeNow = Carbon::now();
        $currentTime = Carbon::now()->timestamp;
        // $oldDay = ($currentTime/86400) - 15;



        // if ($oldDay < 0) {
        //     $oldDay = 30 - abs($oldDay);
        //     $oldMonth = $month -1;
        //     if ($oldMonth == 0) {
        //         $oldYear--;
        //         $oldMonth = 12;
        //     }
        // }

        // $dt = Carbon::create($oldYear , $oldMonth, $oldDay, 0, 0, 0)->toDateTimeString();
 
        $posts = Post
            ::orderBy('post_view_count','desc')            
            ->where('posts.post_status', '=','public')
            ->where('posts.post_timer', '>', $currentTime - 15 * 86400)
            ->where('posts.post_timer', '<=', $currentTime)
            ->select('posts.*')
            ->take(6)
            ->get();
        return $posts;
    }

    public function historyPost(){
        if (Auth::check()) {
            $user = User::find(Auth::user()->id);

            if ($user->user_history != "") {
                $userHistory = explode(',', $user->user_history);
                $arIdHistoryPost = array();

                for ($j=0; $j < count($userHistory); $j++) {
                    if (starts_with($userHistory[$j], 'post_')) {
                        array_push($arIdHistoryPost, trim($userHistory[$j], 'post_'));
                    } 
                }
                
                $post = Post::whereIn('id', $arIdHistoryPost)->take(5)->get()->toJson();
                    
                return $post;
            }            
        }
    }

    public function listOfType(Request $request)
    {
        // if (Auth::check()) {
        //     $userLearLang = Auth::user()->user_learn_lang;
        // } else {
        //     $userLearLang = '*';
        // }

        // if ($postType == 'entertainment') {
        //     $userLearLang = '*';
        // }

        // if ($userLearLang == '*') {
        //    $posts = Post
        //         ::orderBy('id','desc')
        //         ->where('posts.post_type', '=', $postType)
        //         ->select('posts.*')
        //         ->take(24)
        //         ->get()->toArray();
        // } else {
        //     $posts = Post
        //         ::orderBy('id','desc')
        //         ->where('posts.post_type', '=', $postType)
        //         ->where('posts.post_lang', '=', $userLearLang)
        //         ->select('posts.*')
        //         ->take(24)
        //         ->get()->toArray();
        // }
        

        // for ($i=0; $i < count($posts) ; $i++) { 
        //     $posts[$i]['post_title_unicode'] = $this->seoname($posts[$i]['post_title']);
        // }
        // $userId = '';
        
        // return view('frontends.pages.home', compact('posts', 'postType', 'userId'));
        
        $path = url()->full();
        $domain = Config('app.url');
        $postType = str_replace($domain.'/', '', $path);
        return view('frontends.pages.home', compact('postType'));
    }

    public function listOfCat($name, $id)
    {
        // if (Auth::check()) {
        //     $userLearLang = Auth::user()->user_learn_lang;
        // } else {
        //     $userLearLang = '*';
        // }
        // if ($userLearLang == '*'){
        //     $posts = Post
        //         ::orderBy('id','desc')
        //         ->where('posts.cat_id', '=', $catId)
        //         ->select('posts.*')
        //         ->take(24)
        //         ->get();
        // } else {
        //     $posts = Post
        //         ::orderBy('id','desc')
        //         ->where('posts.cat_id', '=', $catId)
        //         ->where('posts.post_lang', '=', $userLearLang)
        //         ->select('posts.*')
        //         ->take(24)
        //         ->get();
        // }        
            
        // $postType = '';
        // return view('frontends.pages.home', compact('posts', 'catId', 'postType'));
        // 
        $x = $name."-".$id;
        $x = explode("-", $x);        
        $catId = $x[count($x)-1];
        unset($x[count($x)-1]);
        $name = implode("-", $x);

        $cat = Cat::find($catId);
        if ($cat == null) {
            return abort(404);
        }

        $originalTitle = $this->seoname($cat->cat_title);

        if ($name != $originalTitle) {
            return redirect()->route('category.show', ['name'=>$originalTitle, 'id'=>$id]);
        }

        // breadcrumb
        $arCats = array();
        while ($cat != null) {
            $ar = array(
                'id'=>$cat->id,
                'cat_title'=>$cat->cat_title,
                'cat_unicode_title'=>$this->seoname($cat->cat_title)
            );
            array_push($arCats, $ar);
            $cat = $cat->catParent;
        }
        $arCats = array_reverse($arCats);

        return view('frontends.pages.home', compact('catId', 'arCats'));
    }

    public function ajaxIndex(Request $request){
        $lastPostId = $request->lastPostId;
        $postType = $request->postType;
        $userId = $request->userId;

        if (Auth::check()) {
            $userLearLang = Auth::user()->user_learn_lang;
        } else {
            $userLearLang = '*';
        }

        if (empty($postType) && !empty($userId)) {
            if ($userLearLang == '*') {
                $posts = Post
                    ::orderBy('id','desc')
                    ->where('posts.id', '<', $lastPostId)
                    ->where('posts.user_id', '=', $userId)
                    ->select('posts.*')
                    ->take(9)
                    ->get()->toArray(); 
            } else {
                $posts = Post
                    ::orderBy('id','desc')
                    ->where('posts.id', '<', $lastPostId)
                    ->where('posts.user_id', '=', $userId)
                    ->where('posts.post_lang', '=', $userLearLang)
                    ->select('posts.*')
                    ->take(9)
                    ->get()->toArray(); 
            }
              
        } elseif(!empty($postType)) {
            if ($userLearLang == '*') {
                $posts = Post
                    ::orderBy('id','desc')
                    ->where('posts.id', '<', $lastPostId)
                    ->where('posts.post_type', '=', $postType)
                    ->select('posts.*')
                    ->take(9)
                    ->get()->toArray();
            } else {
                $posts = Post
                    ::orderBy('id','desc')
                    ->where('posts.id', '<', $lastPostId)
                    ->where('posts.post_type', '=', $postType)
                    ->where('posts.post_lang', '=', $userLearLang)
                    ->select('posts.*')
                    ->take(9)
                    ->get()->toArray();
            }            
        }
        for ($i=0; $i < count($posts) ; $i++) { 
            $posts[$i]['post_title_unicode'] = $this->seoname($posts[$i]['post_title']);
        }

        return response()->json($posts);
    }

    public function listOfUser($userId)
    {        
        // $posts = Post
        //     ::orderBy('id','desc')
        //     ->where('user_id', '=', $userId)
        //     ->select('posts.*')
        //     ->take(24)
        //     ->get();
        // $postType = '';

        // return view('frontends.pages.home', compact('posts', 'userId', 'postType'));
        return view('frontends.pages.home', compact('userId'));
    }

    public function listPost()
    {
        return view('backends.pages.listPost');
    }

    public function listPostAjax(Request $request)
    {       
        $user = $request->user;
        $category = $request->category;
        $type = $request->type;
        $request->status ? $status = 'draft' : $status = 'public';

        $currentTime = Carbon::now()->timestamp;

        $request->timer ? $timer = false : $timer = true;

        if ($timer == true) {
            $posts = Post
            ::orderBy('id','desc')
            ->join('cats', 'posts.cat_id', '=', 'cats.id')
            ->join('users', 'posts.user_id', '=', 'users.id')
            ->when($user, function ($query) use ($user) {
                    return $query->where('posts.user_id','=', $user);
                })
            ->when($category, function ($query) use ($category) {
                    return $query->where('posts.cat_id','=', $category);
                })
            ->when($type, function ($query) use ($type) {
                    return $query->where('posts.post_type','=', $type);
                })
            ->when($status, function ($query) use ($status) {
                    return $query->where('posts.post_status','=', $status);
                })
            ->where('posts.post_timer','<=', $currentTime)
            ->select('posts.*','users.name','cats.cat_title')
            ->paginate(24)->toJson();
        }
        else {
            $posts = Post
            ::orderBy('id','desc')
            ->join('cats', 'posts.cat_id', '=', 'cats.id')
            ->join('users', 'posts.user_id', '=', 'users.id')
            ->when($user, function ($query) use ($user) {
                    return $query->where('posts.user_id','=', $user);
                })
            ->when($category, function ($query) use ($category) {
                    return $query->where('posts.cat_id','=', $category);
                })
            ->when($type, function ($query) use ($type) {
                    return $query->where('posts.post_type','=', $type);
                })
            ->when($status, function ($query) use ($status) {
                    return $query->where('posts.post_status','=', $status);
                })
            ->where('posts.post_timer','>', $currentTime)
            ->select('posts.*','users.name','cats.cat_title')
            ->paginate(24)->toJson();
        }
        return $posts;
    }

    public function create()
    {
        if (Auth::user()->user_level > 3) {
            $cats = Cat::all();
            $arCats = array();
            foreach ($cats as $cat) {
                $arCats[$cat->id] = $cat->cat_title;
            }

            return view('backends.pages.createPost', compact('arCats'));
        } else {
            return abort(401);
        }
    }

    public function store(Request $request)
    {
        if (Auth::user()->user_level > 3) {
            $this->validate($request, [
                'post_title' => 'required|unique:posts|min:3|max:255',
            ]);

            $post = new Post;

            $post->post_title = trim($request->post_title);

            if ($request -> has('post_content')) {
                $post->post_content = $request->post_content;
            }

            if ( $request->has('youtube_id') ) {
                $youtubeId = $request->youtube_id;
                $youtube = Youtube::find($youtubeId);
                $post->youtube_id = $youtubeId;
                if ($youtube != null) {
                    $post->post_thumbnail = $youtube->youtube_thumbnail;
                }            
            }

            if ($request->has('group_quiz_id')) {
                $post->group_quiz_id = $request->group_quiz_id;
            }
            
            if ($request->has('cat_id')) {
                $post->cat_id = $request->cat_id;    
            }
            
            $post->post_type = $request->post_type;
                 

            if ($request->hasFile('post_thumbnail') && $request->file('post_thumbnail')->isValid()) {
                $post->post_thumbnail = $this->getThumbnail($request);
            }
           
            $post->user_id = Auth::user()->id;     

            // $request->has("save_draft") ? $post->active = 0 : $post->active = 1;
            $post->post_lang = $request->post_lang;

            
            if ($request->has('post_timer')) {
                $post->post_timer = Carbon::parse($request->post_timer)->timestamp;
            } else {
                $post->post_timer = Carbon::now()->timestamp;
            }
            $post->post_status = $request->post_status;

            if ($request->has('vocabularies_id')) {
                $post->vocabularies_id = str_replace (' ','',$request->vocabularies_id);
            }
            $post->save();
            $request->session()->flash('status_action_post', 'Bài viết được tạo thành công');

            return redirect()->route('post.edit', $post->id);
        } else {
            return abort(401);
        }
    }

    public function show($name, $id, Request $request)
    {   

        $x = $name."-".$id;
        $x = explode("-", $x);        
        $id = $x[count($x)-1];
        unset($x[count($x)-1]);
        $name = implode("-", $x);

        $post = Post::find($id);
        if ($post == null) {
            return abort(404);
        }



        $originalTitle = $this->seoname($post->post_title);

        if ($name != $originalTitle) {
            return redirect()->route('post.show', ['name'=>$originalTitle, 'id'=>$id]);
        }
        $currentTime = Carbon::now()->timestamp;
        // related post
        $posts = Post::orderBy('id', 'desc')
                ->where('posts.id', '<>', $id)
                ->where('posts.cat_id', '=', $post->cat_id)
                ->where('posts.post_status', '=', 'public')
                ->where('posts.post_timer', '<=', $currentTime)
                ->select('posts.id')->get()->toArray();
        $arIdPosts = array();
        foreach ($posts as $value) {
            array_push($arIdPosts, $value['id']);
        }
        shuffle($arIdPosts);

        $arIdRelatedPosts = array();
        for ($i=0; $i < count($arIdPosts) ; $i++) { 
            if ($i< 5) {
                array_push($arIdRelatedPosts, $arIdPosts[$i]);
            }            
        }


        $relatedPosts = Post::whereIn('posts.id', $arIdRelatedPosts)->take(5)->get()->toArray();
        for ($i=0; $i < count($relatedPosts) ; $i++) { 
            $relatedPosts[$i]['post_title_unicode'] = $this->seoname($relatedPosts[$i]['post_title']);
        }
        shuffle($relatedPosts);


        $user = $post->user;

       
        // history post
        if (Auth::check()) {
            $userCurrent = User::find(Auth::user()->id);
            $oldUserHistory = explode(',', $userCurrent->user_history);
            $idHistory = "post_" . $id;
            if (!in_array("post_" . $id, $oldUserHistory)) {
                array_push($oldUserHistory, $idHistory);
            } else {
                for ($j=0; $j < count($oldUserHistory); $j++) { 
                    if ($oldUserHistory[$j] == $idHistory) {
                        unset($oldUserHistory[$j]);
                        array_push($oldUserHistory, $idHistory);                        
                    }
                }
            }
            
            $oldUserHistory = implode(",", $oldUserHistory);
            $userCurrent->user_history = $oldUserHistory;
            $userCurrent->save();
        }
        
        // counter page
        $ipaddress = $this->get_client_ip();
        $timeViewPage = "post_" . $ipaddress . "timeViewPage";
        $newIime = Carbon::now()->timestamp / 60;
        if ($request->session()->has($timeViewPage)) {               
            $oldTime = $request->session()->get($timeViewPage);
            if (($newIime - $oldTime) > 1) {                
                $request->session()->put($timeViewPage, $newIime);
                $post->post_view_count += 1;    
                $post->save();
            }
        } else {
            $request->session()->put($timeViewPage, $newIime);
            $post->post_view_count += 1;
            $post->save();
        }
        
        if (!empty($post->youtube_id)) {
            $youtube = $post->getYoutube;
        } else {
            $youtube = '';
        }

        // breadcrumb
        $cat = $post->cat;
        $arCats = array();
        while ($cat != null) {
            $ar = array(
                'id'=>$cat->id,
                'cat_title'=>$cat->cat_title,
                'cat_unicode_title'=>$this->seoname($cat->cat_title)
            );
            array_push($arCats, $ar);
            $cat = $cat->catParent;
        }
        $arCats = array_reverse($arCats);


        if ($post->post_type == 'tu-vung') {
            $vocabularies = $post->vocabularies;

            $arVocabularyId = explode(',', $post->vocabularies_id);
            $vocabularies2 = Vocabulary::whereIn('id', $arVocabularyId)->get();
            return view('frontends.pages.detail', compact('post', 'user', 'youtube', 'relatedPosts', 'vocabularies', 'vocabularies2', 'arCats'));

        } else {
            return view('frontends.pages.detail', compact('post', 'user', 'youtube', 'relatedPosts', 'arCats'));
        }
    }

    public function edit($id, Request $request)
    {
        $post = Post::find($id);
        $user = $post->user;

        if ($post->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {
            $cats = Cat::all();
            $arCats = array();
            foreach ($cats as $cat) {
                $arCats[$cat->id] = $cat->cat_title;
            }
            $groupQuizzes = GroupQuiz::all();    

            $users = User::orderBy('id','ASC')
                ->where('users.user_level','>', 3)
                ->select('users.id', 'users.name')
                ->get();


            if ($request->session()->has('status_action_post')) {
                $status_action_post = $request->session()->get('status_action_post');
                return view('backends.pages.editPost', compact('post', 'arCats', 'status_action_post', 'groupQuizzes', 'users'));
            } else {
                return view('backends.pages.editPost', compact('post', 'arCats', 'groupQuizzes', 'users'));    
            }            
        } else {
            return abort(401);
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'post_title' => 'required|min:3|max:255',
        ]);
        $post = Post::find($id);   
        $user = $post->user;
        if ($post->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(5,6,7))) {            
            
            $post->post_title = trim($request->post_title);

            if ($request -> has('post_content')) {
                $post->post_content = $request->post_content;
            }

            if ( $request->youtube_id != 0 ) {
                $youtubeId = $request->youtube_id;
                $youtube = Youtube::find($youtubeId);
                $post->youtube_id = $request->youtube_id;
            }

            if ($request->has('group_quiz_id')){
                $post->group_quiz_id = $request->group_quiz_id;
            }            

            $post->cat_id = $request->cat_id;

            $post->post_type = $request->post_type;

            if ($request->hasFile('post_thumbnail')) {
                $post->post_thumbnail = $this->getThumbnail($request);
            }

            if (empty($post->post_thumbnail) && $request->youtube_id != 0 ) {
                $post->post_thumbnail = $youtube->youtube_thumbnail;
            }

            $post->post_lang = $request->post_lang;
            if ($request->has('change_author')) {
                $post->user_id = $request->change_author;
            }

            
            if ($request->has('vocabularies_id')) {
                $post->vocabularies_id = str_replace (' ','',$request->vocabularies_id);
            }

            if ($request->has('post_timer')) {
                $post->post_timer = Carbon::parse($request->post_timer)->timestamp;
            }
            $post->post_status = $request->post_status;

            $post->save();
            $request->session()->flash('status_action_post', 'Bài viết được cập nhật thành công');
            return redirect()->route('post.edit', $id);
        } else {
            return abort(401);
        }
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $user = $post->user;
        if ($post->user_id == Auth::user()->id || in_array(Auth::user()->user_level, array(6,7))) {

            $users = User::where('users.user_history', '<>','')->select('users.id')->get()->toArray();
            
            for ($i=0; $i < count($users) ; $i++) { 
                $user = User::find($users[$i]['id']);
                $historyPost = $user->user_history;
                $historyPost = explode(',', $historyPost);
                for ($i=0; $i < count($historyPost) ; $i++) { 
                    if ( starts_with($historyPost[$i], 'post_')) {

                        $idPost = trim($historyPost[$i], 'post_');

                        if ($id == $idPost) {
                            unset($historyPost[$i]);
                        }
                    }
                }
                $historyPost = implode(',', $historyPost);
                $user->user_history = $historyPost;
                $user ->save();
            }            

            $post->destroy($id);
            return redirect()->route('listPost');
        } else {
            return abort(401);
        }        
    }

    public function getThumbnail($request){
        $target_dir = "uploads/images/";
        $file = $request->file('post_thumbnail');
        $file_name = time()."-".$file->getClientOriginalName();
        $file_extension =  $file->guessExtension();
        
        if(in_array($file_extension, array('jpg','png','jpeg', 'gif','mpga'))) {
            $file->move($target_dir, $file_name);        
            $target_file = $target_dir.$file_name;
            $image = Image::make($target_file)->resize(320, 180)->save();
        
            return "/" . $target_file;
        }
    }

    public function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function seoname($str){
        if(!$str) return false;
        $unicode = array(
            'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
            'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
            'd'=>array('đ'),
            'D'=>array('Đ'),
            'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
            'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
            'i'=>array('í','ì','ỉ','ĩ','ị'),
            'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
            'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),
            'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
            'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
            'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
            'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
            'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
            '-'=>array(' ','&quot;','.','-–-')
        );
        foreach($unicode as $nonUnicode=>$uni){
            foreach($uni as $value)
            $str = @str_replace($value,$nonUnicode,$str);
            $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
            $str = preg_replace("/-+-/","-",$str);
            $str = preg_replace("/^\-+|\-+$/","",$str);
        }
        return strtolower($str);
    }  
}