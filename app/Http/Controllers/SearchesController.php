<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;
use App\Vocabulary;

class SearchesController extends Controller
{
    public function search(Request $request)
    {

    	$datas = array();
    	$searchKey = $request->searchKey;

    	$posts = Post::where('posts.post_title', 'like', "%$searchKey%")->get();

        if ($posts != null) {
            foreach ($posts as $post) {
                array_push($datas, array('title'=>$post->post_title, 'url'=>route('post.show',['name'=>$this->seoname($post->post_title), 'id'=>$post->id])));
            }
        }
    	
    	$vocabularies = Vocabulary::where('vocabularies.vocabulary_title', 'like', "%$searchKey%")->get();

        if ($vocabularies != null) {
            foreach ($vocabularies as $vocabulary) {
                $ar = array(
                    'title'=>$vocabulary->vocabulary_title,
                    'url'=>route('vocabulary.show', [
                        'name'=>$this->seoname($vocabulary->vocabulary_title),
                        'id'=>$vocabulary->id
                    ])
                );
                array_push($datas, $ar);
            }
        }

    	return view('frontends.pages.search', compact('datas', 'searchKey'));
    }

    public function seoname($str){
        if(!$str) return false;
        $unicode = array(
            'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
            'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
            'd'=>array('đ'),
            'D'=>array('Đ'),
            'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
            'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
            'i'=>array('í','ì','ỉ','ĩ','ị'),
            'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
            'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),
            'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
            'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
            'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
            'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
            'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
            '-'=>array(' ','&quot;','.','-–-')
        );
        foreach($unicode as $nonUnicode=>$uni){
            foreach($uni as $value)
            $str = @str_replace($value,$nonUnicode,$str);
            $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
            $str = preg_replace("/-+-/","-",$str);
            $str = preg_replace("/^\-+|\-+$/","",$str);
        }
        return strtolower($str);
    }  
}
