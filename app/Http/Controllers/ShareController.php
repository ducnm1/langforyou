<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Requests;

class ShareController extends Controller
{
    public function currentRoute()
    {
    	$name = Route::currentRouteName();
    	$name = json_encode($name);
    	return $name;
    }
}
