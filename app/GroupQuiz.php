<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupQuiz extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'group_quiz_parent_id', 'group_quiz_title','group_quiz_description', 'group_quiz_thumbnail'
    ];

    /**
     * Get the quizzes for the group quiz.
     */
    public function quizzes()
    {
        return $this->hasMany('App\Quiz','group_quiz_id','id');
    }

    public function groupQuizzes()
    {
        return $this->hasMany('App\GroupQuiz','group_quiz_parent_id','id');
    }

    /**
     * Get the user that owns the article.
     */
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
