<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	protected $fillable = [
        'user_id', 'page_title', 'page_thumbnail', 'page_content', 'page_lang'
    ];
     public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
