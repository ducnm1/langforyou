<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quiz_id', 'answer_title', 'answer_thumbnail', 'answer_is_correct'
    ];


 

    
    public function quiz()
    {
        return $this->belongsTo('App\Quiz','quiz_id','id');
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
