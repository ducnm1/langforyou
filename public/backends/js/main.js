var backend = angular.module('lang4u', []);
backend.controller('listPosts', function ($scope, $rootScope, $http, formatString, getCurrentUser, getAllUser ,getAllCat) {
	$scope.url = "/postsAjax?page=";

    getCurrentUser();
    $rootScope.$watch('currentUser', function(){
        if ($rootScope.currentUser) {
            $scope.selectUsers = {
                options: $rootScope.currentUser,
                selectedOption:$rootScope.currentUser[0]
            };    
        }        
    });

    $scope.selectStatus = {
        options: [
            {id:0, name:'Công khai'},
            {id:1, name:'Bản nháp'}
        ],
        selectedOption:{id:0, name:'Công khai'}
    }

    $scope.selectTimer = {
        options: [
            {id:0, name:'Đã qua'},
            {id:1, name:'Chưa tới'}
        ],
        selectedOption:{id:0, name:'Đã qua'}
    }

    $scope.selectCats = {
        options: [
            {id:0, cat_title:'All'}
        ],
        selectedOption:{id:0, cat_title:'All'}
    };

    $scope.selectTypes = {
        options: [
            {id:0, name:'All'},
            {id:1, name:'Standard'},
            {id:2, name:'Từ vựng'},
            {id:3, name:'Ngữ pháp'},
            {id:4, name:'Giải trí'}
        ],
        selectedOption:{id:0, name:'All'}
    };

    getAllUser();
    $rootScope.$watch('allUser', function(){
        if ($rootScope.allUser && $rootScope.currentUser) {
            $scope.users = $rootScope.allUser;
            $scope.users.unshift({id:0, name:'All'});

            $scope.selectUsers = {
                options: $scope.users,
                selectedOption:$rootScope.currentUser[0]
            };    
        }        
    });

    getAllCat();
    $rootScope.$watch('allCat', function(){
        if ($rootScope.allCat) {
            $scope.selectCats = {
                options: $rootScope.allCat,
                selectedOption:$rootScope.allCat[0]
            }
        }
    });

	$scope.load = function(url,pageNumber){
		var url= url+pageNumber;
        $http({
            method: 'GET',
            url: url,
            params: {
                user : $scope.selectUsers.selectedOption.id,
                category : $scope.selectCats.selectedOption.id,
                type : $scope.selectTypes.selectedOption.id,
                status : $scope.selectStatus.selectedOption.id,
                timer : $scope.selectTimer.selectedOption.id
            }
        }).then(function(response){
            jQuery('.page_list .box-body').removeClass('loading');
            if (response.data.data.length > 0) {
                $scope.posts = response.data.data;                
                for (var i = $scope.posts.length - 1; i >= 0; i--) {
                    $scope.posts[i].post_url = '/'+ formatString($scope.posts[i].post_title) + '-' + $scope.posts[i].id;
                }
                var number =[];
	            for (var i = 1; i <= response.data.last_page; i++) {
	            	number.push(i);
	            }

	            $scope.pagination = {
	            	total : response.data.total,
	            	current_page: response.data.current_page,
	            	last_page: response.data.last_page,
	            	numbers: number
	            }
            }
            else {
                $scope.posts = [];
            }
        }, function(response){
            console.log(response);
        });
    };

    $scope.$watch('selectUsers.options', function(newValue, oldValue){
        if ($scope.selectUsers && $scope.selectUsers.options.length == 1) {
            $scope.load($scope.url, 1);
        }
    });    

    $scope.change = function() {
        $scope.load($scope.url, 1);
    }
});

backend.controller('listYoutubes', function($rootScope, $scope, $http, getCurrentUser, getAllUser) {
    $scope.url = "/listYoutubeAjax?page=";

    getCurrentUser();
    $rootScope.$watch('currentUser', function(){
        if ($rootScope.currentUser) {
            $scope.selectUsers = {
                options: $rootScope.currentUser,
                selectedOption:$rootScope.currentUser[0]
            };    
        }        
    });

    getAllUser();
    $rootScope.$watch('allUser', function(){
        if ($rootScope.allUser && $rootScope.currentUser) {
            $scope.selectUsers = {
                options: $rootScope.allUser,
                selectedOption:$rootScope.currentUser[0]
            };    
        }        
    });

    $scope.load = function(url,pageNumber){
        var url= url+pageNumber;
        $http({
            method: 'GET',
            url: url,
            params: {
                user : $scope.selectUsers.selectedOption.id
            }
        }).then(function(response){
            jQuery('.page_list .box-body').removeClass('loading');
            if (response.data.data.length > 0) {                
                $scope.youtubes = response.data.data;
                var number =[];
                for (var i = 1; i <= response.data.last_page; i++) {
                    number.push(i);
                }

                $scope.pagination = {
                    total : response.data.total,
                    current_page: response.data.current_page,
                    last_page: response.data.last_page,
                    numbers: number
                }
            }
            else {
                $scope.youtubes = [];
            }
        }, function(response){
            console.log(response);
        });
    };

    $scope.$watch('selectUsers.options', function(newValue, oldValue){
        if ($scope.selectUsers != undefined && $scope.selectUsers.options.length == 1) {
            $scope.load($scope.url, 1);
        }
    });    

    $scope.change = function() {
        $scope.load($scope.url, 1);
    }
});

backend.controller('shareCtl', function($scope){
    $scope.pathname = window.location.pathname;
});

backend.factory('formatString', function(){
    return function(str) {
        str= str.toLowerCase();
        str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
        str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
        str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
        str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
        str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
        str= str.replace(/đ/g,"d"); 
        str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");
        str= str.replace(/-+-/g,"-");
        str= str.replace(/^\-+|\-+$/g,""); 

        return str;
    }
});

backend.factory('getCurrentUser', function($rootScope, $http){
    return function(){
        $http({
            method: 'GET',
            url: '/getCurrentUser'            
        }).then(function(response){
            $rootScope.currentUser = response.data;
        }, function(response){

        });
    }
});

backend.factory('getAllUser', function($rootScope, $http){
    return function(){
        $http({
            method: 'GET',
            url: '/getAllUsers'            
        }).then(function(response){
            $rootScope.allUser = response.data;
            // $rootScope.allUser.unshift({id:0, name:'All'});
        }, function(response){

        });        
    }
});

backend.factory('getAllCat', function($rootScope, $http){
    return function(){
        $http({
            method: 'GET',
            url: '/getAllCats'            
        }).then(function(response){
            $rootScope.allCat = response.data;
            $rootScope.allCat.unshift({id:0, cat_title:'All'});
        }, function(response){

        });        
    }
});

backend.filter('filterDate', function($filter){
    return function(input){
        var  output = new Date(input.replace(/-/g,"/"));
        return $filter('date')(output, "dd-MM-yyyy HH:mm");
    }
});