"use strict";

var myApp = function($){
    var monkeyPatchAutocomplete = function(){
        var oldFn = $.ui.autocomplete.prototype._renderItem;
        $.ui.autocomplete.prototype._renderItem = function( ul, item) {
            ul.addClass('list-group');
            var re = new RegExp(this.term, "gi") ;
            var t = item.label.replace(re,"<strong>" + this.term + "</strong>");
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + t + "</a>" )
            .addClass('list-group-item')
            .appendTo( ul );
        };
    }

    var searchVocabularySuggest = function(){
        $.ajax({
            url: '/searchSuggest',
            type:'GET',
            success: function(data){
                $(".search_form input.form-control").autocomplete({
                    autoFocus: true,
                    source: function(req, responseFn) {
                        var re = $.ui.autocomplete.escapeRegex(req.term);
                        var matcher = new RegExp(re, "gi");
                        var a = $.grep( data, function(item, index){
                            return matcher.test(item);
                        });
                        responseFn( a );                    
                    },
                    open: function( event, ui ) {
                        $('.ui-autocomplete').mCustomScrollbar({
                            theme:"dark"
                        }); 
                    },
                    response: function( event, ui ) {
                        $('.ui-autocomplete').mCustomScrollbar("destroy");
                         
                    }
                });  
            }
        }); 
    }

    var scrollToMediaDetail =  function() {
        if ($('.detail_page .media-item').length && $(window).width > 1023) {
            $('body').animate({
                scrollTop: $(".media-item").offset().top - 120
            }, 300);
        }
        
    }
    return {
        init: function(){
            // monkeyPatchAutocomplete();
            // searchVocabularySuggest();
            scrollToMediaDetail();
        }
    }
}(jQuery);



jQuery( document ).ready(function($) {
    myApp.init();
});

function getYoutube(){
    // var youtubeId = $('#post_youtube').val();
    // $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    // });
}

var myVar;
var currentTime;
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


var player;
var videoId = $('#youtube_code_id').val();
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        width: 840,
        height: 473,
        videoId: videoId,
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

function onPlayerReady(event) {
    player.playVideo();
}

function onPlayerStateChange(event) {
    if (event.data == 1) {
        $('.box-subs .sub').removeClass('hide').addClass('show');

        var myVar = setInterval(function(){ 
            currentTime = player.getCurrentTime();
            $('.box-subs .sub').each(function(){
                var $this = $(this);
                $this.find('p').each(function(){
                    var timeStart = $(this).data('time-start');
                    var timeEnd = $(this).data('time-end');

                    if (currentTime >= timeStart && currentTime <= timeEnd) {
                        $this.find('p').removeClass('show').addClass('hide');
                        $(this).removeClass('hide').addClass('show');
                    } else if(player.getDuration() == currentTime){
                        $this.find('p').removeClass('show').addClass('hide');
                    }
                });
            });
        }, 1);
        
    } else if(event.data == 2) {
        clearInterval(myVar);
        $('.box-subs .sub').removeClass('show').addClass('hide');
    } else if(event.data == 0) {
        if ($('.widget-related-post .list-group-item').length > 0) {
            var urlNextPost = $('.widget-related-post .list-group-item:first a').attr('href');
            var titleNextPost =   $('.widget-related-post .list-group-item:first a').attr('title');  
            $('.youtube_content .next-post-title span').text(titleNextPost);   
            $('.next-post').removeClass('hidden');
            var timeoutNextPost = setTimeout(function(){
                window.location.href = urlNextPost;
            },3000);

            $('.close-next-post').click(function(){
                $('.next-post').addClass('hidden');
                clearTimeout(timeoutNextPost);
            });
            $('.next-post-now').click(function(){
                window.location.href = urlNextPost;
            });
        }
    }
}


var app = angular.module('myApp',[]);
app.controller('loadMoreController',function($scope, $http, formatString, strLimit){
    $scope.url = "/ajaxGetPost?page=1";   
    $scope.moreContent = false;
    $scope.posts = [];
    $scope.load = function(){
        var postType = jQuery('.widget-list-items' ).data('posttype') || 0;
        var catId = jQuery('.widget-list-items' ).data('catid') || 0;
        var userId = jQuery('.widget-list-items' ).data('userid') || 0;
        $http({
            method: 'GET',
            url: $scope.url,
            params: {
                postType: postType,
                catId: catId,
                userId: userId
            }
        }).then(function(response){
            var html = "";
            if (response.data.next_page_url == null ) {
                $scope.moreContent = false;
            } else {
                $scope.url = response.data.next_page_url;
                $scope.moreContent = true;
            }

            if (response.data.data.length > 0) {

                $scope.posts = $scope.posts.concat(response.data.data);
                if ($scope.posts.length <=24) {
                    jQuery(".widget-list-items").removeClass('loading');
                }
                
                for (var i = $scope.posts.length - 1; i >= 0; i--) {
                    $scope.posts[i].post_url = '/'+ formatString($scope.posts[i].post_title) + '-' + $scope.posts[i].id;
                    $scope.posts[i].post_title = strLimit($scope.posts[i].post_title, 65);
                }                
            } else {
                if ($scope.posts.length == 0) 
                {
                    jQuery(".widget-list-items").addClass('hidden');    
                }                
            }

        }, function(){
            
        });
    };    
    $scope.load();
});

app.factory('formatString', function(){
    return function(str) {
        str= str.toLowerCase();
        str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
        str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
        str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
        str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
        str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
        str= str.replace(/đ/g,"d"); 
        str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");
        str= str.replace(/-+-/g,"-");
        str= str.replace(/^\-+|\-+$/g,""); 

        return str;
    }
});

app.controller('featurePostController',function($scope, $http, formatString, strLimit){
    $http({
        method: 'GET',
        url: '/feature-post',
    }).then(function(response){
        $scope.posts = response.data;
        if ($scope.posts.length > 0) 
        {
            jQuery(".widget-feature-posts").removeClass('loading');
            for (var i = $scope.posts.length - 1; i >= 0; i--) {
                $scope.posts[i].post_url = '/'+ formatString($scope.posts[i].post_title) + '-' + $scope.posts[i].id;
                $scope.posts[i].post_title = strLimit($scope.posts[i].post_title, 65);
            }
        }
        else {
            jQuery(".widget-feature-posts").addClass('hidden');
        }
    }, function(){
        
    }); 
});

app.controller('historyPost', function($scope, $http, formatString, strLimit){
    $http({
        method: 'GET',
        url: '/historyPost'
    })
    .then(function(response){
        $scope.posts = response.data;
        for (var i = $scope.posts.length - 1; i >= 0; i--) {
            $scope.posts[i].post_url = '/'+ formatString($scope.posts[i].post_title) + '-' + $scope.posts[i].id;
            $scope.posts[i].post_title = strLimit($scope.posts[i].post_title, 45);
        }
    }, function(response){
        console.log(response);
    });
});

/*
*   Quiz
*/ 
$('.widget-practive .btn, .widget-practive .practive').click(function(){
    jQuery('#modalPostQuiz').modal({
        'show': true,
        'backdrop': 'static',
        'keyboard': false
    }); 
});

$('body').on('change', '[type^="radio"]', function(){
    if ($(this).is(':checked')) {
        $('.quiz-test-body .row .item').each(function(){
            var $this = $(this);
            if ($this.hasClass('checked')) {
                $this.removeClass('checked');
            }
        });
        $(this).closest('.item').addClass('checked');
    }
});

app.controller('quizTest', function($scope, $http, formatString, $sce){
    $scope.scoreQuiz = 0;
    $scope.arIndexQuiz = [];
    var result = [];
    var groupQuizId = jQuery('#group_quiz_id').val() || 0;

    $scope.state = 0;
    $scope.stateText = "Kiểm tra";

    $scope.resultStatus = false;
    $scope.resultStatusText = "";
    $scope.resultTrueText = "";

    $scope.disabledOver = false;
    $scope.stateOver = true;
    $scope.disabledCheck = true;    

    $scope.notify = "";

    $scope.answerVal = null;

    $scope.complete = false;

    $http({
        url: '/ajaxGetQuizzes',
        method: 'POST',
        data: 
        {
            groupQuizId: groupQuizId
        }
    }).then(function(response){
        $scope.data = response.data;
        $scope.quizTestTitle = $scope.data.title;

        if ($scope.data.quizs.length > 0) 
        {
            for (var i = 0; i < $scope.data.quizs.length; i++) {
                $scope.arIndexQuiz.push(i);
            }

            $scope.scorePlus =  100/$scope.data.quizs.length;
            $scope.randomIndex = $scope.arIndexQuiz[Math.floor(Math.random() * $scope.arIndexQuiz.length)];            
        };

    }, function(response){
        console.log(response);
    });

    $scope.over = function(){
        $scope.randomIndex = $scope.arIndexQuiz[Math.floor(Math.random() * $scope.arIndexQuiz.length)];
        $scope.answerVal = null;
    }
    $scope.check = function(){   
        if ($scope.state == 0) 
        {
            if ($scope.data.quizs[$scope.randomIndex].type == 'select') 
            {
                //$val = jQuery(".quiz-test-body [name^='answer_quiz']:checked").val();
                if ($scope.data.quizs[$scope.randomIndex].answers[$scope.answerVal].is_correct == 1) {
                    $scope.resultStatus = false;
                    $scope.resultStatusText = "Đúng";

                    if($scope.arIndexQuiz.indexOf($scope.randomIndex) != -1) {
                        $scope.arIndexQuiz.splice($scope.arIndexQuiz.indexOf($scope.randomIndex), 1);
                    }
                    $scope.scoreQuiz += $scope.scorePlus;

                    result.push({ "quiz_id": $scope.data.quizs[$scope.randomIndex].id, 
                                            "answer_id": $scope.data.quizs[$scope.randomIndex].answers[$scope.answerVal].id });

                    if ($scope.arIndexQuiz.length == 0) 
                    {
                        $scope.plusScore();
                    }
                    else {
                        $scope.state = 1;
                        $scope.stateText = "Câu tiếp";
                        $scope.disabledOver = true;
                    }                     

                } else {
                    $scope.resultStatus = true;
                    $scope.resultStatusText = "Sai";

                    for (var i = 0; i < $scope.data.quizs[$scope.randomIndex].answers.length; i++) {
                        if ($scope.data.quizs[$scope.randomIndex].answers[i].is_correct == 1) {
                            $scope.resultTrueText = $scope.data.quizs[$scope.randomIndex].answers[i].title;
                            break;
                        }
                    }

                    $scope.state = 1;
                    $scope.stateText = "Câu tiếp";
                    $scope.disabledOver = true;                    
                }
            }     
            else
            {
                //$val = formatString(jQuery(".quiz-test-body [name^='answer_quiz']").val());
                $scope.answerVal = formatString($scope.answerVal);
                
                for (var i = 0; i < $scope.data.quizs[$scope.randomIndex].answers.length; i++) {
                    var $answerTitle = formatString($scope.data.quizs[$scope.randomIndex].answers[i].title);
                    if ($scope.answerVal == $answerTitle) 
                    {
                        $scope.resultStatus = false;
                        $scope.resultStatusText = "Đúng";

                        if($scope.arIndexQuiz.indexOf($scope.randomIndex) != -1) {
                            $scope.arIndexQuiz.splice($scope.arIndexQuiz.indexOf($scope.randomIndex), 1);
                        }
                        $scope.scoreQuiz += $scope.scorePlus;
                        result.push({ "quiz_id": $scope.data.quizs[$scope.randomIndex].id, 
                                                    "answer_id": $scope.data.quizs[$scope.randomIndex].answers[i].id,
                                                    "answer_from_user" : $scope.answerVal
                                                });

                        if ($scope.arIndexQuiz.length == 0) 
                        {
                            $scope.plusScore();                            
                        }
                        else {
                            $scope.state = 1;
                            $scope.stateText = "Câu tiếp";
                            $scope.disabledOver = true;
                        }         
                        $scope.resultStatus = false;
                        break;   
                    }            
                    else
                    {
                        $scope.resultStatus = true;
                    }        
                }

                if ($scope.resultStatus == true) 
                {
                    $scope.resultStatusText = "Sai";
                    $scope.state = 1;
                    $scope.stateText = "Câu tiếp";
                    $scope.disabledOver = true;

                    for (var i = 0; i < $scope.data.quizs[$scope.randomIndex].answers.length; i++) {

                        if ($scope.data.quizs[$scope.randomIndex].answers[i].is_correct == 1) {
                            $scope.resultTrueText = $scope.data.quizs[$scope.randomIndex].answers[i].title;
                            break;
                        }
                    }                    
                }

                // $scope.state = 1;
                // $scope.stateText = "Câu tiếp";
                // $scope.disabledOver = true;
            }             
            //jQuery('.quiz-test-body .item [name^="answer_quiz"]').attr('disabled', 'disabled');                  
        } 
        else if($scope.state == 1)
        {   
            $('input[name^="answer_quiz"]:checked').prop('checked', false); 
            $('textarea[name^="answer_quiz"]').val(''); 
            $scope.randomIndex = $scope.arIndexQuiz[Math.floor(Math.random() * $scope.arIndexQuiz.length)];

            $scope.resultStatusText = "";
            $scope.resultStatus = false;
            $scope.resultTrueText = "";
            $scope.state = 0;
            $scope.stateText = "Kiểm tra";
            $scope.answerVal = null;
            $scope.disabledOver = false;
            $scope.disabledCheck = true; 
            $('.quiz-test-body .row .item.checked').removeClass('checked');


        }
        else
        {
            $scope.scoreQuiz = 0;
            $scope.arIndexQuiz.length = 0;
            for (var i = 0; i < $scope.data.quizs.length; i++) {
                $scope.arIndexQuiz.push(i);
            }
            $scope.randomIndex = $scope.arIndexQuiz[Math.floor(Math.random() * $scope.arIndexQuiz.length)];

            $scope.state = 0;
            $scope.stateText = "Kiểm tra";
            $scope.disabledOver = false;
            $scope.disabledCheck = true;
            $scope.stateOver = true;
            $scope.complete = false;
            $scope.answerVal = null;

            $('.quiz-test-body .row .item.checked').removeClass('checked');
        }
    }
    $scope.plusScore = function(){
        $scope.stateOver = false;
        $scope.resultStatus = false;
        $scope.resultStatusText = "";
        $scope.state = 2;
        $scope.stateText = "Luyện tập lại";        

        $http({
            url: '/ajaxResultQuiz',
            method: 'POST',
            data: 
            {
                jsonResult: result,
                groupQuizId: groupQuizId
            },
        }).then(function(response){
            $scope.notify = $sce.trustAsHtml(response.data.notify);
            $scope.complete = true;
        });        
    }
    $scope.change = function(answerVal){
        answerVal ? $scope.disabledCheck = false : $scope.disabledCheck = true;   
        $scope.answerVal = answerVal;     
    }
});

app.factory('strLimit', function(){
    return function(str, limit){
        if (str.length > limit)
        {
            return str = str.substring(0, limit - 3) + '...';
        }
        return str;
    }
});

app.controller('orderUser', function($scope, $http){
    $http({
        url:'/orderUser',
        method:'GET'
    }).then(function(response){
        $scope.users = response.data;
    }, function(response){
        console.log(response);
    });
});

app.controller('listCat', function($scope, $http, formatString, strLimit){
    $http({
        url:'/getCatsNumberPosts',
        method:'GET'
    }).then(function(response){
        $scope.cats = response.data;
        for (var i = $scope.cats.length - 1; i >= 0; i--) {
            $scope.cats[i].cat_url = '/chuyen-muc/'+ formatString($scope.cats[i].cat_title) + '-' + $scope.cats[i].id;
        }
    }, function(response){
        console.log(response);
    });
});