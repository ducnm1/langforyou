<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Youtube;

class YoutubesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();        

        for ($i=0; $i < 5; $i++) {
            $youtube = new Youtube;
            $youtube->user_id = 8;
            $youtube->youtube_title = $faker->text($maxNbChars = 100);
            $youtube->youtube_code_id = "31chz2WlL3Q";
            $youtube->youtube_thumbnail = "https://i.ytimg.com/vi/31chz2WlL3Q/mqdefault.jpg";
            $youtube->youtube_sub_en = $faker->paragraph($nbSentences = 50, $variableNbSentences = true);
            $youtube->youtube_sub_vn = $faker->paragraph($nbSentences = 50, $variableNbSentences = true);
            $youtube->save();         
        }
    }
}
