<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();      

        for ($i=0; $i < 10; $i++) {
            $post = new Post;
            $post->user_id = 2;
            $post->cat_id = 1;
            $post->group_quiz_id = 1;
            $post->post_title = $faker->text($maxNbChars = 100);
            $post->post_thumbnail = $faker->imageUrl($width = 320, $height = 180);
            $post->post_content = "";
            $post->post_type = 'ngu-phap';
            $post->youtube_id = 1;
            $post->post_lang = "JP";
            $post->post_timer = 0;
            $post->post_status = "public";
            $post->post_view_count = 14;
            $post->save();         
        }
    }
}
