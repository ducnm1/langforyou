<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Cat;

class CatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();        

        for ($i=0; $i < 5; $i++) {
            $cat = new Cat;
            $cat->user_id = 1;
            $cat->cat_title = $faker->text($maxNbChars = 50);
            $cat->save();         
        }
    }
}
