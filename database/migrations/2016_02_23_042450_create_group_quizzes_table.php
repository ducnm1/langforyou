<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('group_quiz_parent_id')->default(-1);            
            $table->string('group_quiz_title');
            $table->text('group_quiz_description', 500);
            $table->string('group_quiz_thumbnail');
            $table->char('group_quiz_lang',3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('group_quizzes');
    }
}
