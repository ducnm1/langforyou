<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYoutubesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youtubes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('youtube_title');
            $table->string('youtube_code_id');
            $table->string('youtube_thumbnail');
            $table->text('youtube_sub_en');
            $table->text('youtube_sub_vn');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('youtubes');
    }
}
