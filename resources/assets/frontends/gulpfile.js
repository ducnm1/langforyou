var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var swig 		= require('gulp-swig');
var imageop     = require('gulp-image-optimization');
var uglify      = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var reload      = browserSync.reload;
var include       = require("gulp-include");



// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("./source/styles/*.scss")
        .pipe(sass())
        .pipe(uglifycss({
          "maxLineLen": 1000,
          "uglyComments": true
        }))
        .pipe(gulp.dest("./app/assets/css"))
        .pipe(gulp.dest('../../../public/frontends/css'))
        .pipe(browserSync.stream());
});

// swig
gulp.task('templates', function() {
  gulp.src('./source/templates/pages/*.html')
    .pipe(swig({
        defaults : {
            cache : false
        }
    }))
    .pipe(gulp.dest('./app'))
});

// copy image, compress image
gulp.task('images', function(cb) {
    gulp.src('./source/assets/images/**').pipe(imageop({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    }))
    .pipe(gulp.dest('./app/assets/images')).on('end', cb).on('error', cb)
    .pipe(gulp.dest('../../../public/frontends/images'));
});

// process JS files and return the stream.
gulp.task('js', function () {
    return gulp.src(['./source/assets/js/*.js', '!./source/assets/js/libs.js'])
        .pipe(gulp.dest('./app/assets/js'))
        .pipe(gulp.dest('../../../public/frontends/js'));
});

gulp.task('compress_js', function () {
    return gulp.src('./source/assets/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./app/assets/js'));
});

gulp.task("scripts", function() { 
  gulp.src("./source/assets/js/libs.js")
    .pipe(include())
      .on('error', console.log)
    .pipe(uglify())
    .pipe(gulp.dest("./app/assets/js"))
    .pipe(gulp.dest('../../../public/frontends/js'));
});

// copy font
gulp.task('copy_fonts', function () {
    return gulp.src(['./source/assets/fonts/**'])
        .pipe(gulp.dest('./app/assets/fonts/'));
});


// gulp libs
gulp.task('libs', function () {
    return gulp.src('./source/libs/*/**')
        .pipe(gulp.dest('./app/libs'));
});

// Static Server + watching scss/html files
gulp.task('serve', ['sass', 'templates', 'js'], function() {
    browserSync.init({
        server: "./app/",
        files:[
                "./app/*.html",
                "./app/assets/css/main.css",
                "./app/assets/js/*.js"
            ]
    });

    gulp.watch("./source/styles/**.scss", ['sass']);
    gulp.watch("./source/templates/**.html" , ['templates']);
    gulp.watch("./source/assets/js/**.js", ['js']);
});

gulp.task('copy_all', ['js', 'copy_fonts', 'images', 'scripts']);
gulp.task('default', ['serve']);
