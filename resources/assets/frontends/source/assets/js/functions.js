"use strict";
jQuery( document ).ready(function($) {
	/**
	 * back to top
	 */

	var scrollTrigger = 100;
	var backToTop = function () {
	    var scrollTop = $(window).scrollTop();
	    if (scrollTop > scrollTrigger) {
	        $('#back-to-top').addClass('active');
	    } else {
	        $('#back-to-top').removeClass('active');
	    }
	};
	backToTop();
	$(window).on('scroll', function () {
	    backToTop();
	});
	$('#back-to-top').on('click', function (e) {
	    e.preventDefault();
	    $('html,body').animate({
	        scrollTop: 0
	    }, 700);
	});

	/* mobile menu */
	$('.icon-slide').click(function(){
		var $this = $(this);
		if (!$this.hasClass('active')) {
			$this.addClass('active');
			$('.mobile-sidebar').addClass('open');
			$('.wrap-page').addClass('m-slideout');
			$('html').addClass('m-open');
		} else {
			$this.removeClass('active');
			$('.mobile-sidebar').removeClass('open');
			$('.wrap-page').removeClass('m-slideout');
			$('html').removeClass('m-open');
		}		
	});

	/* transcript youtube */ 
	if ($('.detail_page .sub-item .panel-body .panel-body').length > 0) {
		$('.detail_page .sub-item .panel-body .panel-body').mCustomScrollbar({
			theme:"dark"
		})
	}
	/* Sticky menu */ 
	var  mn = $("#page-header .page-header-bottom");
	var mns = "main-nav-scrolled";
	var hdr = $('#page-header .page-header-middle').height();

	$(window).scroll(function() {
	  if( $(this).scrollTop() > hdr ) {
	    mn.addClass(mns);
	  } else {
	    mn.removeClass(mns);
	  }
	});
});
