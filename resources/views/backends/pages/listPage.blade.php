@extends('backends.layouts.master')

@section('title', 'List page')

@section('body_class', 'page_list')

@section('main_content')
	<div class="content-wrapper">		
        <section class="content">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="box box-primary box-list-item">
        				
		                <div class="box-header with-border">
							<h3 class="box-title">Danh sách các bài viết</h3>
							<div class="pull-right">
								<a href="{{ route('page.create') }}" class="btn btn-primary">Tạo bài mới</a>
							</div>
		                </div><!-- box-header -->
		                @if(count($pages)>0)			                
			                <div class="box-body no-padding">			                	
			                  <div class="page-controls clearfix">
			                    <div class="pull-right">
			                      <div class="pages-count pull-left">
			                      	{{ $pages->total() }} items
			                      </div>
			                      <div class="pull-left">
			                        {!! $pages->links() !!}
			                      </div><!-- btn-group -->
			                    </div><!-- pull-right -->
			                  </div>
			                  <div class="table-responsive ">
			                    <table class="table table-hover table-striped">
									<thead>
										<tr>
											<th>ID</th>
											<th class="page-user">User</th>
											<th class="page-title" colspan="3">Title</th>
											<th class="page-date">Created at</th>
											<th class="page-date">Updated at</th>
										</tr>
									</thead>
			                      	<tbody>
				                        @foreach($pages as $page)				                        	
											<tr>
												<td class="page-id" width="80">{{ $page['id'] }}</td>
					                          	<td class="page-user" width="150"><a href="/"> {{ $page['name'] }} </a></td>
					                          	<td class="page-title"><a href="{{ route('page.show',['name'=>$page['page_title_unicode'],'id'=>$page['id']] )}}">{{ $page['page_title'] }}</a></td>

					                          	<td class="page-edit" width="50">				
					                          		@if(Auth::user()->id == $page['user_id'] || ( Auth::user()->user_level > $page['user_level']  && Auth::user()->user_level > 3 ))                          		
					                          		<a href="{{ route('page.edit', $page['id'])}}">Edit</a>
					                          		@endif
					                          	</td>
					                          	<td class="page-delete" width="60">
					                          		@if(Auth::user()->id == $page['user_id'] || ( Auth::user()->user_level > $page['user_level']  && Auth::user()->user_level > 3 ))                       		
					                          		<a href="{{ route('page.destroy', $page['id'])}}" class="text-danger">Delete</a>
					                          		@endif
					                          	</td>
					                          	<td class="page-date" width="140">{{ $page['created_at'] }}</td>
					                          	<td class="page-date" width="140">{{ $page['updated_at'] }}</td>
					                        </tr>					                        
										@endforeach
			                      	</tbody>
			                    </table><!-- table -->
			                  </div><!-- mail-box-messages -->
			                </div><!-- box-body -->
			                <div class="box-footer no-padding">
			                  <div class="page-controls clearfix">
			                    <div class="pull-right">
			                      	<div class="pages-count pull-left">
				                      	{{ $pages->total() }} item
			                      	</div>
			                      	<div class="pull-left">
			                        	{!! $pages->links() !!}
			                      	</div><!-- btn-group -->
			                    </div><!-- pull-right -->
			                  </div>
			                </div>
							
			            
		                @else 
							<p>Bạn chưa có bài viết nào.</p>
		                @endif
		            </div><!-- box -->
        		</div>
        	</div>
			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection