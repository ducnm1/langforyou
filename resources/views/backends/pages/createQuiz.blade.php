@extends('backends.layouts.create')

@section('title','Create Quiz')

@section('body_class','create-quiz')

@section('entry_title', 'Tạo câu hỏi mới')

@section('main_content')
	{!!
		Form::open([
			'route'=>['quiz.store'],
			'method'=>'POST',
			'class'=>'post_form',
			'files'=>true
		])
	!!}
		<div class="row">			
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('quiz_title', 'Tiêu đề') !!}
					{!! Form::text('quiz_title',null,['id'=>'quiz_title','class'=>'form-control']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('post_content', 'Nội dung'); !!}
					{!! Form::textarea('quiz_description',null,['id'=>'quiz_content','class'=>'form-control']) !!}
				</div>
				<div class="row answers">
					<div class="col-md-3">
						<div class="form-group">
							{!! Form::label('answer_1', 'Đáp án 1') !!}
							{!! Form::text('answers_0',null,['id'=>'answer_1','class'=>'form-control','placeholder'=>'Answer 1']) !!}
							<div class="wrap-thumb">       
							   <input class="thumbUpload" type="file" name="answer_thumbnail0" />
							   <div class="image-holder"> </div>
							</div>
						</div>
						<!-- end answer 1 -->
					</div>
					<div class="col-md-3">
						<div class="form-group">
							{!! Form::label('answer_2', 'Đáp án 2') !!}
							{!! Form::text('answers_1',null,['id'=>'answer_2','class'=>'form-control','placeholder'=>'Answer 2']) !!}
							<div class="wrap-thumb">       
							   <input class="thumbUpload" type="file" name="answer_thumbnail1" />
							   <div class="image-holder"> </div>
							</div>
						</div>
						<!-- end answer 2 -->
					</div>
					<div class="col-md-3">
						<div class="form-group">
							{!! Form::label('answer_3', 'Đáp án 3') !!}
							{!! Form::text('answers_2',null,['id'=>'answer_3','class'=>'form-control','placeholder'=>'Answer 3']) !!}
							<div class="wrap-thumb">       
							   <input class="thumbUpload" type="file" name="answer_thumbnail2" />
							   <div class="image-holder"> </div>
							</div>
						</div>
						<!-- end answer 3 -->
					</div>
					<div class="col-md-3">
						<div class="form-group">
							{!! Form::label('answer_4', 'Đáp án 4') !!}
							{!! Form::text('answers_3',null,['id'=>'answer_4','class'=>'form-control','placeholder'=>'Answer 4']) !!}	
							<div class="wrap-thumb">       
							   <input class="thumbUpload" type="file" name="answer_thumbnail3" />
							   <div class="image-holder"> </div>
							</div>
						</div>
						<!-- end answer 4 -->
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group quiz_select show">
							{!! Form::label('is_correct', 'Đáp án đúng') !!}
							{!! Form::select(
									'answer_is_correct',
									[
										'0'=>'Đáp án 1',
										'1'=>'Đáp án 2',
										'2'=>'Đáp án 3',
										'3'=>'Đáp án 4'
									],
									null,
									[
										'id'=>'answer_is_correct',
										'class'=>'form-control'
									]
								) 
							!!}
						</div>
						<div class="quiz_input hidden">
							<div class="form-group">
								{!! Form::label('answer_text_1', 'Đáp án đúng') !!}
								{!! Form::text('answer_text_0',null,['id'=>'answer_text_1', 'class'=>'form-control']) !!}
							</div>

							<div class="form-group">
								{!! Form::label('', 'Đáp án khác') !!}
								<div class="different-answers">
									<div class="form-group">											
										{!! Form::text('answer_text_1',null,['class'=>'form-control']) !!}
									</div>
								</div>
								<input type="hidden" name="num_answer">
								<a href="#" class="add_more btn btn-default">Thêm đáp án</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">				
				<div class="form-group">
					{!! Form::label('group_quiz_id', 'Id của bài trắc nghiệm'); !!}
					{!! Form::text('group_quiz_id',null,['id'=>'group_quiz_id','class'=>'form-control','placeholder'=>'Group quiz id']) !!}					
				</div>
				<div class="form-group">
					{!! Form::label('quiz_type', 'Kiểu câu trắc nghiệm') !!}
					{!! Form::select('quiz_type',['select'=>'Chọn đáp án đúng','input'=>'Điền nội dung'],null,['id'=>'quiz_type','class'=>'form-control']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('quiz_media', 'Audio'); !!}
					<input id="quiz_media" type="file" name="quiz_media" />
				</div>
				<div class="form-group">
					{!! Form::label('quiz_thumbnail', 'Ảnh đại diện'); !!}
					
					<div class="wrap-thumb">       
					   <input class="thumbUpload" id="quiz_thumbnail" type="file" name="quiz_thumbnail" />
					   <div class="image-holder"> </div>
					 </div>
				</div>	
			</div>
		</div>

		
		<div class="row">
			<div class="col-md-12">
				<div class="form-group text-right form-submit">
					{!! Form::submit("Lưu",['class'=>'btn btn-primary','name'=>'save']) !!}
				</div>
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection