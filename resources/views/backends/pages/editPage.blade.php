@extends('backends.layouts.create')

@section('title','Edit Page')

@section('entry_title', 'Chỉnh sửa bài viết')

@section('main_content')	
	{!!
		Form::model($page,[
			'route'=>['page.update',$page->id],
			'method'=>'PUT',
			'class'=>'post_form',
			'files'=>true
		])
	!!}
		<?php if(isset($status_action_post) && !empty($status_action_post)) { ?>
		<div class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  {{ $status_action_post }}
		</div>
		<?php } ?>
		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('page_title', 'Tiêu đề') !!}
					{!! Form::text('page_title',null,['id'=>'page_title','class'=>'form-control']) !!}	
				</div>	
					
				<div class="form-group">
					{!! Form::label('page_content', 'Nội dung'); !!}
					{!! Form::textarea('page_content',null,['id'=>'post_content','class'=>'form-control']) !!}
				</div>
				
				
			</div>
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('page_lang', 'Ngôn ngữ') !!}
					<?php 
						$arLang = array(
							'EN'=>'ENGLISH',
							'JP'=>'JAPAN'
						);
					?>
					{!! Form::select('page_lang', $arLang, null, ['class'=>'form-control']); !!}
				</div>
				
				<div class="form-group">
					{!! Form::label('thumbnail', 'Ảnh đại diện'); !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" type="file" name="page_thumbnail" />
					   <div class="image-holder"> </div>
					 </div>
				</div>			
				<div class="form-group text-right form-submit">			
					{!! Form::submit("Cập nhật",['class'=>'btn btn-primary','name'=>'save']) !!}
					<!-- {!! Form::submit("Save Draft",['class'=>'btn btn-default','name'=>'save_draft']) !!} -->
				</div>
			</div>									
		</div>

	{!! Form::close() !!}
@endsection