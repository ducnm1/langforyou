@extends('backends.layouts.create')

@section('title','Edit user')

@section('body_class','edit-user')

@section('entry_title', 'Sửa thông tin user')

@section('main_content')
	{!!
		Form::model($user,[
			'route'=>['user.update', $user->id],
			'method'=>'PUT',
			'class'=>'post_form'
		])
	!!}
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label for="user_level"></label>
					{!! Form::select(
						'user_level',
						[0,1,2,3,4,5],
						null,
						['class'=>'form-control','id'=>'user_level']
					) !!}	
				</div>

				<div class="form-group text-right">
					{!! Form::button("Update",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>				
			</div>
		</div>
	{!! Form::close() !!}    				
@endsection