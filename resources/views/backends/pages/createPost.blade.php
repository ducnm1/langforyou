@extends('backends.layouts.create')

@section('title','Create Post')

@section('body_class','create-post')

@section('entry_title', 'Tạo bài viết mới')

@section('main_content')
	{!!
		Form::open([
			'route'=>['post.store'],
			'method'=>'POST',
			'class'=>'post_form',
			'files'=>true
		])
	!!}

		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('post_title', 'Tiêu đề') !!}
					{!! Form::text('post_title',null,['id'=>'post_title','class'=>'form-control']) !!}	
				</div>	
					
				<div class="form-group">
					{!! Form::label('post_content', 'Nội dung'); !!}
					{!! Form::textarea('post_content',null,['id'=>'post_content','class'=>'form-control']) !!}
				</div>
				
			</div>
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('post_status', 'Trạng thái') !!}
					<?php 
						$arStatus = array(
							'public'=>'Công khai',
							'draft'=>'Bản nháp'
						);
					?>
					{!! Form::select('post_status', $arStatus, null, ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('post_timer', 'Hẹn giờ') !!}
					
					<div class='input-group date' id='post_timer'>
	                    {!! Form::text('post_timer',null,['id'=>'post_timer','class'=>'form-control','placeholder'=>'']) !!}
	                    <span class="input-group-addon">
	                        <span class="fa fa-calendar"></span>
	                    </span>
	                </div>
				</div>
				<div class="form-group">
					{!! Form::label('post_lang', 'Ngôn ngữ') !!}
					<?php 
						$arLang = array(
							'EN'=>'ENGLISH',
							'JP'=>'JAPAN'
						);
					?>
					{!! Form::select('post_lang', $arLang, null, ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('cat_id', 'Chuyên mục') !!}
					{!! Form::select('cat_id', $arCats, null, ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('group_quiz_id', 'Id của bài trắc nghiệm') !!}					
					{!! Form::text('group_quiz_id',null,['id'=>'group_quiz_id','class'=>'form-control','placeholder'=>'Group quizzes id']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('vocabularies_id', 'Từ vựng') !!}
					{!! Form::text('vocabularies_id',null,['id'=>'vocabularies_id','class'=>'form-control','placeholder'=>'Danh sách id từ vựng']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('youtube_id', 'Id của video youtube') !!}
					{!! Form::text('youtube_id',null,['id'=>'youtube_id','class'=>'form-control','placeholder'=>'youtube']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('post_type', 'Kiểu bài viết') !!}											
					{!! Form::select(
						'post_type',
						[
							'standard'=>'Tiêu chuẩn',
							'tu-vung'=> 'Từ vựng',
							'ngu-phap'=> 'Ngữ pháp',
							'giai-tri'=> 'Giải trí'
						],
						null,
						['class'=>'form-control','id'=>'post_type']
						) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('thumbnail', 'Ảnh đại diện') !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" type="file" name="post_thumbnail" />
					   <div class="image-holder"> </div>
					 </div>
				</div>		
					
				<div class="form-group text-right form-submit">			
					{!! Form::submit("Lưu",['class'=>'btn btn-primary', 'id'=>'btn-save', 'name'=>'save']) !!}
				</div>
			</div>									
		</div>
	{!! Form::close() !!}        				
@endsection