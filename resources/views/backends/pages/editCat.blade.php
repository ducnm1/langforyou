@extends('backends.layouts.create')

@section('title','Create Cat')

@section('body_class','create-cat')

@section('entry_title', 'Chỉnh sửa chuyên mục')

@section('main_content')
	{!!
		Form::model($cat,[
			'route'=>['cat.update', $cat->id],
			'method'=>'PUT',
			'class'=>'post_form'
		])
	!!}
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					{!! Form::label('cat_title', 'Chuyên mục') !!}
					{!! Form::text('cat_title',null,['id'=>'cat_title','class'=>'form-control']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('cat_parent_id', 'Chuyên mục cha') !!}
					{!! Form::select('cat_parent_id', $arCats, null, ['class'=>'form-control']); !!}
				</div>
				<div class="form-group text-right">
					{!! Form::button("Cập nhật",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>		
			</div>
		</div>
	{!! Form::close() !!}    				
@endsection