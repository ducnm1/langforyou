@extends('backends.layouts.create')

@section('title','Create Group Quiz')

@section('body_class','create-group-quiz')

@section('entry_title', 'Tạo nhóm câu hỏi trắc nghiệm')

@section('main_content')
	{!!
		Form::open([
			'route'=>['group_quiz.store'],
			'method'=>'POST',
			'class'=>'post_form',
			'files'=>true
		])
	!!}

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					{!! Form::label('group_quiz_title', 'Tiêu đề') !!}
					{!! Form::text('group_quiz_title',null,['id'=>'group_quiz_title','class'=>'form-control','placeholder'=>'Group quiz title']) !!}	
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							{!! Form::label('group_quiz_lang', 'Ngôn ngữ') !!}
							<?php 
								$arLang = array(
									'EN'=>'ENGLISH',
									'JP'=>'JAPAN'
								);
							?>
							{!! Form::select('group_quiz_lang', $arLang, null, ['class'=>'form-control']); !!}		
						</div>
					</div>					
				</div>
				<div class="form-group">
					{!! Form::label('group_quiz_parent_id', 'Id của bài trắc nghiệm cha') !!}
					{!! Form::text('group_quiz_parent_id',null,['id'=>'group_quiz_parent_id','class'=>'form-control','placeholder'=>'Group quiz parent id']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('group_quiz_description', 'Mô tả') !!}
					{!! Form::text('group_quiz_description',null,['id'=>'group_quiz_description','class'=>'form-control','placeholder'=>'Group quiz description']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('thumbnail', 'Ảnh đại diện'); !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" type="file" name="group_quiz_thumbnail" />
					   <div class="image-holder"> </div>
					 </div>
				</div>
				
				<div class="form-group text-right form-submit">
					{!! Form::button("Lưu",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>		
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection