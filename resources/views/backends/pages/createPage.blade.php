@extends('backends.layouts.create')

@section('title','Create Page')

@section('body_class','create-post')

@section('entry_title', 'Tạo bài viết mới')

@section('main_content')
	{!!
		Form::open([
			'route'=>['page.store'],
			'method'=>'POST',
			'class'=>'post_form',
			'files'=>true
		])
	!!}

		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('page_title', 'Tiêu đề') !!}
					{!! Form::text('page_title',null,['id'=>'page_title','class'=>'form-control']) !!}	
				</div>	
					
				<div class="form-group">
					{!! Form::label('page_content', 'Nội dung'); !!}
					{!! Form::textarea('page_content',null,['id'=>'post_content','class'=>'form-control']) !!}
				</div>
				
			</div>
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('page_lang', 'Ngôn ngữ') !!}
					<?php 
						$arLang = array(
							'EN'=>'ENGLISH',
							'JP'=>'JAPAN'
						);
					?>
					{!! Form::select('page_lang', $arLang, null, ['class'=>'form-control']); !!}
				</div>
				
				<div class="form-group">
					{!! Form::label('thumbnail', 'Ảnh đại diện'); !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" type="file" name="page_thumbnail" />
					   <div class="image-holder"> </div>
					 </div>
				</div>		
					
				<div class="form-group text-right form-submit">			
					{!! Form::submit("Lưu",['class'=>'btn btn-primary', 'id'=>'btn-save', 'name'=>'save']) !!}
					<!-- {!! Form::submit("Save Draft",['class'=>'btn btn-default', 'id'=>'btn-draft', 'name'=>'save_draft']) !!} -->
				</div>
			</div>									
		</div>
	{!! Form::close() !!}        				
@endsection