@extends('backends.layouts.create')

@section('title','Update Group Quiz')

@section('body_class','create-group-quiz')

@section('entry_title', 'Sửa câu hỏi trắc nghiệm')

@section('main_content')
	{!!
		Form::model($quiz, [
			'route'=>['quiz.update', $quiz->id],
			'method'=>'PUT',
			'class'=>'post_form',
			'files'=>true
		])
	!!}
		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('quiz_title', 'Tiêu đề') !!}
					{!! Form::text('quiz_title',null,['id'=>'quiz_title','class'=>'form-control']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('post_content', 'Nội dung'); !!}
					{!! Form::textarea('quiz_description',null,['id'=>'quiz_content','class'=>'form-control']) !!}
				</div>
				@if($quiz->quiz_type == "select")
					<div class="row answers">
						<?php $count = 0; ?>
						@foreach($answers as $answer)
							<?php $count++; ?>
							<div class="col-md-3">
								<div class="form-group">
									<label for="answers_<?php echo ($count-1); ?>">Đáp án <?php echo $count; ?></label>
									<input type="text" name="answers_<?php echo ($count-1); ?>" class="form-control" value="{{ $answer->answer_title }}">
									<div class="wrap-thumb">       
									   <input class="thumbUpload" type="file" name="answer_thumbnail<?php echo ($count-1); ?>" />
									   <div class="image-holder"> </div>
									</div>
									<input type="hidden" name="id_answers_<?php echo ($count-1); ?>" value="{{ $answer->id }}">
								</div>				
							</div>		
						@endforeach
					</div>
				@endif

				<div class="row">
					<div class="col-md-3">
						<!-- select true answer -->
						@if($quiz->quiz_type == "select")
							<div class="form-group quiz_select">
								{!! Form::label('answer_is_correct', 'Đáp án đúng') !!}
								
								{!! Form::select(
										'answer_is_correct',
										[
											'0'=>'Đáp án 1',
											'1'=>'Đáp án 2',
											'2'=>'Đáp án 3',
											'3'=>'Đáp án 4'
										],
										null,
										[
											'id'=>'answer_is_correct',
											'class'=>'form-control'
										]
									) 
								!!}
							</div>
						@endif

						<!-- input true answer -->
						@if($quiz->quiz_type == "input")
							<div class="quiz_input">
								<?php $count = 0; ?>
								@foreach($answers as $answer)
									<?php $count++; ?>
									@if($count == 1)
										<div class="form-group quiz_input">												
											{!! Form::label('answer_text', 'Đáp án đúng') !!}
											<input type="text" name="answer_text_0" class="form-control" value="{{ $answer->answer_title }}">
										</div>
									@endif
								@endforeach

								<!-- input different true answer -->
								<div class="form-group">
									{!! Form::label('', 'Đáp án khác') !!}
									<div class="different-answers">
										<?php $count = 0; ?>
										@foreach($answers as $answer)
											<?php $count++; ?>
											@if($count > 1)
											<div class="form-group">											
												<input type="text" name="answer_text_{{ $count - 1 }}" class="form-control" value="{{ $answer->answer_title }}">
												
											</div>
											@endif
										@endforeach
									</div>
									<input type="hidden" name="num_answer">
									<a href="#" class="add_more btn btn-default">Thêm đáp án</a>
								</div>
								<!-- form group -->
								
							</div>
						@endif				
					</div>			
				</div>
			</div>
			<div class="col-md-3">				
				<div class="form-group">
					{!! Form::label('group_quiz_id', 'Id của bài trắc nghiệm'); !!}
					{!! Form::text('group_quiz_id',null,['id'=>'group_quiz_id','class'=>'form-control']) !!}					
				</div>
				<div class="form-group">
					{!! Form::label('quiz_type', 'Kiểu câu trắc nghiệm') !!}
					<input type="text" name="quiz_type" id="quiz_type" class="form-control" value="{{ $quiz->quiz_type }}" readonly>	
				</div>
				<div class="form-group">
					{!! Form::label('quiz_media', 'Audio'); !!}
					<input id="quiz_media" type="file" name="quiz_media" />
				</div>
				<div class="form-group">
					{!! Form::label('quiz_thumbnail', 'Ảnh đại diện'); !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" id="quiz_thumbnail" type="file" name="quiz_thumbnail" />
					   <div class="image-holder"> </div>
					 </div>
				</div>
			</div>
		</div>
		
		

		<div class="row">
			<div class="col-md-12">
				<div class="form-group text-right form-submit">
					{!! Form::submit("Cập nhật",['class'=>'btn btn-primary','name'=>'save']) !!}
				</div>
				<!-- form submit -->
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection