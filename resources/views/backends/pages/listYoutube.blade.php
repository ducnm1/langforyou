@extends('backends.layouts.master')

@section('title', 'List youtube')

@section('body_class', 'page_list')

@section('main_content')
	<div class="content-wrapper">		
        <section class="content" ng-controller="listYoutubes">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="box box-primary">
		                <div class="box-header with-border">
		                  <h3 class="box-title">Danh sách video youtube</h3>
		                  
		                </div><!-- /.box-header -->
		                <div class="box-body no-padding loading">
		                  	<div class="post-controls clearfix">		                    
		                    	<div class="pull-right">
		                    		@include('backends.components.pagination')
		                    	</div><!-- /.pull-right -->
		                  	</div>
		                  	<div class="post-controls">
		                  		@include('backends.components.filter')
		                  	</div>
		                  <div class="table-responsive ">
		                    <table class="table table-hover table-striped">
		                    	<thead>
		                    		<tr>
		                    			<th  width="60">ID</th>
		                    			<th  width="200">User name</th>
		                    			<th  colspan="3">Youtube title</th>
		                    		</tr>
		                    	</thead>
		                      <tbody>
		              
								<tr ng-repeat="youtube in youtubes">
		                          	<td >@{{ youtube.id }}</td>
		                          	<td >@{{ youtube.name }}</td>
		                          	<td ><a href="/dashboard/youtube/@{{youtube.id}}/edit"> @{{ youtube.youtube_title }} </a></td>
									
			                        <td class="cat-edit" width="80">
			                          	<a href="/dashboard/youtube/@{{youtube.id}}/edit">Edit</a>
			                        </td>

			                        <td class="cat-delete" width="80">
			                          	<a href="/dashboard/youtube/@{{youtube.id}}/destroy" class="text-danger">Delete</a>
			                        </td>
		                        </tr>							
		                      </tbody>
		                    </table><!-- /.table -->
		                  </div><!-- /.mail-box-messages -->
		                </div><!-- /.box-body -->
		                <div class="box-footer no-padding">
		                  <div class="post-controls clearfix">		                    
		                    <div class="pull-right">
		                     	@include('backends.components.pagination')
		                    </div><!-- /.pull-right -->
		                  </div>
		                </div>
		              </div><!-- /. box -->
        		</div>
        	</div>
			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection