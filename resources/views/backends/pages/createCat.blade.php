@extends('backends.layouts.create')

@section('title','Create Cat')

@section('body_class','create-cat')

@section('entry_title', 'Tạo chuyên mục mới')

@section('main_content')
	{!!
		Form::open([
			'route'=>['cat.store'],
			'method'=>'POST',
			'class'=>'post_form'
		])
	!!}

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('cat_title', 'Chuyên mục') !!}
					{!! Form::text('cat_title',null,['id'=>'cat_title','class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('cat_parent_id', 'Chuyên mục cha') !!}
					{!! Form::select('cat_parent_id', $arCats, null, ['class'=>'form-control']); !!}
				</div>
				<div class="form-group text-right">
					{!! Form::button("Lưu",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>		
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection