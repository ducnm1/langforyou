@extends('backends.layouts.master')

@section('title', 'List user')

@section('body_class', 'page_list')

@section('main_content')
	<div class="content-wrapper">		
        <section class="content">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="box box-primary box-list-item">
        				
		                <div class="box-header with-border">
							<h3 class="box-title">Danh sách thành viên</h3>							
		                </div><!-- box-header -->
		                @if(count($users)>0)
			                <div class="box-body no-padding">			                	
			                  <div class="user-controls clearfix">
			                    <div class="pull-right">
			                      <div class="users-count pull-left">
			                      	{{ $users->total() }} items
			                      </div>
			                      <div class="pull-left">
			                        {!! $users->links() !!}
			                      </div><!-- btn-group -->
			                    </div><!-- pull-right -->
			                  </div>
			                  <div class="table-responsive ">
			                    <table class="table table-hover table-striped">
									<thead>
										<tr>
											<th>ID</th>
											<th class="user-user" colspan="3">User</th>						
											<th class="user-date">Created at</th>
											<th class="user-date">Updated at</th>
										</tr>
									</thead>
			                      	<tbody>
				                        @foreach($users as $user)				                        	
											<tr>
												<td class="user-id" width="80">{{ $user->id }}</td>
					                          	<td class="user-user" width="150"><a href="/"> {{ $user->name }} </a></td>
					                          	<td class="user-edit" width="50">				
					                          		@if(Auth::user()->user_level > 5)                          		
					                          		<a href="{{ route('user.edit', $user->id)}}">Edit</a>
					                          		@endif
					                          	</td>
					                          	<td class="user-delete" width="60">
					                          		@if(Auth::user()->user_level > 5) 
					                          		<a href="{{ route('user.destroy', $user->id)}}" class="text-danger">Delete</a>
					                          		@endif
					                          	</td>	
					                          	<td class="user-date" width="140">{{ $user->created_at }}</td>
					                          	<td class="user-date" width="140">{{ $user->updated_at }}</td>
					                        </tr>					                        
										@endforeach
			                      	</tbody>
			                    </table><!-- table -->
			                  </div><!-- mail-box-messages -->
			                </div><!-- box-body -->
			                <div class="box-footer no-padding">
			                  <div class="user-controls clearfix">
			                    <div class="pull-right">
			                      	<div class="users-count pull-left">				                    	
				                      	{{ $users->total() }} item
			                      	</div>
			                      	<div class="pull-left">
			                        	{!! $users->links() !!}
			                      	</div><!-- btn-group -->
			                    </div><!-- pull-right -->
			                  </div>
			                </div>
		                @endif
		            </div><!-- box -->
        		</div>
        	</div>
			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection