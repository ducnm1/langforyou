@extends('backends.layouts.create')

@section('title','Edit youtube')

@section('body_class','create-youtube')

@section('entry_title', 'Sửa youtube')

@section('main_content')
	{!!
		Form::model($youtube, [
			'route'=>['youtube.update', $youtube->id],
			'method'=>'PUT',
			'class'=>'post_form',
			'files'=>true
		])
	!!}

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					{!! Form::label('youtube_title', 'Tiêu đề') !!}
					{!! Form::text('youtube_title',null,['id'=>'youtube_title','class'=>'form-control','placeholder'=>'Youtube title']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('youtube_url', 'Đường dẫn') !!}
					@if(!empty($youtube->youtube_code_id))
					<input type="text" id="youtube_url" name="youtube_url" class="form-control" placeholder="Youtube url" value="https://www.youtube.com/watch?v={{ $youtube->youtube_code_id }}">
					@else
					<input type="text" id="youtube_url" name="youtube_url" class="form-control" placeholder="Youtube url">
					@endif
				</div>
				<div class="form-group">
					{!! Form::label('youtube_sub_vn', 'Phụ đề tiếng việt') !!}
					<input class="form-control" type="file" name="youtube_sub_vn" />
				</div>
				<div class="form-group">
					{!! Form::label('youtube_sub_en', 'Phụ đề tiếng anh') !!}
					<input class="form-control" type="file" name="youtube_sub_en" />
				</div>
				@if(Auth::user()->user_level == 7)
				<div class="form-group">
					{!! Form::label('change_author', 'Đổi tác giả') !!}
					<select name="change_author" class="form-control">
						@foreach($users as $user)
							@if($youtube->user_id == $user->id)
								<option value="{{ $user->id }}" selected="">{{ $user->name }}</option>
							@else
								<option value="{{ $user->id }}">{{ $user->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
				@endif
				<div class="form-group text-right">
					{!! Form::button("Cập nhật",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>		
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection