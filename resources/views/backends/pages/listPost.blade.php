@extends('backends.layouts.master')

@section('title', 'List post')

@section('body_class', 'page_list')

@section('main_content')
	<div class="content-wrapper">		
        <section class="content" ng-controller="listPosts">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="box box-primary box-list-item">        				
		                <div class="box-header with-border">
							<h3 class="box-title">Danh sách các bài viết</h3>
							<div class="pull-right">
								<a href="{{ route('post.create') }}" class="btn btn-primary">Tạo bài mới</a>
							</div>
		                </div>
		                <!-- box-header -->		                		                
		                <div class="box-body no-padding loading">			                	
		                  	<div class="post-controls clearfix" ng-if="posts.length">
			                    <div class="pull-right">			                      
			                        @include('backends.components.pagination')
			                    </div><!-- pull-right -->
		                  	</div>
		                  	<div class="post-controls">
		                  		@include('backends.components.filter')
		                  	</div>
		                  <div class="table-responsive ">
		                    <table class="table table-hover table-striped">
								<thead>
									<tr>
										<th>ID</th>
										<th >User</th>
										<th>Category</th>
										<th>Type</th>
										<th colspan="3">Title</th>	
										<th>Status</th>	
										<th>Timer</th>	
										<th>Created at</th>
										<th>Updated at</th>
									</tr>
								</thead>
		                      	<tbody>				                        	
									<tr ng-repeat="post in posts">
										<td class="post-id" width="50">@{{ post.id }}</td>
			                          	<td  width="150">
			                          		<a href="/"> @{{ post.name }} </a>
			                          	</td>
			                          	<td width="150">
			                          		<a href="/">@{{ post.cat_title }}</a>
			                          	</td>
			                          	<td width="150">@{{ post.post_type }}</td>
			                          	<td width="200">
			                          		<a href="@{{ post.post_url }}">@{{ post.post_title }}</a>
			                          	</td>

			                          	<td class="post-edit" width="50">  		
			                          		<a href="/dashboard/post/@{{post.id}}/edit">Edit</a>
			                          	</td>
			                          	<td class="post-delete" width="60">	
			                          		<a href="/dashboard/post/@{{post.id}}/destroy" class="text-danger">Delete</a>
			                          	</td>
			                          	<td width="80">@{{ post.post_status }}</td>
										<td width="140"><span ng-if="post.post_timer">@{{ post.post_timer * 1000  | date:'dd-MM-yyyy HH:mm' }}</span> </td>
			                          			
			                          	<td width="140">@{{ post.created_at | filterDate }}</td>
			                          	<td width="140">@{{ post.updated_at | filterDate }}</td>
			                        </tr>	
		                      	</tbody>
		                    </table><!-- table -->
		                  </div><!-- mail-box-messages -->
		                </div><!-- box-body -->
		                <div class="box-footer no-padding" ng-if="posts.length">
		                  <div class="post-controls clearfix">
		                    <div class="pull-right">
			                    @include('backends.components.pagination')
		                    </div><!-- pull-right -->
		                  </div>
		                </div>
		            </div><!-- box -->
        		</div>
        	</div>
			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection