@extends('backends.layouts.create')

@section('title','Create youtube')

@section('body_class','create-youtube')

@section('entry_title', 'Tạo youtube')

@section('main_content')
	{!!
		Form::open([
			'route'=>['youtube.store'],
			'method'=>'POST',
			'class'=>'post_form',
			'files'=>true
		])
	!!}

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				{!! Form::label('youtube_title', 'Tiêu đề') !!}
				{!! Form::text('youtube_title',null,['id'=>'youtube_title','class'=>'form-control','placeholder'=>'Youtube title']) !!}	
			</div>
			<div class="form-group">
				{!! Form::label('youtube_url', 'Đường dẫn') !!}
				{!! Form::text('youtube_url',null,['id'=>'youtube_url','class'=>'form-control','placeholder'=>'Youtube url']) !!}	
			</div>
			<div class="form-group">
				{!! Form::label('youtube_sub_vn', 'Phụ đề tiếng việt') !!}
				<input class="form-control" type="file" name="youtube_sub_vn" />
			</div>
			<div class="form-group">
				{!! Form::label('youtube_sub_en', 'Phụ đề tiếng anh') !!}
				<input class="form-control" type="file" name="youtube_sub_en" />
			</div>
			<div class="form-group text-right">
				{!! Form::button("Lưu",['class'=>'btn btn-primary','type'=>'submit']) !!}
			</div>		
		</div>
	</div>

	{!! Form::close() !!}    				
@endsection