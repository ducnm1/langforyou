@extends('backends.layouts.create')

@section('title','Create vocabulary')

@section('body_class','create-vocabulary')

@section('entry_title', 'Tạo từ vựng mới')

@section('main_content')
	{!!
		Form::open([
			'route'=>['vocabulary.store'],
			'method'=>'POST',
			'class'=>'post_form',
			'files' =>true
		])
	!!}

		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('vocabulary_title', 'Từ vựng') !!}
					{!! Form::text('vocabulary_title',null,['id'=>'vocabulary_title','class'=>'form-control']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('vocabulary_ipa', 'Phiên âm quốc tế') !!}
					{!! Form::text('vocabulary_ipa',null,['id'=>'vocabulary_ipa','class'=>'form-control']) !!}	
				</div>	
				<div class="form-group">
					{!! Form::label('post_content', 'Nội dung giải thích'); !!}
					{!! Form::textarea('vocabulary_content',null,['id'=>'post_content','class'=>'form-control']) !!}
				</div>
				
			</div>
			<!-- col 9 -->
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('thumbnail', 'Ảnh đại diện'); !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" id="thumbnail" type="file" name="vocabulary_thumbnail" />
					   <div class="image-holder"> </div>
					 </div>
				</div>	
				<div class="form-group">
					{!! Form::label('audio', 'Audio'); !!}
					<input type="file" name="vocabulary_media" id="audio" />
				</div>
				<div class="form-group">
					{!! Form::label('post_id', 'Id của bài viết') !!}
					{!! Form::text('post_id',null,['id'=>'post_id','class'=>'form-control']) !!}	
				</div>
				<div class="form-group text-right">
					{!! Form::button("Lưu",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>		
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection