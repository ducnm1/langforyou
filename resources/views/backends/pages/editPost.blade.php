@extends('backends.layouts.create')

@section('title','Edit Post')

@section('entry_title', 'Chỉnh sửa bài viết')

@section('main_content')	
	{!!
		Form::model($post,[
			'route'=>['post.update',$post->id],
			'method'=>'PUT',
			'class'=>'post_form',
			'files'=>true
		])
	!!}
		@if(isset($status_action_post) && !empty($status_action_post))
			<div class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  {{ $status_action_post }}
			</div>
		@endif
		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('post_title', 'Tiêu đề') !!}
					{!! Form::text('post_title',null,['id'=>'post_title','class'=>'form-control']) !!}	
				</div>		
				<div class="form-group">
					{!! Form::label('post_content', 'Nội dung'); !!}
					{!! Form::textarea('post_content',null,['id'=>'post_content','class'=>'form-control']) !!}
				</div>
				
				
			</div>
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('post_status', 'Trạng thái') !!}
					<?php 
						$arStatus = array(
							'public'=>'Công khai',
							'draft'=>'Bản nháp'
						);
					?>
					{!! Form::select('post_status', $arStatus, null, ['class'=>'form-control']); !!}
				</div>
				<div class="form-group">
					{!! Form::label('post_timer', 'Hẹn giờ') !!}
					
					<div class='input-group date' id='post_timer'>
	                    {!! Form::text('post_timer',null,['id'=>'post_timer','class'=>'form-control','placeholder'=>'']) !!}
	                    <span class="input-group-addon">
	                        <span class="fa fa-calendar"></span>
	                    </span>
	                </div>
				</div>
				<div class="form-group">
					{!! Form::label('post_lang', 'Ngôn ngữ') !!}
					<?php 
						$arLang = array(
							'EN'=>'ENGLISH',
							'JP'=>'JAPAN'
						);
					?>
					{!! Form::select('post_lang', $arLang, null, ['class'=>'form-control']); !!}
				</div>
				<div class="form-group">
					{!! Form::label('cat_id', 'Chuyên mục'); !!}
					{!! Form::select('cat_id', $arCats, null, ['class'=>'form-control']); !!}
				</div>
				<div class="form-group">
					{!! Form::label('group_quiz_id', 'Id của bài trắc nghiệm') !!}					
					{!! Form::text('group_quiz_id',null,['id'=>'group_quiz_id','class'=>'form-control','placeholder'=>'Group quizzes id']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('vocabularies_id', 'Từ vựng') !!}
					{!! Form::text('vocabularies_id',null,['id'=>'vocabularies_id','class'=>'form-control','placeholder'=>'Danh sách id từ vựng']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('youtube_id', 'Id của video youtube') !!}
					{!! Form::text('youtube_id',null,['id'=>'youtube_id','class'=>'form-control','placeholder'=>'youtube']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('post_type', 'Kiểu bài viết'); !!}											
					{!! Form::select(
						'post_type',
						[
							'standard'=>'Tiêu chuẩn',
							'tu-vung'=> 'Từ vựng',
							'ngu-phap'=> 'Ngữ pháp',
							'giai-tri'=> 'Giải trí'
						],
						null,
						['class'=>'form-control','id'=>'post_type']
						) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('thumbnail', 'Ảnh đại diện'); !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" type="file" name="post_thumbnail" />
					   <span id="thumbnail_old" class="hidden">{{ $post->post_thumbnail }}</span>
					   <div class="image-holder"></div>
					 </div>
				</div>		

				@if(Auth::user()->user_level == 7)
				<div class="form-group">
					{!! Form::label('change_author', 'Đổi tác giả') !!}
					<select name="change_author" class="form-control">
						@foreach($users as $user)
							@if($post->user_id == $user->id)
								<option value="{{ $user->id }}" selected="">{{ $user->name }}</option>
							@else
								<option value="{{ $user->id }}">{{ $user->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
				@endif

				<div class="form-group text-right form-submit">			
					{!! Form::submit("Cập nhật",['class'=>'btn btn-primary','name'=>'save']) !!}
					<!-- {!! Form::submit("Save Draft",['class'=>'btn btn-default','name'=>'save_draft']) !!} -->
				</div>
			</div>									
		</div>

	{!! Form::close() !!}
@endsection