<?php header("Cache-Control: max-age=2592000, public"); ?>
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="/backends/images/favicon.png"/>
    <link rel="stylesheet" href="/backends/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/backends/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/backends/plugins/adminLte/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/backends/plugins/adminLte/dist/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="/backends/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="/backends/css/admin.css">
    <script src="/backends/js/angular.min.js"></script>
  </head>

  <body class="skin-blue sidebar-mini @yield('body_class')" ng-app="lang4u">
    <div class="wrapper" ng-controller="shareCtl">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="{{ url('/') }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">
            <img src="/backends/images/logo.png" alt="lang4u">
          </span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <?php
                    $comment_author_email=Auth::user()->email;
                    $gravatar_link = 'http://www.gravatar.com/avatar/' . md5($comment_author_email) . '?s=25';
                    echo '<img src="' . $gravatar_link . '" class="img-circle user-image" />';
                  ?>
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">{{Auth::user()->name}}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <?php
                      $comment_author_email=Auth::user()->email;
                      $gravatar_link = 'http://www.gravatar.com/avatar/' . md5($comment_author_email) . '?s=90';
                      echo '<img src="' . $gravatar_link . '" class="img-circle" />';
                    ?>
                    <p>
                      {{Auth::user()->name}}
                      <small>Member since {{Auth::user()->created_at->format('d-m-Y')}}</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!-- <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li> -->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="{{ route('user.profile', Auth::user()->id) }}" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Đăng xuất</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!-- <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li> -->
            </ul>
          </div>
        </nav>
      </header>