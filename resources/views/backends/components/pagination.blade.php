<div class="posts-count pull-left">
  	@{{ pagination.total }} items
</div>
<div class="pull-left">
    <ul class="pagination">
	    <li ng-class="(pagination.current_page - 1) ? '' : 'disabled'">
	      <a href="javascript:;" aria-label="Previous" ng-click="load('/postsAjax?page=', pagination.current_page - 1)">
	        <span aria-hidden="true">&laquo;</span>
	      </a>
	    </li>
	    <li ng-repeat="number in pagination.numbers" ng-class="(number == pagination.current_page) ? 'active' : ''">
	    	<a href="javascript:;" ng-click="load('/postsAjax?page=',number)">@{{ number }}</a>
	    </li>
	    <li ng-class="(pagination.current_page == pagination.last_page ) ? 'disabled' : ''">
	      <a href="javascript:;" aria-label="Next" ng-click="load('/postsAjax?page=', pagination.current_page + 1)">
	        <span aria-hidden="true">&raquo;</span>
	      </a>
	    </li>
	</ul>
</div><!-- btn-group -->