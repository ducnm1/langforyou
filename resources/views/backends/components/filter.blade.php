<div class="row">
	@if(Auth::user()->user_level == 6 || Auth::user()->user_level == 7)
	<div class="col-sm-3">
		<label for="">Tác giả</label>
	    <select class="form-control" ng-change="change()" ng-model="selectUsers.selectedOption.id" ng-options="user.id as user.name for user in selectUsers.options">
	    </select>
	</div>
	@endif

	
	<div class="col-sm-3" ng-if="pathname == '/dashboard/posts'">
		<label for="">Chủ đề</label>		                  				
		<select class="form-control" ng-change="change()" ng-model="selectCats.selectedOption.id" ng-options="cat.id as cat.cat_title for cat in selectCats.options">
    	</select>
	</div>
	<div class="col-sm-2" ng-if="pathname == '/dashboard/posts'">
		<label for="">Kiểu</label>
		<select class="form-control" ng-change="change()" ng-model="selectTypes.selectedOption.id" ng-options="type.id as type.name for type in selectTypes.options">
    	</select>
	</div>
	<div class="col-sm-2" ng-if="pathname == '/dashboard/posts'">
		<label for="">Trạng thái</label>
		<select class="form-control" ng-change="change()" ng-model="selectStatus.selectedOption.id" ng-options="type.id as type.name for type in selectStatus.options">
    	</select>
	</div>
	<div class="col-sm-2" ng-if="pathname == '/dashboard/posts'">
		<label for="">Hẹn giờ</label>
		<select class="form-control" ng-change="change()" ng-model="selectTimer.selectedOption.id" ng-options="type.id as type.name for type in selectTimer.options">
    	</select>
	</div>
</div>