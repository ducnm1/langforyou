<div class="modal-header">
	<button type="button" class="close" ><span class="fa fa-close" ng-click="cancel()"></span></button>
	<h2 class="modal-title" id="myModalLabel"></h2>
	<div class="progress">
	  <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
	  </div>
	</div>
</div>
<div class="modal-body">

</div>
<div class="modal-footer">
	<div class="row">	          
	  <div class="col-md-3 col-xs-12">
	    <button type="button" class="btn btn-default" >Bỏ qua</button>
	    <button type="button" class="btn btn-primary check" >Kiểm tra</button>    
	  </div>
	  <div class="col-md-9 col-xs-12">
	    <div class="result text-left">
	      <span class="result-status"></span>
	      <h4 class="result-true-label hide">Đáp án đúng</h4>
	      <p class="result-true"></p>
	    </div>    
	  </div>
	</div>
</div>