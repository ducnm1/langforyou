<div class="widget widget-user">
	<div class="widget-content">
		@if(Auth::check())
			<div class="info-user media">
				  <div class="media-left user-avatar">
				    <a href="#">
				    	<?php 
							$gravatar_link = 'http://www.gravatar.com/avatar/' . md5(Auth::user()->email) . '?s=80';
						?>
				      <img src="{{ $gravatar_link }}" alt="" class="img-circle">
				    </a>
				  </div>
				  <div class="media-body">
				    <ul class="list-unstyled">
						<li class="user-name">{{ Auth::user()->name}}</li>
						<?php
							$arNameLevel = array('Cấp 1', 'Cấp 2', 'Cấp 3', 'Sinh viên', 'Giáo viên', 'Admin', 'Super Admin');
						?>
						<li class="user-score">Cấp bậc: {{ $arNameLevel[Auth::user()->user_level - 1] }}</li>
						<li><a href="{{ route('user.profile') }}">Tài khoản</a></li>
						<li><a href="{{ url('/logout') }}">Đăng xuất</a></li>	
					</ul>
				  </div>
			</div>
			
		@else
			<p>Hãy đăng nhập để có thể sử dụng các chức năng rèn luyện giúp bạn tăng kĩ năng ngoại ngữ của mình.</p>
			<a class="btn btn-primary" href="{{ url('/login') }}">Đăng nhập</a>
		@endif
	</div>
</div>
<!-- widget user -->

@if($currentNameRoute != "group_quiz.show")
<div class="widget widget-practive" ng-controller="quizTest">
	<div class="widget-content">
		<p>Học một ngôn ngữ đòi hỏi một quá trình luyện tập hàng ngày vì vậy hãy chăm chỉ luyện tập một cách thường xuyên nhất.</p>
		<span class="btn btn-primary" ng-click="initQuizTest()">Luyện tập</span>
	</div>
</div>
<!-- widet practive -->
@endif

@if($currentNameRoute == "post.show")
	@if(count($relatedPosts) > 0)
	<div class="widget widget-related-post">
		<h3 class="widget-title">Có thể bạn quan tâm</h3>
		<div class="widget-content">
			<ul class="list-group">
				@foreach($relatedPosts as $relatedPost)
					<li class="list-group-item" >
						<a href="{{ route('post.show', ['name'=>$relatedPost['post_title_unicode'], 'id'=>$relatedPost['id']] )}}" title="{{ $relatedPost['post_title'] }}" class="clearfix">
							<img src="{{ $relatedPost['post_thumbnail'] }}" alt="{{ $relatedPost['post_title'] }}">
							<span>{{ str_limit($relatedPost['post_title'], 45) }}</span>
						</a>
					</li>	
				@endforeach			
			</ul>		
		</div>
	</div>
	<!-- widget history post -->
	@endif
@else
	@if(Auth::check())
	<div class="widget widget-history-post" ng-controller="historyPost" ng-if="posts.length">
		<h3 class="widget-title">Các bài đã xem</h3>
		<div class="widget-content">
			<ul class="list-group">
				<li class="list-group-item" ng-repeat="post in posts">
					<a href="@{{post.post_url}}" title="@{{post.post_title}}" class="clearfix">
						<img ng-src="@{{post.post_thumbnail}}" alt="@{{post.post_title}}">
						<span ng-bind="post.post_title"></span>
					</a>
				</li>
			</ul>		
		</div>
	</div>
	<!-- widget history post -->
	@endif
@endif

<div class="widget widget-list-cat" ng-controller="listCat">
	<h3 class="widget-title">Các chủ đề</h3>
	<div class="widget-content">
		<ul class="list-group">
		  <li class="list-group-item" ng-repeat="cat in cats"><a href="@{{ cat.cat_url }}">@{{ cat.cat_title }}</a>  <span class="badge">@{{ cat.posts }}</span></li>
		</ul>		
	</div>
</div>


<div class="widget widget-order-person" ng-controller="orderUser">
	<h3 class="widget-title">Bảng xếp hạng</h3>
	<div class="widget-content">
		<table class="table table-bordered">
			<tr ng-repeat="user in users">
				<td>
					<img src="http://www.gravatar.com/avatar/@{{user.email}}?s=48" alt="" class="img-circle">
				</td>
				<td ng-bind="user.name"></td>
				<td ><span ng-bind="user.user_score"></span> điểm</td>
			</tr>
		</table>
	</div>
</div>
<!-- widget-order-person -->


<div class="widget widget-page-facebook">
	<div class="widget-content">
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	
		<div class="fb-page" data-href="https://www.facebook.com/lang4ucom-539106796261667/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>		
	</div>
</div>