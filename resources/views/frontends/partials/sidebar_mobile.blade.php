<div class="mobile-sidebar">
  <div class="widget widget-user">
    <div class="widget-content">
      @if(Auth::check())
        <div class="info-user media">
            <div class="media-left user-avatar">
              <a href="#">
                <?php 
                $gravatar_link = 'http://www.gravatar.com/avatar/' . md5(Auth::user()->email) . '?s=80';
              ?>
                <img src="{{ $gravatar_link }}" alt="" class="img-circle">
              </a>
            </div>
            <div class="media-body">
              <ul class="list-unstyled">
              <li class="user-name">{{ Auth::user()->name}}</li>
              <?php
                $arNameLevel = array('Cấp 1', 'Cấp 2', 'Cấp 3', 'Sinh viên', 'Giáo viên', 'Admin', 'Super Admin');
              ?>
              <li class="user-score">Cấp bậc: {{ $arNameLevel[Auth::user()->user_level - 1] }}</li>
              <li><a href="{{ route('user.profile') }}">Tài khoản</a></li>
              <li><a href="{{ url('/logout') }}">Đăng xuất</a></li> 
            </ul>
            </div>
        </div>
        
      @else
        <p>Hãy đăng nhập để có thể sử dụng các chức năng rèn luyện giúp bạn tăng kĩ năng ngoại ngữ của mình.</p>
        <a class="btn btn-primary" href="{{ url('/login') }}">Đăng nhập</a>
      @endif
    </div>
  </div>
  <!-- widget user -->
  {!! $FrontendTopMenu->asUl(array('class' => '', 'id' => 'mobile-primary-menu')) !!}
</div>
<!-- mobile sidebar -->