<header id="page-header">		
	<div class="page-header-middle">
		<div class="container">
			<a id="logo-top" href="{{ url('/') }}">
				<img src="/frontends/images/logo.png" alt="lang4u">
			</a>
		</div>
	</div>
	<!-- header middel -->
		
	<div class="page-header-bottom">
		<div class="container">		
			<a class="icon-slide visible-sm-block visible-xs-block pull-left"><i class="fa fa-bars"></i></a>

			{{-- $FrontendTopMenu->asUl(array('class' => 'sf-menu visible-md-block visible-lg-block', 'id' => 'primary-menu')) --}}
			<ul id="primary-menu" class="sf-menu visible-md-block visible-lg-block">
				<li><a href="/">Trang chủ</a></li>
				<li><a href="/tu-vung">Từ vựng</a></li>
				<li><a href="/ngu-phap">Ngữ pháp</a></li>
				<li><a href="/giai-tri">Giải trí</a></li>
				<li><a href="/trac-nghiem">Trắc nghiệm</a></li>
			</ul>
			<h2 class="page-title">lang4u.com</h2>

			<div class="search-box visible-md-block visible-lg-block pull-right">
				{!!
					Form::open([
						'route'=>['search'],
						'method'=>'GET',
						'class'=>'search_form'
					])
				!!}
					{!! Form::text('searchKey',null,['id'=>'search','class'=>'form-control','placeholder'=>'Tìm kiếm']) !!}					
					{!! Form::button("",['class'=>'btn btn-default fa fa-search','type'=>'submit']) !!}
				{!! Form::close() !!}
			</div>
			@if(Auth::check())
				<div class="widget-practive visible-sm-block visible-xs-block pull-right">
					<span class="fa fa-pencil-square-o practive ">
					</span>
				</div>
			@endif
		</div>
	</div>
	<!-- header bottom -->		
</header>