<footer id="page-footer">
	<div class="container">
		<div class="copyright">
			<p>© Copyright 2016 <a href="/">Lang4u.com</a></p>	
		</div>
		<ul id="third-menu" class="visible-md-block visible-lg-block">
			<!-- <li><a href="#">Contact</a></li> -->
			<li><a href="#">Faqs</a></li>
		</ul>
	</div>
</footer>
<a href="#" id="back-to-top" class="fa fa-arrow-up"></a>
