<?php header("Cache-Control: max-age=2592000, public"); ?>
<!DOCTYPE html>
<html ng-app="myApp">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta http-equiv="content-language" content="vi" />
    <meta name="desciption" content="Học ngoại ngữ miễn phí mọi nơi mọi lúc. Học tiếng Anh, học tiếng Nhật, trắc nghiệm, ôn thi">
    <!-- <meta property="fb:admins" content="100011629029752" />
    <meta property="fb:app_id" content="1706594402921686" /> -->

    @if($currentNameRoute == "post.show")
    <meta property="og:url" content="<?php echo url()->full(); ?>"/>  
    <meta property="og:title" content="{{$post->post_title}}" />  
      @if($post->post_content != "")
      <meta property="og:description" content="{{ str_limit(strip_tags($post->post_content), 300) }}" />  
      @else
      <meta property="og:description" content="Học ngoại ngữ miễn phí mọi nơi mọi lúc. Học tiếng Anh, học tiếng Nhật, trắc nghiệm, ôn thi" />  
      @endif
    <meta property="og:type" content="article" />  
      @if($post->youtube_id == 0)
      <meta property="og:image" content="http://lang4u.com{{$post->post_thumbnail}}" />
      @else
      <meta property="og:image" content="{{$post->post_thumbnail}}" />
      @endif
    @else 
    <meta property="og:site_name" content="Lang4u" />  
    <meta property="og:description" content="Học ngoại ngữ miễn phí mọi nơi mọi lúc. Học tiếng Anh, học tiếng Nhật, trắc nghiệm, ôn thi" />  
    <meta property="og:type" content="website" /> 
    @endif

    <link rel="alternate" href="http://lang4u.com" hreflang="vi-vn" />
    <link rel="shortcut icon" type="image/png" href="/frontends/images/favicon.png"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/frontends/css/main.css">
    <script src="/frontends/js/angular.min.js"  defer></script>
</head>
<body class="@yield('body_class')" >
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76031719-1', 'auto');
  ga('require', 'linkid');
  ga('send', 'pageview');
  </script>

  @include('frontends.partials.sidebar_mobile')
  <section class="wrap-page">
  	@include('frontends.partials.header')
  	<section id="main-content" class="container">
  		<section class="row">
  			<section id="main-section" class="col-xs-12 col-md-9">
  				@section('main_section')
  				@show			
  			</section>
  			<aside id="sidebar" class="col-xs-12 col-md-3">
  				@section('sidebar')
  					@include('frontends.partials.sidebar')
  				@show			
  			</aside>
  		</section>
  	</section>  	
  	@include('frontends.partials.footer')
    <div class="mask"></div>
  </section>
  <!-- wrap-page -->

@if($currentNameRoute != "group_quiz.show")
    <div class="modal fade quiz-test" id="modalPostQuiz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" ng-controller="quizTest">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header quiz-test-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-close"></span></button>
            <h2 class="modal-title quiz-test-title">@{{quizTestTitle}}</h2>
            <div class="progress">
              <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"  style="width: @{{scoreQuiz}}%">
              </div>
            </div>
          </div>
          <div class="modal-body quiz-test-body">
                <div ng-if="data.quizs[randomIndex].type == 'select'" class="item item-select" ng-hide="complete">
                    <h3 class="quiz-title">@{{data.quizs[randomIndex].title}}</h3>
                    <div class="row">
                      <div class="col-xs-12">
                        <div ng-if="data.quizs[randomIndex].media || data.quizs[randomIndex].thumbnail" class='item-content has-media'>
                            <p>@{{data.quizs[randomIndex].description}}</p>
                            <audio ng-if="data.quizs[randomIndex].media" controls>
                                <source src="@{{data.quizs[randomIndex].media}}"  type="audio/mpeg">
                                Your browser does not support the audio element.
                            </audio>
                            <img ng-if="data.quizs[randomIndex].thumbnail" ng-src="data.quizs[randomIndex].thumbnail" alt="@{{data.quizs[randomIndex].title }}" />
                        </div>
                        <div ng-if="data.quizs[randomIndex].media == '' && data.quizs[randomIndex].thumbnail == ''" class='item-content'>
                            <p>@{{data.quizs[randomIndex].description}}</p>
                        </div>
                      </div>                      
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6" ng-repeat="answer in data.quizs[randomIndex].answers">
                            <div ng-if="answer.thumbnail != ''" class="item has-thumbnail">
                                <label>
                                    <div class="item-thumbnail">
                                        <img ng-src="@{{answer.thumbnail}}" alt="@{{answer.title}}">
                                    </div>
                                    <h4 class="answer-title"> 
                                        <span class="item-control"><i class="fa fa-circle-o"></i></span>
                                        @{{answer.title}}
                                    </h4>
                                    <input type="radio" ng-model="answerVal" ng-change="change(answerVal)" name="answer_quiz_@{{data.quizs[randomIndex].id}}" value="@{{$index}}">
                                </label>
                            </div>
                            <!-- item -->

                            <div ng-if="answer.thumbnail == ''" class="item">
                                <label>
                                    <h4 class="answer-title"> 
                                        <span class="item-control"><i class="fa fa-circle-o"></i></span>
                                        @{{answer.title}}
                                    </h4>
                                    <input type="radio" ng-model="answerVal" ng-change="change(answerVal)"  name="answer_quiz_@{{data.quizs[randomIndex].id}}" value="@{{$index}}">
                                </label>
                            </div>
                            <!-- item -->
                        </div>
                        <!-- col -->
                    </div>
                    <!-- row -->
                </div>
                <!-- item -->

                <div ng-if="data.quizs[randomIndex].type == 'input'" class="item item-input" ng-hide="complete">
                    <h3 class="quiz-title">@{{data.quizs[randomIndex].title}}</h3>
                    <div class="row">
                        <div class='col-md-12'>
                            <div class='row'>
                                <div class='col-md-6'>
                                    <div ng-if="data.quizs[randomIndex].media || data.quizs[randomIndex].thumbnail" class='item-content has-media'>
                                        <p>@{{data.quizs[randomIndex].description}}</p>
                                        <audio ng-if="data.quizs[randomIndex].media" controls>
                                            <source src="@{{data.quizs[randomIndex].media}}"  type="audio/mpeg">
                                            Your browser does not support the audio element.
                                        </audio>
                                        <img ng-if="data.quizs[randomIndex].thumbnail" ng-src="data.quizs[randomIndex].thumbnail" alt="@{{data.quizs[randomIndex].title }}" />
                                    </div>
                                    <div ng-if="data.quizs[randomIndex].media == '' && data.quizs[randomIndex].thumbnail == ''" class='item-content'>
                                        <p>@{{data.quizs[randomIndex].description}}</p>
                                    </div>
                                </div>
                                <div class='col-md-6'>
                                    <textarea name="answer_quiz_@{{data.quizs[randomIndex].id}}" ng-model="answerVal" ng-change="change(answerVal)"  class='form-control' autofocus ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <p class="notify" ng-show="complete" ng-bind-html="notify"></p>
          </div>
          <div class="modal-footer quiz-test-footer">
            <div class="row">              
              <div class="col-md-3 col-xs-12">
                <button type="button" class="btn btn-default btn-dimiss" ng-click="over()" ng-disabled="disabledOver" ng-show="stateOver">Bỏ qua</button>
                <button type="button" class="btn btn-primary btn-check" ng-click="check()" ng-disabled="disabledCheck">@{{stateText}}</button>    
              </div>
              <div class="col-md-9 col-xs-12">
                <div class="result text-left">
                  <span class="result-status" ng-class="resultStatus ? 'false' : 'true'" >@{{resultStatusText}}</span>

                  <h4 class="result-true-label" ng-show="resultStatus">Đáp án đúng</h4>

                  <p class="result-true" ng-show="resultStatus" >@{{resultTrueText}}</p>
                </div>    
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endif

	<script src="/frontends/js/libs.js" defer></script>
	<script src="/frontends/js/functions.js"  defer></script>
	<script src="/frontends/js/main.js"  defer></script>
</body>
</html>
