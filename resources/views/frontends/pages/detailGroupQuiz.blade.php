@extends('frontends.layouts.master')

@section('title', $groupQuiz->group_quiz_title)

@section('body_class', 'detail_page detail-group-vocabulary')

@section('main_section') 
    <section class="wrap-item">        
    	@include('frontends.modules.group_quizzes.contentDetail')
        <input type="hidden" id="group_quiz_id" value="{{$groupQuiz->id}}">
    </section>
    <div id="comment">
        <div id="disqus_thread"></div>
        <script>
       
        var disqus_config = function () {
        this.page.url = "<?php echo url()->full(); ?>"; // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = "<?php echo url()->full(); ?>"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        (function() {
        var d = document, s = d.createElement('script');

        s.src = '//lang4ucom.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
        })();
        </script>
    </div>         
@endsection