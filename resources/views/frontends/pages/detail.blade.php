@extends('frontends.layouts.master')

@section('title', $post->post_title)

@section('body_class', 'detail_page')

@section('main_section')
    @include('frontends.modules.breadcrumb')
    <section class="wrap-item post-{{$post->post_type}}" >        
        @include('frontends.modules.posts.contentDetail')

        @if(!empty($youtube->youtube_sub_en) || !empty($youtube->youtube_sub_vn))
        <section class="transcripts">                        
            <div class="panel-group" id="accordion"  aria-multiselectable="true">
                @include('frontends.modules.transcripts.transcript')
            </div>
        </section>  
        @endif      
        @if($post->post_type == 'tu-vung')
            @include('frontends.modules.vocabularies.contentListVocabulary')
        @endif
        <input type="hidden" id="group_quiz_id" value="{{$post->group_quiz_id}}">
        <input type="hidden" id="post_youtube" value="{{$post->youtube_id}}">
    </section>      
    
    <div id="comment">
        <div id="disqus_thread"></div>
        <script>
       
        var disqus_config = function () {
        this.page.url = "<?php echo url()->full(); ?>"; // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = "<?php echo url()->full(); ?>"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        (function() {
        var d = document, s = d.createElement('script');

        s.src = '//lang4ucom.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
        })();
        </script>
    </div> 
@endsection
