@extends('frontends.layouts.master')

@if($currentNameRoute == 'home')
	@section('title', 'Lang4u')
	@section('body_class', 'home_page')
@else
	@section('title', 'List page')
	@section('body_class', 'list_page')
@endif



@section('main_section')
	@if(count($posts) > 0)
		<div class="widget widget-list-items" data-type="{{ $postType }}"  data-user-id="{{$userId}}" ng-controller="loadMoreController">
			<div class="row">
				@foreach($posts as $post)
					@include('frontends.modules.posts.contentList')	
				@endforeach
			</div>
			<div class="text-center">
				@if(count($posts) > 23)
				<span class="btn btn-default btn-lg" id="load-more" ng-click="load()"><i class="fa"></i>Tải thêm</span>	
				@endif
			</div>
		</div>
	@else
		<p>Chưa có bài viết nào</p>
	@endif
@endsection
