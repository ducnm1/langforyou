@extends('frontends.layouts.master')

@section('title', $page->page_title)

@section('body_class', 'detail_page')

@section('main_section') 
    <section class="wrap-item " >        
        <article class="page page-{{ $page->id }}">
            
            <div class="entry-header">
                <h1 class="entry-title">{{ $page->page_title }}</h1>
                <div class="metadata">
                    <span class="metadata-time"><i class="fa fa-calendar"></i> {{ $page->created_at->format('d-m-Y') }}</span>
                    <div class="fb-like" data-href="https://www.facebook.com/meocat123/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                </div>
            </div>
            <!-- entry header -->
            
            @if(!empty($page->page_content))
            <div class="entry-content">                     
                {!! $page->page_content !!}
            </div>
            @endif
        </article>
        <!-- page -->
    </section>
    <div id="comment">
        <div id="disqus_thread"></div>
        <script>
       
        var disqus_config = function () {
        this.page.url = "<?php echo url()->full(); ?>"; // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = "<?php echo url()->full(); ?>"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        (function() {
        var d = document, s = d.createElement('script');

        s.src = '//lang4ucom.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
        })();
        </script>
    </div> 
@endsection
