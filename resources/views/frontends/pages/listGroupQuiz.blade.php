@extends('frontends.layouts.master')

@section('title', 'Danh sách các bài trắc nghiệm')
@section('body_class', 'list_page')

@section('main_section')
	@if(count($group_quizzes) > 0)
		<div class="widget widget-list-items" >
			<div class="row">
				@foreach($group_quizzes as $group_quiz)
					@include('frontends.modules.group_quizzes.contentList')	
				@endforeach
			</div>
		</div>  
		<div class="text-center">
			@if(count($group_quizzes) > 23)
			<a href="#" class="btn btn-default btn-lg" id="load-more"><i class="fa"></i>Tải thêm</a>	
			@endif
		</div>
	@else
		<p>Chưa có bài trắc nghiệm nào</p>
	@endif
@endsection