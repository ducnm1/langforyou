@extends('frontends.layouts.master')

@section('title', 'Result page')

@section('body_class', 'result_page')

@section('main_section')
    <section class="wrap-item">
        <article class="post">                    
            <div class="entry-header">
                <h1 class="entry-title">{{ $postTitle }}</h1>
            </div>
            <!-- entry header -->
            
            <div class="entry-content">        
                <div class="quiz-score">
                    <h2 class="quiz-score-title">Kết quả: </h2>
                    <div class="progress">
                        @if($resultScore < 8)
                            @if($resultScore < 4)
                        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 40%">
                            @else
                        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: {{ $resultScore }}0%">    
                            @endif
                            <span class="sr-only">Bạn làm đúng  {{ $trueAnswer }}/{{$countQuiz}} câu, không được cộng điểm</span>
                        </div>             
                        @else
                        <div class="progress-bar progress-bar-success progress-bar-striped" style="width: {{ $resultScore }}0%">
                            <span class="sr-only">Bạn làm đúng  {{ $trueAnswer }}/{{$countQuiz}} câu,  được {{ $resultScore }} điểm</span>
                        </div>             
                        @endif                       
                    </div>
                </div>    
                <!-- score -->      
                <div class="list_quizzes">
                    <h2 class="list_quizzes">Tổng quan: </h2>
                    
                    @for($i = 0; $i < $countQuiz; $i++)
                    <div class="list-quiz">
                        <div class="list-quiz-item">
                            <h4 class="list-quiz-item-title">
                                Câu {{ $i + 1 }}: {{ $quizzes[$i]["quiz_title"] }}
                            </h4>    
                            @if($quizzes[$i]["quiz_type"] == "select")
                                <ul class="list-unstyled">
                                    @for($j = 0; $j < count($answers); $j++)
                                        @if($quizzes[$i]["id"] == $answers[$j]["quiz_id"])
                                            <li>
                                            {{ $answers[$j]["answer_title"] }} 
                                            <?php
                                                foreach ($ar_result as $key => $value) { 
                                                    if ($key == $quizzes[$i]["id"]) {
                                                        if ($answers[$j]["answer_is_correct"] == 1) {
                                                            echo '<span class="quiz-status quiz-status-true">Đúng</span>';
                                                        } else {
                                                            if ($value == $answers[$j]["answer_title"]) {
                                                                echo '<span class="quiz-status quiz-status-false">Sai</span>';
                                                            }
                                                        }
                                                    }
                                                }
                                            ?>
                                            </li>
                                        @endif
                                    @endfor
                                </ul>       
                            @else          
                                <?php
                                    foreach ($ar_result as $key => $value) {
                                        if ($key == $quizzes[$i]["id"]) {
                                            echo '<p>'. $value;
                                            $is_corect = false;

                                            for($j = 0; $j < count($answers); $j++){
                                                if ($key == $answers[$j]["quiz_id"] && $value == $answers[$j]["answer_title"]) { 
                                                    $is_corect = true;
                                                    break;
                                                } else {                                                                
                                                    $is_corect = false;
                                                }
                                            }               

                                            if ($is_corect == true) {
                                                echo '<span class="quiz-status quiz-status-true">Đúng</span>';                      
                                            } else {
                                                echo '<span class="quiz-status quiz-status-false">Sai</span>';
                                            }
                                            echo '</p>';
                                        }
                                    }
                                ?>                                                                                                            
                            @endif
                        </div>
                    </div>
                    <!-- list-quiz -->
                    @endfor
                </div>
                <!-- list quiz -->
                <div class="text-center">
                    <a href="{{ route('post.show', $postId)}}" class="btn btn-primary">Làm lại</a>
                </div>
            </div>
        </article>
        <!-- post -->          
    </section>            
@endsection