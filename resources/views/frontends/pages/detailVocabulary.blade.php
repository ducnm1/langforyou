@extends('frontends.layouts.master')

@section('title', $vocabulary->vocabulary_title)

@section('body_class', 'detail_page detail-vocabulary')

@section('main_section') 
    <section class="wrap-item">        
        @include('frontends.modules.vocabularies.contentVocabulary')
    </section>           
@endsection