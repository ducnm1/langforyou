@extends('frontends.layouts.master')

@section('title', 'Search page')
@section('body_class', 'search_page')

@section('main_section')
	@if(count($datas) > 0)
		<div class="widget widget-search-list-items" >
			<h3 class="widget-title">Kết quả tìm kiếm cho: {{$searchKey}}</h3>
			<div class="list-group">
				@foreach($datas as $data)
					<li class="list-group-item">
						<a href="{{ $data['url'] }}">{{ $data['title'] }}</a>
					</li>
				@endforeach
			</div>
		</div> 
	@else
		<div class="widget widget-search-not-found">
			<p>Không có kết quả tìm kiếm phù hợp với bạn.</p>
			<div class="row">
				<div class="col-md-6">
					<div class="search-box pull-left">
						{!!
							Form::open([
								'route'=>['search'],
								'method'=>'GET',
								'class'=>'search_form'
							])
						!!}
							{!! Form::text('searchKey',null,['id'=>'search','class'=>'form-control','placeholder'=>'Tìm kiếm']) !!}					
							{!! Form::button("",['class'=>'btn btn-default fa fa-search','type'=>'submit']) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	@endif
@endsection
