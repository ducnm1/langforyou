@extends('frontends.layouts.master')

@if($currentNameRoute == 'home')
	@section('title', 'Lang4u')
	@section('body_class', 'home_page')
@else
	@section('title', 'List page')
	@section('body_class', 'list_page')
@endif

@section('main_section')
	@if($currentNameRoute == "category.show")
		@include('frontends.modules.breadcrumb')
	@endif
	
	@if($currentNameRoute == "home")
	<div class="widget widget-feature-posts loading" ng-controller="featurePostController" ng-show="posts">
		<h2 class="widget-title">Bài viết nổi bật</h2>
		<div class="widget-content">
			<div class="row">
				@include('frontends.modules.posts.singleItemList')
			</div>
		</div>
	</div>
	<!-- widget feature posts -->
	@endif
	
	<div class="widget widget-list-items loading" ng-controller="loadMoreController" 
	@if($currentNameRoute == "post.listOfUser")
		data-userid="{{$userId}}"
	@elseif($currentNameRoute == "post.listOfType")
		data-posttype="{{$postType}}"
	@elseif($currentNameRoute == "category.show")
		data-catid="{{$catId}}"
	@endif
	>
		@if($currentNameRoute == "home")
		<h2 class="widget-title">Bài viết mới nhất</h2>
		@endif
		<div class="widget-content">
			<div class="row" >
				@include('frontends.modules.posts.singleItemList')
			</div>
			<div class="text-center">
				<span class="btn btn-default btn-lg " id="load-more" ng-click="load()" ng-show="moreContent"><i class="fa"></i>Tải thêm</span>	
			</div>
		</div>
	</div>
@endsection
