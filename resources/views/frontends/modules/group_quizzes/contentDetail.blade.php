<article class="post group-quiz-{{ $groupQuiz->id }}" itemscope itemtype="http://schema.org/NewsArticle">
    <meta itemscope itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage" itemid="https://google.com/article"/>
    <div class="entry-header">
       
        <h1 class="entry-title" itemprop="headline">{{ $groupQuiz->group_quiz_title }}</h1>
      
        <div class="metadata">
            <span class="metadata-author" itemprop="author" itemscope itemtype="https://schema.org/Person"><i class="fa fa-user"></i><span itemprop="name">{{ $user->name }}</span></span>
            <span class="metadata-time" itemprop="datePublished" content="{{ $groupQuiz->created_at }}"><i class="fa fa-calendar"></i> {{ $groupQuiz->created_at->format('d-m-Y') }}</span>
            
            <!-- <span class="fb-like" data-href="https://www.facebook.com/meocat123/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></span> -->
            <div class="fb-like" data-href="{{url()->full()}}" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
        </div>
    </div>
    <!-- entry header -->
    <div class="entry-content">
        {{ $groupQuiz->group_quiz_description }}
        <div class="quiz-test" id="modalPostQuiz" ng-controller="quizTest">
            <div class="quiz-test-header">
                <!-- <h2 class="quiz-test-title">@{{quizTestTitle}}</h2> -->
                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"  style="width: @{{scoreQuiz}}%">
                    </div>
                </div>
            </div>
            <div class="quiz-test-body">
                <div ng-if="data.quizs[randomIndex].type == 'select'" class="item item-select" ng-hide="complete">
                    <h3 class="quiz-title">@{{data.quizs[randomIndex].title}}</h3>
                    <div class="row">
                      <div class="col-xs-12">
                        <div ng-if="data.quizs[randomIndex].media || data.quizs[randomIndex].thumbnail" class='item-content has-media'>
                            <p>@{{data.quizs[randomIndex].description}}</p>
                            <audio ng-if="data.quizs[randomIndex].media" controls>
                              <source src="@{{data.quizs[randomIndex].media}}"  type="audio/mpeg">
                              Your browser does not support the audio element.
                            </audio>
                            <img ng-if="data.quizs[randomIndex].thumbnail" ng-src="@{{data.quizs[randomIndex].thumbnail}}" alt="@{{data.quizs[randomIndex].title }}" />
                        </div>
                        <div ng-if="data.quizs[randomIndex].media == '' && data.quizs[randomIndex].thumbnail == ''" class='item-content'>
                            <p>@{{data.quizs[randomIndex].description}}</p>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6" ng-repeat="answer in data.quizs[randomIndex].answers">
                            <div ng-if="answer.thumbnail != ''" class="item has-thumbnail">
                                <label>
                                      <div class="item-thumbnail">
                                          <img ng-src="@{{answer.thumbnail}}" alt="@{{answer.title}}">
                                      </div>
                                      <h4 class="answer-title"> 
                                          <span class="item-control"><i class="fa fa-circle-o"></i></span>
                                          @{{answer.title}}
                                      </h4>
                                      <input type="radio" ng-model="answerVal" ng-change="change(answerVal)" name="answer_quiz_@{{data.quizs[randomIndex].id}}" value="@{{$index}}">
                                </label>
                            </div>
                            <!-- item -->

                            <div ng-if="answer.thumbnail == ''" class="item">
                                <label>
                                    <h4 class="answer-title"> 
                                          <span class="item-control"><i class="fa fa-circle-o"></i></span>
                                          @{{answer.title}}
                                    </h4>
                                    <input type="radio" ng-model="answerVal" ng-change="change(answerVal)"  name="answer_quiz_@{{data.quizs[randomIndex].id}}" value="@{{$index}}">
                                </label>
                            </div>
                            <!-- item -->
                        </div>
                        <!-- col -->
                    </div>
                    <!-- row -->
                </div>
                <!-- item -->

                <div ng-if="data.quizs[randomIndex].type == 'input'" class="item item-input" ng-hide="complete">
                      <h3 class="quiz-title">@{{data.quizs[randomIndex].title}}</h3>
                      <div class="row">
                          <div class='col-md-12'>
                              <div class='row'>
                                  <div class='col-md-6'>
                                      <div ng-if="data.quizs[randomIndex].media || data.quizs[randomIndex].thumbnail" class='item-content has-media'>
                                          <p>@{{data.quizs[randomIndex].description}}</p>
                                          <audio ng-if="data.quizs[randomIndex].media" controls>
                                              <source src="@{{data.quizs[randomIndex].media}}"  type="audio/mpeg">
                                              Your browser does not support the audio element.
                                          </audio>
                                          <img ng-if="data.quizs[randomIndex].thumbnail" ng-src="@{{data.quizs[randomIndex].thumbnail}}" alt="@{{data.quizs[randomIndex].title }}" />
                                      </div>
                                      <div ng-if="data.quizs[randomIndex].media == '' && data.quizs[randomIndex].thumbnail == ''" class='item-content'>
                                          <p>@{{data.quizs[randomIndex].description}}</p>
                                      </div>
                                  </div>
                                  <div class='col-md-6'>
                                      <textarea name="answer_quiz_@{{data.quizs[randomIndex].id}}" ng-model="answerVal" ng-change="change(answerVal)"  class='form-control' autofocus ></textarea>
                                  </div>
                              </div>
                          </div>
                      </div>                    
                </div>
                <p class="notify" ng-show="complete" ng-bind-html="notify"></p>
            </div>
            <div class="quiz-footer quiz-test-footer">
              <div class="row">              
                <div class="col-md-3 col-xs-12">
                  <button type="button" class="btn btn-default btn-dimiss" ng-click="over()" ng-disabled="disabledOver" ng-show="stateOver">Bỏ qua</button>
                  <button type="button" class="btn btn-primary btn-check" ng-click="check()" ng-disabled="disabledCheck">@{{stateText}}</button>    
                </div>
                <div class="col-md-9 col-xs-12">
                  <div class="result text-left">
                    <span class="result-status" ng-class="resultStatus ? 'false' : 'true'" >@{{resultStatusText}}</span>

                    <h4 class="result-true-label" ng-show="resultStatus">Đáp án đúng</h4>

                    <p class="result-true" ng-show="resultStatus" >@{{resultTrueText}}</p>
                  </div>    
                </div>
              </div>
            </div>
        </div>
        <!-- quiz test -->
    </div>    
</article>
<!-- post -->