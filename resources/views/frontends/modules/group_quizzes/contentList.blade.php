<div class="col-md-4 col-sm-6 col-xs-12">
	<article class="list-item" data-id="{{ $group_quiz['id'] }}" >
		<a href="{{ route('group_quiz.show', ['id'=>$group_quiz['id'], 'name'=>$group_quiz['group_quiz_title_unicode']])}}" class="list-item-thumbnail">
		@if (!empty($group_quiz['group_quiz_thumbnail']))			
			<img src="{{ $group_quiz['group_quiz_thumbnail'] }}" alt="{{ $group_quiz['group_quiz_title'] }}">
		@else
			<img src="/frontends/images/default/320x180.png" alt="{{ $group_quiz['group_quiz_title'] }}">			
		@endif
		</a>
		<h3 class="list-item-title"><a href="{{ route('group_quiz.show', ['id'=>$group_quiz['id'], 'name'=>$group_quiz['group_quiz_title_unicode']])}}" title="{{ $group_quiz['group_quiz_title'] }}">{{ $group_quiz['group_quiz_title'] }}</a></h3>				
	</article>
</div>
<!-- col -->
