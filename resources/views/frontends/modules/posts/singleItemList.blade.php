<div class="col-md-4 col-sm-6" ng-repeat="post in posts">
    <div class="list-item" >
        <a href="@{{post.post_url}}" class="list-item-thumbnail">
            <img ng-if="post.thumbnail != ''" ng-src="@{{post.post_thumbnail}}" alt="@{{post.post_title}}">
            
            <img ng-if="post.thumbnail == ''" ng-src="/frontends/images/default/320x180.png" alt="@{{post.post_title}}">
      
            <i ng-if="post.youtube_id != 0" class="fa fa-play icon-play"></i>
        
        	<span class="item-lang" ng-bind="post.post_lang"></span>
        </a>
        <h3 class="list-item-title">
            <a href="@{{post.post_url}}" title="@{{post.post_title}}" ng-bind="post.post_title">
            </a>
        </h3>              
    </div>
</div>