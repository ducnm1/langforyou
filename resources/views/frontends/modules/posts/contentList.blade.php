<div class="col-md-4 col-sm-6 col-xs-12">
	<article class="list-item" >
		<a href="{{ route('post.show', ['id'=>$post['id'], 'name'=>$post['post_title_unicode']])}}" class="list-item-thumbnail">
		@if (!empty($post['post_thumbnail']))			
			<img src="{{ $post['post_thumbnail'] }}" alt="{{ $post['post_title'] }}">
		@else
			<img src="/frontends/images/default/320x180.png" alt="{{ $post['post_title'] }}">			
		@endif
		@if(!empty($post['youtube_id']))
			<i class="fa fa-play icon-play"></i>			
		@endif
			<span class="item-lang">{{$post['post_lang']}}</span>
		</a>
		<h3 class="list-item-title"><a href="{{ route('post.show', ['id'=>$post['id'], 'name'=>$post['post_title_unicode']])}}" title="{{ $post['post_title'] }}">{{ $post['post_title'] }}</a></h3>				
	</article>
</div>
<!-- col -->
