@if($post->post_youtube != "")
<article class="post post-{{ $post->id }}" itemscope itemtype="http://schema.org/VideoObject">
@else
<article class="post post-{{ $post->id }}" itemscope itemtype="http://schema.org/NewsArticle">
<meta itemscope itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage" itemid="https://google.com/article"/>
@endif
    @if(!empty($youtube))
        <div class="media-item">
            <div class="youtube_content embed-responsive embed-responsive-16by9">
                <!-- <iframe id="player" src="https://www.youtube.com/embed/{{ $youtube->youtube_code_id }}" frameborder="0" allowfullscreen></iframe> -->
                <input type="hidden" id="youtube_code_id" value="{{ $youtube->youtube_code_id }}">
                <div id="player"></div>
                <div class="box-subs">
                    <div class="sub sub-en hide">
                        {!!$youtube->youtube_sub_en!!}
                    </div>
                    <div class="sub sub-vn hide">
                        {!!$youtube->youtube_sub_vn!!}
                    </div>
                </div>
                <div class="next-post hidden">
                    <h3 class="next-post-title">Bài tiếp theo: <span></span></h3>
                    <i class="fa fa-play next-post-now"></i>
                    <div class="windows8">
                        <div class="wBall" id="wBall_1">
                            <div class="wInnerBall"></div>
                        </div>
                        <div class="wBall" id="wBall_2">
                            <div class="wInnerBall"></div>
                        </div>
                        <div class="wBall" id="wBall_3">
                            <div class="wInnerBall"></div>
                        </div>
                        <div class="wBall" id="wBall_4">
                            <div class="wInnerBall"></div>
                        </div>
                        <div class="wBall" id="wBall_5">
                            <div class="wInnerBall"></div>
                        </div>
                    </div>
                    <i class="fa fa-close close-next-post"></i>
                </div>
            </div>
            
        </div>
        <!-- media item -->
        <!-- <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          Để nâng cao trải nghiêm của người dùng LANG4U đang cần các bạn có thể giúp sức chỉnh sửa lại phụ đề của video cho hay hơn. Bạn nào có thể làm được vui lòng liên hệ với mình qua facebook: <a href="https://www.facebook.com/profile.php?id=100011629029752">Nguyễn minh đức</a>
        </div> -->
    @endif
    <div class="entry-header">
        @if($post->post_youtube != "")
        <h1 class="entry-title" itemprop="name">{{ $post->post_title }}</h1>
        @else
        <h1 class="entry-title" itemprop="headline">{{ $post->post_title }}</h1>
        @endif
        <div class="metadata">
            <span class="metadata-author" itemprop="author" itemscope itemtype="https://schema.org/Person">
                <i class="fa fa-user"></i>
                <a href="{{ route('post.listOfUser', $user->id) }}">
                    <span itemprop="name">{{ $user->name }}</span>
                </a>
            </span>

            <span class="metadata-view"><i class="fa fa-eye"></i>{{ $post->post_view_count }}</span>
            <span class="metadata-time" itemprop="datePublished" content="{{ $post->created_at }}"><i class="fa fa-calendar"></i> {{ $post->created_at->format('d-m-Y') }}</span>
            
            <div class="fb-like" data-href="{{url()->full()}}" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
        </div>
    </div>
    <!-- entry header -->
    
    @if(!empty($post->post_content))
    <div class="entry-content">                     
        {!! $post->post_content !!}
    </div>
    @endif
</article>
<!-- post -->