{!!
    Form::open([
        'route'=>['quiz.result'],
        'method'=>'POST',
        'class'=>'post_form form_quiz'
    ])
!!}

    <div class="panel-group wrap-carousel carousel_loading" id="sub-accordion"  aria-multiselectable="true">
        <div class="quiz-carousel">
            <?php
                $count=0;

                foreach ($quizzes as $quiz) {
                    $count++;

            ?>      
                    @if($quiz["quiz_type"] == "select")
                        <div class="item ">
                            <h4 class="quiz-title"><?php echo "Question " . $count . ": "; ?> {{ $quiz["quiz_title"] }}</h4>
                            <div class="quiz-body">
                                <ul class="list-unstyled row">
                                    <?php
                                    for ($j=0; $j < count($answers) ; $j++) { 
                                        if ($quiz["id"] == $answers[$j]["quiz_id"]) {
                                    ?>
                                        <li class="col-md-3">
                                            <label>                                                 
                                                <input type="radio" name="{{ $quiz['id'] }}" value="{{ $answers[$j]['answer_title'] }}">
                                                {{ $answers[$j]['answer_title'] }}          
                                            </label>
                                        </li>
                                    <?php } } ?>
                                    
                                </ul>
                            </div>
                        </div>                  
                    @endif
            <?php  }

            ?>
        </div>
    </div>
    <!-- wrap-carousel -->


    <input type="hidden" name="ar_result">
    <div class="form-submit">
        <button class="btn btn-primary" type="submit">Xem kết quả</button>
    </div>
{!! Form::close() !!}