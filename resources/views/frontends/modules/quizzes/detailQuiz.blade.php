@include('errors.form')

{!!
    Form::open([
        'route'=>['quiz.result'],
        'method'=>'POST',
        'class'=>'post_form form_quiz'
    ])
!!}

    <div class="panel-group wrap-carousel carousel_loading" id="sub-accordion"  aria-multiselectable="true">
        <div class="quiz-carousel">
            <?php
                $count=0;
                $array_quiz_id = array();

                foreach ($quizzes as $quiz) {
                    $count++;
                    if ($count <= 10) {
                        array_push($array_quiz_id, $quiz["id"]);

            ?>      
                    @if($quiz["quiz_type"] == "select")
                        <div class="item select">
                            @if(!empty($quiz["quiz_description"]))
                            <div class="item-description">
                                {!! $quiz["quiz_description"] !!}
                            </div>
                            @endif
                            <div class="panel panel-default list-quiz-item">
                                <div class="panel-heading"  >
                                    <h4 class="panel-title"><?php echo "Question " . $count . ": "; ?> {{ $quiz["quiz_title"] }}</h4>
                                </div>
                                <div  class="panel-collapse collapse in" >
                                    <div class="panel-body">
                                        @if(!empty($quiz["quiz_media"]))
                                        <div class="item-media">
                                            <audio controls>
                                                <source src="{{ $quiz['quiz_media'] }}" type="audio/mpeg">
                                                Trình duyệt của bạn không hỗ trợ audio    
                                            </audio>
                                        </div>
                                        @endif
                                        <ul class="list-unstyled row">
                                            <?php
                                            for ($j=0; $j < count($answers) ; $j++) { 
                                                if ($quiz["id"] == $answers[$j]["quiz_id"]) {
                                            ?>
                                                @if(!empty($answers[$j]["answer_thumbnail"]))
                                                <li class="has_thumbnail col-md-3">
                                                    <label>
                                                        <img src="{{ $answers[$j]['answer_thumbnail'] }}" alt="{{ $answers[$j]['answer_title'] }}">
                                                @else 
                                                <li class="col-md-3">
                                                    <label>
                                                @endif                                                        
                                                        <input type="radio" name="{{ $quiz['id'] }}" value="{{ $answers[$j]['answer_title'] }}">
                                                        {{ $answers[$j]['answer_title'] }}          
                                                    </label>
                                                </li>
                                            <?php } } ?>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else 
                        <div class="item">
                            <div class="panel panel-default list-quiz-item">
                                <div class="panel-heading"  >
                                    <h4 class="panel-title">
                                        <?php echo "Question " . $count . ": "; ?> {{ $quiz["quiz_title"] }}                          
                                    </h4>
                                </div>
                                <div  class="panel-collapse collapse in" >
                                    <div class="panel-body">
                                        <input type="text" name="{{ $quiz["id"] }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
            <?php } }

            ?>
        </div>
    </div>
    <!-- wrap-carousel -->
    @if($count > 1)
        <div class="carousel-controls clearfix">
            <div class="pull-left">
                <a href="#" class="btn prev-carousel">&laquo; Cau truoc</a>

            </div>
            <div class="pull-right">
                <a href="#" class="btn next-carousel">Cau tiep &raquo;</a>
            </div>
        </div>
        <!-- carousel controls -->
    @endif
    
    <!-- <input type="hidden" name="group_quizzes_id" value="{{ $group_quizzes->id }}">
    <input type="hidden" name="number_quizzes">
    <input type="hidden" name="article_title" value="{{ $post->post_title }}">
    <input type="hidden" name="article_id" value="{{ $post->id }}"> -->

    <?php
        $array_quiz_id = implode(',', $array_quiz_id);
    ?>
    <!-- <input type="hidden" name="array_quiz_id" value="<?php echo $array_quiz_id; ?>"> -->
    <input type="hidden" name="ar_result">
    <div class="form-submit">
        <button class="btn btn-primary" type="submit">Xem kết quả</button>
    </div>
{!! Form::close() !!}