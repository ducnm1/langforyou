<div class="widget widget-breadcrumb">
	<ul class="list-unstyled clearfix">
		<li itemprop="itemListElement" itemscope
      	itemtype="http://schema.org/ListItem">
      		<a itemprop="item" href="/">
      			<span itemprop="name">Trang chủ</span>
      		</a>
      		<meta itemprop="position" content="1" />
      	</li>
      	<?php $indexContentBreadcrumb = 2; ?>
		@foreach($arCats as $arCat)
			<li itemprop="itemListElement" itemscope
  			itemtype="http://schema.org/ListItem">
  				<a itemprop="item" href="{{route('category.show',[
				'name'=>$arCat['cat_unicode_title'], 
				'id'=>$arCat['id']
				])}}"><span itemprop="name">{{ $arCat['cat_title'] }}</span>
				</a>
				<meta itemprop="position" content="{{$indexContentBreadcrumb}}" />
			</li>
			<?php $indexContentBreadcrumb++; ?>
		@endforeach
	</ul>
</div>
