<article class="post post-{{ $vocabulary->id }} " itemscope itemtype="http://schema.org/NewsArticle">    
    <div class="entry-header">
        <h1 class="entry-title"  itemprop="headline">{{ $vocabulary->vocabulary_title }}</h1>
    </div>
    <!-- entry header -->    
    <div class="entry-content">    
        <p>Phiên âm: <span class="v-ipa">{{ $vocabulary->vocabulary_ipa }}</span></p>
        <div class="v-media">
            @if(!empty($vocabulary->vocabulary_media))
            <audio controls>
                <source src="{{ $vocabulary->vocabulary_media }}" type="audio/mpeg">
                Trình duyệt của bạn không hỗ trợ audio    
            </audio>
            @endif

        </div>
        <div class="v-content">
            {!! $vocabulary->vocabulary_content !!}
        </div>
    </div>
</article>
<!-- post -->