
<ul class="list-group list-vocabulary">

@foreach($vocabularies as $vocabulary)
    <li class="list-group-item row">
        <div class="col-md-4">
            <img src="{{ $vocabulary->vocabulary_thumbnail }}" class="img-responsive" alt="">
        </div>
        <div class="col-md-8">
            <div class="v-header"><h4 class="v-title">{{ $vocabulary->vocabulary_title }}</h4> <span class="v-ipa">{{ $vocabulary->vocabulary_ipa }}</span></div>
            <div class="v-media">
                <audio controls>
                    <source src="{{ $vocabulary->vocabulary_media }}" type="audio/mpeg">
                    Trình duyệt của bạn không hỗ trợ audio    
                </audio>    
            </div>            
            <div class="v-content">
                {!! $vocabulary->vocabulary_content !!}
            </div>
        </div>
    </li>
@endforeach

@foreach($vocabularies2 as $vocabulary)
    <li class="list-group-item row">
        <div class="col-md-4">
            <img src="{{ $vocabulary->vocabulary_thumbnail }}" class="img-responsive" alt="">
        </div>
        <div class="col-md-8">
            <div class="v-header"><h4 class="v-title">{{ $vocabulary->vocabulary_title }}</h4> <span class="v-ipa">{{ $vocabulary->vocabulary_ipa }}</span></div>
            <div class="v-media">
                <audio controls>
                    <source src="{{ $vocabulary->vocabulary_media }}" type="audio/mpeg">
                    Trình duyệt của bạn không hỗ trợ audio    
                </audio>    
            </div>            
            <div class="v-content">
                {!! $vocabulary->vocabulary_content !!}
            </div>
        </div>
    </li>
@endforeach
</ul>